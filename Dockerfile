# Dockerfile
FROM php:5.6.24-apache

# Install dependencies
ADD https://raw.githubusercontent.com/mlocati/docker-php-extension-installer/master/install-php-extensions /usr/local/bin/

RUN chmod uga+x /usr/local/bin/install-php-extensions && sync

RUN DEBIAN_FRONTEND=noninteractive apt-get update -q \
    && DEBIAN_FRONTEND=noninteractive apt-get install -qq -y \
      curl \
      git \
      zip unzip \
    && install-php-extensions \
      bcmath \
      bz2 \
      calendar \
      date \
      dba \
      dom \
      ereg \
      exif \
      fileinfo \
      filter \
      ftp \
      gd \
      gettext \
      hash \
      json \
      libxml \
      mcrypt \
      mysql \
      mysqli \
      openssl \
      pcre \
      pdo_mysql \
      pdo_pgsql \
      pgsql \
      posix \
      readline \
      reflection \
      session \
      shmop \
      soap \
      sockets \
      sysvmsg \
      tokenizer \
      wddx \
      xml \
      xmlreader \
      xmlwriter \
      zip 
# missing extensions:
#      mhash \
#   SimpleXML
# Apache configuration
COPY proyect.conf /etc/apache2/sites-available/
RUN cd /etc/apache2/sites-available
RUN a2dissite 000-default.conf
RUN a2ensite proyect.conf
RUN a2enmod rewrite 
# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
WORKDIR /var/www/html
