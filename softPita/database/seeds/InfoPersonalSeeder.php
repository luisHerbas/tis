<?php

use Illuminate\Database\Seeder;
use App\InformacionPersonal;
class InfoPersonalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Informacion personal de estudiantes del websis
        InformacionPersonal::create([
            'nombres'    => 'Luis',
            'apellidos'  => 'Herbas',
            'email'      => 'luis@gmail.com',
            'direccion'  => '8698 At C.'
        ]);

        InformacionPersonal::create([
            'nombres'    => 'Rogan',
            'apellidos'  => 'Horn',
            'email'      => 'malesuada.vel.venenatis@lobortis.edu',
            'direccion'  => '998-1326 Dictum Ctra'
        ]);

        InformacionPersonal::create([
            'nombres'    => 'Joel',
            'apellidos'  => 'Shepherd',
            'email'      => 'turpis@accumsanneque.co.uk',
            'direccion'  => 'Apartado núm.: 715, 3179 Dolor Avenida'
        ]);

        InformacionPersonal::create([
            'nombres'    => 'James',
            'apellidos'  => 'Justice',
            'email'      => 'at.arcu@tellus.ca',
            'direccion'  => 'Apdo.:459-1026 Dictum '
        ]);

        InformacionPersonal::create([
            'nombres'    => 'Branden',
            'apellidos'  => 'Lindsey',
            'email'      => 'tempus.lorem@Duismi.ca',
            'direccion'  => '260-9820 Nunc'
        ]);
        
        InformacionPersonal::create([
            'nombres'    => 'Isaac',
            'apellidos'  => 'Brown',
            'email'      => 'gravida.sit@mi.co.uk',
            'direccion'  => '260-98 Nunc'
        ]);

        InformacionPersonal::create([
            'nombres'    => 'Macaulay',
            'apellidos'  => 'Bradshaw',
            'email'      => 'sem.eget.massa@Quisquetinciduntpede.net',
            'direccion'  => '4151 Pharetra. Carretera'
        ]);

        InformacionPersonal::create([
            'nombres'    => 'Alvin',
            'apellidos'  => 'Mckenzie',
            'email'      => 'urna@placerateget.co.uk',
            'direccion'  => '288-6036 Semper Ctra.'
        ]);

        InformacionPersonal::create([
            'nombres'    => 'Dustin',
            'apellidos'  => 'Farrell',
            'email'      => 'montes.nascetur.ridiculus@loremtristiquealiquet.org',
            'direccion'  => 'Apdo.:923-6455 Integer Avenida'
        ]);

        InformacionPersonal::create([
            'nombres'    => 'Brennan',
            'apellidos'  => 'Chan',
            'email'      => 'felis@ultricesmauris.co.uk',
            'direccion'  => '5613 Congue Calle'
        ]);

        InformacionPersonal::create([
            'nombres'    => 'Clinton',
            'apellidos'  => 'Gillespie',
            'email'      => 'mollis.non@luctussitamet.net',
            'direccion'  => '9776 Pellentesque, C/'
        ]);

        InformacionPersonal::create([
            'nombres'    => 'Murphy',
            'apellidos'  => 'Velez',
            'email'      => 'Aliquam@nibhAliquam.edu',
            'direccion'  => '3959 Faucibus C.'
        ]);

        InformacionPersonal::create([
            'nombres'    => 'Conan',
            'apellidos'  => 'French',
            'email'      => 'dignissim.lacus.Aliquam@aptent.edu',
            'direccion'  => '422-3821 Enim'
        ]);

        InformacionPersonal::create([
            'nombres'    => 'Lionel',
            'apellidos'  => 'Green',
            'email'      => 'ornare.libero.at@utpellentesque.ca',
            'direccion'  => '319-4498 Netus C.'
        ]);

        InformacionPersonal::create([
            'nombres'    => 'Alvin',
            'apellidos'  => 'Woodward',
            'email'      => 'Aenean.eget.metus@nonegestas.co.uk',
            'direccion'  => '995-984 Auctor, Avda.'
        ]);

        InformacionPersonal::create([
            'nombres'    => 'Troy',
            'apellidos'  => 'Cherry',
            'email'      => 'a.scelerisque.sed@est.net',
            'direccion'  => '506-2748 Consequat, Avenida'  
        ]);
    }
}
