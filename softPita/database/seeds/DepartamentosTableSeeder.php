<?php

use Illuminate\Database\Seeder;
use App\Departamento;
use App\Subunidad;
use App\Item;

class DepartamentosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;'); // Desactivamos la revisión de claves foráneas
        //Creamos los departamentos
        Departamento::create([
            'name' => 'Departamento de Informática y Sistemas',
        ]);

        Departamento::create([
            'name' => 'Departamento de Industrial',
        ]);

        //Creamos sus subunidades
        Subunidad::create([
            'name' => 'Laboratorio de Informática',
            'id_departamento' => 1
        ]);

        Subunidad::create([
            'name' => 'Auxiliaturas de pizarra',
            'id_departamento' => 1
        ]);

        Subunidad::create([
            'name' => 'Auxiliaturas de pizarra',
            'id_departamento' => 2
        ]);

        //Asignamos items
        Item::create([
            'subunidad_id' => 1,
            'nombre' => 'Auxiliar de laboratorio de computación',
            'nivel' => 'D',
            'electiva' => 'No'
        ]);

        Item::create([
            'subunidad_id' => 1,
            'nombre' => 'Auxiliar de laboratorio mantenimiento',
            'nivel' => 'D',
            'electiva' => 'No'
        ]);

        Item::create([
            'subunidad_id' => 2,
            'nombre' => 'Introducción a la programación',
            'nivel' => 'A',
            'electiva' => 'No'
        ]);

        Item::create([
            'subunidad_id' => 2,
            'nombre' => 'Elementos de programación',
            'nivel' => 'B',
            'electiva' => 'No'
        ]);

        Item::create([
            'subunidad_id' => 3,
            'nombre' => 'Investigación Operativa',
            'nivel' => 'C',
            'electiva' => 'No'
        ]);

        Item::create([
            'subunidad_id' => 3,
            'nombre' => 'Investigación Operativa 2',
            'nivel' => 'D',
            'electiva' => 'No'
        ]);

        DB::statement('SET FOREIGN_KEY_CHECKS = 1;'); // Reactivamos la revisión de claves foráneas


    }
}
