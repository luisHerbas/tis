<?php

use Illuminate\Database\Seeder;
use App\Docente;

class DocenteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Docentes base de datos Websis
        Docente::create([
            'codsis'          => '202020201',
            'nombre'          => 'Rolando Jaldín',
            'ci'              => '123461',
            'id_informacion'  => 6
        ]); 

        Docente::create([
            'codsis'          => '202020202',
            'nombre'          => 'Leticia Blanco',
            'ci'              => '123462',
            'id_informacion'  => 7
        ]); 

        Docente::create([
            'codsis'          => '202020203',
            'nombre'          => 'Vladimir Costas',
            'ci'              => '123463',
            'id_informacion'  => 8
        ]); 

        Docente::create([
            'codsis'          => '202020204',
            'nombre'          => 'Rosemary Torrico',
            'ci'              => '123464',
            'id_informacion'  => 9
        ]); 

        Docente::create([
            'codsis'          => '202020205',
            'nombre'          => 'Jorge Orellana',
            'ci'              => '123465',
            'id_informacion'  => 10
        ]); 

        Docente::create([
            'codsis'          => '202020206',
            'nombre'          => 'Marcelo Antezana',
            'ci'              => '123466',
            'id_informacion'  => 11
        ]); 

        Docente::create([
            'codsis'          => '202020207',
            'nombre'          => 'Patricia Romero',
            'ci'              => '123467',
            'id_informacion'  => 12
        ]); 

        Docente::create([
            'codsis'          => '202020208',
            'nombre'          => 'Tatiana Aparicio',
            'ci'              => '123468',
            'id_informacion'  => 13
        ]); 

        Docente::create([
            'codsis'          => '202020209',
            'nombre'          => 'Patricia Rodriguez',
            'ci'              => '123469',
            'id_informacion'  => 14
        ]); 

        Docente::create([
            'codsis'          => '202020210',
            'nombre'          => 'Carla Salazar',
            'ci'              => '123470',
            'id_informacion'  => 15
        ]); 

        Docente::create([
            'codsis'          => '202020211',
            'nombre'          => 'Corina Flores',
            'ci'              => '123471',
            'id_informacion'  => 16
        ]); 
    }
}
