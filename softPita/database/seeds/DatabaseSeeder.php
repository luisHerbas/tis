<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PermissionsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(RoleUserSeeder::class);
        $this->call(InfoPersonalSeeder::class);
        $this->call(EstudianteSeeder::class);
        $this->call(DocenteSeeder::class);
        //$this->call(ConvocatoriasTableSeeder::class);
        //$this->call(PostulantsTableSeeder::class);
        //$this->call(DepartamentosTableSeeder::class);
        //$this->call(NotasSeeder::class);
    }
}
