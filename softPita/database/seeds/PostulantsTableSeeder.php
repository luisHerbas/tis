<?php

use Illuminate\Database\Seeder;
use App\Postulant;
use App\NotaConocimiento;
use App\NotaMerito;

class PostulantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;'); // Desactivamos la revisión de claves foráneas

        Postulant::create([
            'requerimiento_id' => 1,
            'cod_sis'          => '201604418',
            'name'             => 'Edgar Ruben',
            'last_name'        => 'Balderrama',
            'address'          => 'C. Santos Taborga 424',
            'phone'            => 76991234,
            'nro_documents'    => 8,
            'nro_certificates' => 2,
            'id_status'        => 2,
            'observaciones'    => 'Ninguna',
            'nota_final'       => 0
        ]); 

        Postulant::create([
            'requerimiento_id' => 1,
            'cod_sis'          => '201604419',
            'name'             => 'Diego Marcelo',
            'last_name'        => 'Choque Ramirez',
            'address'          => 'C. Dorbigni 35',
            'phone'            => 76995678,
            'nro_documents'    => 10,
            'nro_certificates' => 2,
            'id_status'        => 1,
            'observaciones'    => 'Ninguna',
            'nota_final'       => 0
        ]); 

        Postulant::create([
            'requerimiento_id' => 5,
            'cod_sis'          => '201604417',
            'name'             => 'Jorge Luis',
            'last_name'        => 'Herbas Garcia',
            'address'          => 'Av. Blanco Galindo 589',
            'phone'            => 76996512,
            'nro_documents'    => 6,
            'nro_certificates' => 2,
            'id_status'        => 2,
            'observaciones'    => 'Ninguna',
            'nota_final'       => 0
        ]); 

        Postulant::create([
            'requerimiento_id' => 3,
            'cod_sis'          => '201604416',
            'name'             => 'Sandra',
            'last_name'        => 'Renjel Salinas',
            'address'          => 'Av. Victor Ustariz 689',
            'phone'            => 76996589,
            'nro_documents'    => 8,
            'nro_certificates' => 1,
            'id_status'        => 2,
            'observaciones'    => 'Ninguna',
            'nota_final'       => 0
        ]); 

        Postulant::create([
            'requerimiento_id' => 4,
            'cod_sis'          => '201604415',
            'name'             => 'Daniela',
            'last_name'        => 'Santa Cruz Andrade',
            'address'          => 'Av. Tadeo Ahenke 12',
            'phone'            => 76998521,
            'nro_documents'    => 9,
            'nro_certificates' => 1,
            'id_status'        => 2,
            'observaciones'    => 'Ninguna',
            'nota_final'       => 0
        ]); 

        //Postulante 1
        NotaConocimiento::create([
            'postulante_id' => 1,
            'nota_id'          => '1',
            'nota'             => '0',
        ]); 
        NotaConocimiento::create([
            'postulante_id' => 1,
            'nota_id'          => '2',
            'nota'             => '0',
        ]); 
        NotaConocimiento::create([
            'postulante_id' => 1,
            'nota_id'          => '3',
            'nota'             => '0',
        ]); 
        NotaMerito::create([
            'postulante_id' => 1,
            'nota'             => '0',
        ]); 

        //Postulante 2
        NotaConocimiento::create([
            'postulante_id' => 3,
            'nota_id'          => '8',
            'nota'             => '0',
        ]);
        NotaMerito::create([
            'postulante_id' => 3,
            'nota'             => '0',
        ]); 

        //Postulante 3
        NotaConocimiento::create([
            'postulante_id' => 4,
            'nota_id'          => '6',
            'nota'             => '0',
        ]); 
        NotaMerito::create([
            'postulante_id' => 4,
            'nota'             => '0',
        ]); 

        //Postulante 4
        NotaConocimiento::create([
            'postulante_id' => 5,
            'nota_id'          => '6',
            'nota'             => '0',
        ]); 
        NotaMerito::create([
            'postulante_id' => 5,
            'nota'             => '0',
        ]); 
        
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;'); // Reactivamos la revisión de claves foráneas

    }
}
