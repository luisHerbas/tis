<?php

use Illuminate\Database\Seeder;
use App\Estudiante;


class EstudianteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Base de datos estudiantes
        Estudiante::create([
            'codsis'          => '201600010',
            'ci'              => '123456',
            'carrera'         => 'Ingenieria de Sistemas',
            'id_informacion'  => 1
        ]); 
        
        Estudiante::create([
            'codsis'          => '201600011',
            'ci'              => '123457',
            'carrera'         => 'Ingenieria de Sistemas',
            'id_informacion'  => 2
        ]); 

        Estudiante::create([
            'codsis'          => '201600012',
            'ci'              => '123458',
            'carrera'         => 'Ingenieria Informatica',
            'id_informacion'  => 3
        ]); 

        Estudiante::create([
            'codsis'          => '201600013',
            'ci'              => '123459',
            'carrera'         => 'Ingenieria Informatica',
            'id_informacion'  => 4
        ]); 

        Estudiante::create([
            'codsis'          => '201600014',
            'ci'              => '123460',
            'carrera'         => 'Ingenieria Industrial',
            'id_informacion'  => 5
        ]); 

        Estudiante::create([
            'codsis'          => '201600015',
            'ci'              => '113456',
            'carrera'         => 'Ingenieria de Sistemas',
            'id_informacion'  => 6
        ]); 

        Estudiante::create([
            'codsis'          => '201600016',
            'ci'              => '123356',
            'carrera'         => 'Ingenieria de Sistemas',
            'id_informacion'  => 7
        ]); 

        Estudiante::create([
            'codsis'          => '201600017',
            'ci'              => '122456',
            'carrera'         => 'Ingenieria de Sistemas',
            'id_informacion'  => 8
        ]); 

        Estudiante::create([
            'codsis'          => '201600018',
            'ci'              => '100000',
            'carrera'         => 'Ingenieria de Sistemas',
            'id_informacion'  => 9
        ]); 
        
        Estudiante::create([
            'codsis'          => '201600019',
            'ci'              => '100001',
            'carrera'         => 'Ingenieria de Sistemas',
            'id_informacion'  => 10
        ]); 

        Estudiante::create([
            'codsis'          => '201600020',
            'ci'              => '100002',
            'carrera'         => 'Ingenieria Informatica',
            'id_informacion'  => 11
        ]); 

        Estudiante::create([
            'codsis'          => '201600021',
            'ci'              => '100003',
            'carrera'         => 'Ingenieria Informatica',
            'id_informacion'  => 12
        ]); 

        Estudiante::create([
            'codsis'          => '201600022',
            'ci'              => '100004',
            'carrera'         => 'Ingenieria Industrial',
            'id_informacion'  => 13
        ]); 

        Estudiante::create([
            'codsis'          => '201600023',
            'ci'              => '100005',
            'carrera'         => 'Ingenieria de Sistemas',
            'id_informacion'  => 14
        ]); 

        Estudiante::create([
            'codsis'          => '201600024',
            'ci'              => '100006',
            'carrera'         => 'Ingenieria de Sistemas',
            'id_informacion'  => 15
        ]); 

        Estudiante::create([
            'codsis'          => '201600025',
            'ci'              => '100007',
            'carrera'         => 'Ingenieria de Sistemas',
            'id_informacion'  => 16
        ]); 
    }
}
