<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotaConocimientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('notas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('requerimiento_id');
            $table->foreign('requerimiento_id')->references('id')->on('requerimientos')->onDelete('cascade');
            $table->string('nombre_nota');
            $table->integer('porcentaje');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notas');
    }
    
}
