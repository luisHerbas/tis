<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequisitosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('convocatorias', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('subunidad_id');
            $table->foreign('subunidad_id')->references('id')->on('subunidades')->onDelete('cascade');
            $table->longText('titulo_convocatoria');
            $table->date('fecha_publicacion');
            $table->integer('porcentaje_conocimiento');
            $table->integer('porcentaje_meritos');
            $table->string('comision_conocimiento');
            $table->string('comision_merito');
            $table->string('estado')->default('Habilitada');
            $table->integer('publicacion_conocimientos')->default(1);
            $table->integer('publicacion_meritos')->default(1);
            $table->integer('publicacion_finales')->default(1);
            $table->string('gestion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('convocatorias');
    }
}
