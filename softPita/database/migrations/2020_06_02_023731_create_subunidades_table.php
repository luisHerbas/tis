<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubunidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('subunidad_id');
            $table->foreign('subunidad_id')->references('id')->on('subunidades')->onDelete('cascade');
            $table->string('nombre');
            $table->string('nivel');
            $table->string('electiva');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
