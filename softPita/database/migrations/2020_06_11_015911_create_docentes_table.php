<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocentesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('websis')->create('docentes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codsis');
            $table->string('nombre');
            $table->string('ci');
            $table->unsignedInteger('id_informacion');
            $table->foreign('id_informacion')->references('id')->on('informacion_personals')->onDelete('cascade');
           

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('docentes');
    }
}
