<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotaMeritosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('nota_meritos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('postulante_id');
            $table->foreign('postulante_id')->references('id')->on('postulants')->onDelete('cascade');
            $table->integer('nota');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nota_meritos');
    }
}
