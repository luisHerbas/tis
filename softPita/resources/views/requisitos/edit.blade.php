@extends('layouts.app2')
@section('content')

<!-- link bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<div class="container h5"><br>
  <h2 class="text-center">EDITAR REQUISITO</h2><br><br>
  <form method="post" action="{{ route('requisito.update', [ 'convocatoria_id' =>$convocatoria_id, 'requisito_id'=> $requisito->id ]) }}">
    {{ method_field('PUT') }}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="form-group">
      <!-- label: requisito -->
      <label for="">Requisito:</label>
      <!-- textarea: requisito -->
      <input type="text" class="form-control {{$errors->has('requisito')?'is-invalid':'' }}" name="requisito" rows="3" value="{{ isset($requisito->requisito) ? $requisito->requisito:old('requisito')}}" rows="3"></textarea>
      {!! $errors->first('requisito','<div class="invalid-feedback">:message</div>') !!}
    </div>
    <div>
      <!-- botones guardar y cancelar -->
      <button class="btn btn-success" type="submit">Guardar</button>
      <a href="{{ route('detalle.index', [ 'convocatoria_id' =>$convocatoria_id ]) }} " class="btn btn-secondary">Cancelar</a>
    </div>
  </form>
</div>

@endsection