@extends('layouts.app2')
<!-- link bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
@section('content')
<!-- visualizacion de rol -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default"><br>
                <h2 class="text-center">ROL</h2><br><br>

                <div class="panel-body">
                    <!-- datos rol -->
                    <p><strong>Nombre</strong> {{ $role->name }}</p>
                    <p><strong>Slug</strong> {{ $role->slug }}</p>
                    <p><strong>Descripción</strong> {{ $role->description }}</p>
                </div><br>
                <!-- boton volver -->
                <a href="{{ URL::previous() }}" class="btn btn-secondary">Volver</a>
            </div>
        </div>
    </div>

@endsection