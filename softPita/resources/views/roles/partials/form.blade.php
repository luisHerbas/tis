	<!-- formulario creacio rol -->
	<div class="form-group">
		<!-- labelform: nombre etiqueta -->
		{{ Form::label('name', 'Nombre de la etiqueta:') }}
		<!-- input: nombre etiqueta -->
		<input type="text" class="form-control {{$errors->has('name')?'is-invalid':'' }}" name="name" id="name" value="{{ isset($rol->name) ? $rol->name:old('name')}}" required maxlength="150">
		{!! $errors->first('name','<div class="invalid-feedback">:message</div>') !!}

	</div>

	<div class="form-group">
		<!-- labelform: url amigable -->
		{{ Form::label('slug', 'URL Amigable:') }}
		<!-- input: url amigable -->
		<input type="text" class="form-control {{$errors->has('slug')?'is-invalid':'' }}" name="slug" id="slug" value="{{ isset($rol->slug) ? $rol->slug:old('slug')}}" required maxlength="150">
		{!! $errors->first('slug','<div class="invalid-feedback">:message</div>') !!}
	</div>

	<div class="form-group">
		<!-- labelform: descripcion -->
		{{ Form::label('description', 'Descripción:') }}
		<!-- textareaform: descripcion -->
		{{ Form::textarea('description', null, ['class' => 'form-control']) }}
	</div>

	<hr>

	<div class="form-group">
		<!-- permiso especial -->
		<h4>Permiso especial:</h4><br>
		<!-- permiso: ningun acceso -->
		<label>{{ Form::radio('special', 'no-access') }} Ningún acceso</label>
	</div>

	<hr>

	<h3>Lista de permisos:</h3><br>
	<div class="form-group">
		<!-- lista de permisos -->
		<ul class="list-unstyled">
			@foreach($permissions as $permission)
			<li>
				<label>
					{{ Form::checkbox('permissions[]', $permission->id, null) }}
					{{ $permission->name }}
					<em>({{ $permission->description }})</em>
				</label>
			</li>
			@endforeach
		</ul>
	</div>

	<div class="form-group"><br>
		<!-- botones guardar y cancelar -->
		{{ Form::submit('Guardar', ['class' => 'btn btn-success ']) }}
		<a href="{{ URL::previous() }}" class="btn btn-secondary">Cancelar</a>
	</div>