@extends('layouts.app2')
<!-- link bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
@section('content')

<!-- inicio usuarios -->
<div class="container">
    <div class="row">
        <div class="col-md-12 "><br>
            <h2 class="text-center">USUARIOS</h2><br><br>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- boton nuevo usuario -->
                    <a href="{{ route('register') }}" class="btn float-right" style="background:#172962; color:white">Nuevo usuario</a>
                    <br>
                </div>
                <div class="panel-body"><br>
                    <!-- tabla de usuarios -->
                    <table class="table table-bordered table-hover">
                        <thead class="thead-light">
                            <tr>
                                <th width="10px">ID</th>
                                <th>Nombre</th>
                                <th colspan="3">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->name }}</td>
                                <!-- botones ver, rol y eliminar -->
                                <td width="10px">
                                    <a href="{{ route('users.show', $user->id)}}" class="btn btn-info">Ver</a>
                                </td>
                                <td width="10px">
                                    <a href="{{ route('users.edit', $user->id)}}" class="btn btn-warning text-white">Rol</a>
                                </td>
                                <td width="10px">
                                    {!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'DELETE']) !!}
                                    <button class="btn btn-danger" style="background-color: #C92023" onclick="return confirm('¿Esta seguro de que desea eliminar el elemento?');">Eliminar</button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $users->render() }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection