	<!-- form para asignar rol a usuario -->
	<div class="form-group">
		{{ Form::label('name', 'Nombre de la etiqueta') }}
		{{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) }}
	</div>
	<hr>
	<h3>Lista de roles</h3>
	<!-- roles existentes -->
	<div class="form-group">
		<ul class="list-unstyled">
			@foreach($roles as $role)
			<li>
				<label>
					{{ Form::checkbox('roles[]', $role->id, null) }}
					{{ $role->name }}
					<em>({{ $role->description }})</em>
				</label>
			</li>
			@endforeach
		</ul>
	</div>
	<div class="form-group"><br>
		<!-- botones guardar y cancelar -->
		{{ Form::submit('Guardar', ['class' => 'btn btn-success']) }}
		<a href="{{ URL::previous() }}" class="btn btn-secondary">Cancelar</a>
	</div>