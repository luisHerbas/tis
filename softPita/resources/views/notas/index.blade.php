@extends('layouts.app2')
@section('content')

<!-- inicio de examenes que  se calificaran -->
<div class="container"><br>
    <h2 class="text-center">ASIGNATURA: {{$nombre->destino}}</h2><br><br>
    <!-- botones crear -->
    <a class="btn float-right" style="background:#172962; color:white" href="{{ route('nota.create', [ 'convocatoria_id' => $convocatoria_id, 'requerimiento_id' => $nombre->requerimiento_id ])}}">Crear</a><br><br>
    <!-- tabla de examenes -->
    @php
    $sumaTotal=0;
    @endphp
    
    <table class="table table-hover table-bordered">
        <thead class="thead-light">
            <tr>
                <th scope="col" class="text-center">Nombre</th>
                <th scope="col" class="text-center">Porcentaje</th>
                <th scope="col" class="text-center">Accion</th>
            </tr>
        </thead>
        <tbody class="text-center">
            @foreach($notas as $nota)
            <tr>
                <td>{{$nota->nombre_nota}}</td>
                <td>{{$nota->porcentaje}} %</td>
                <td>
                    @php
                    $sumaTotal+=$nota->porcentaje;
                    @endphp
                    <!-- botones editar y borrar -->
                    <a class="btn btn-warning text-white" href="{{ route('nota.edit', [ 'convocatoria_id' => $convocatoria_id, 'requerimiento_id' => $nota->requerimiento_id, 'nota_id'=> $nota->id ])}}">Editar</a>
                    <form method="post" action="{{ route('nota.delete', [ 'convocatoria_id' => $convocatoria_id, 'requerimiento_id' => $nota->requerimiento_id, 'nota_id'=> $nota->id ])}}" style="display:inline">
                        {{ method_field('DELETE') }}
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button class="btn btn-danger" style="background-color: #C92023" type="submit" onclick="return confirm('¿Esta seguro de que desea eliminar este elemento?');">Borrar</button>
                    </form>
                </td>
            </tr>
            @endforeach
            <tr>
                <td>Total</td>
                <td>{{$sumaTotal}} %</td>
            </tr>
        </tbody>
    </table>
    @if($sumaTotal == 100)
        <a class="btn btn-secondary" href="{{ route('notasConocimiento.index',  [ 'convocatoria_id' => $convocatoria_id])}}">Volver</a>
        @else
        <a class="btn btn-secondary disabled" href="{{ route('notasConocimiento.index',  [ 'convocatoria_id' => $convocatoria_id])}}">Volver</a>
        <p style="color:crimson;">Corregir el porcentaje de las notas</p>
        @endif
    
</div>

@endsection

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>