@extends('layouts.app2')
@section('content')

<!-- link bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<!-- formulario edicion requerimiento -->
<div class="container h5"><br>
  <h2 class="text-center">EDITAR REQUERIMIENTO</h2><br><br>
  <form method="post" action="{{ route('reque.update', [ 'convocatoria_id' =>$convocatoria_id, 'requerimiento_id'=> $requerimiento->id ]) }}">
    {{ method_field('PUT') }}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="form-group">
      <!-- lista desplegable cantidad de auxiliares -->
      <label for="">Cantidad de auxiliares</label>
      <select class="form-control" name="cantidad_auxiliares" value="{{ isset($requerimiento->cantidad_auxiliares) ? $requerimiento->cantidad_auxiliares:old('cantidad_auxiliares')}}">
        <option>1</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
        <option>5</option>
        <option>6</option>
        <option>7</option>
        <option>8</option>
        <option>9</option>
        <option>10</option>
        <option>11</option>
        <option>12</option>
        <option>13</option>
        <option>14</option>
        <option>15</option>
      </select>
      {!! $errors->first('cantidad_auxiliares','<div class="invalid-feedback">:message</div>') !!}
    </div>
    <div class="form-group">
      <!-- label: horas_academicas -->
      <label for="horas_academicas" class="control-label h3">{{'Horas academicas: '}}</label>
      <!-- input: horas_academicas -->
      <input type="number" class="form-control {{$errors->has('horas_academicas')?'is-invalid':'' }}" name="horas_academicas" id="horas_academicas" value="{{ isset($requerimiento->horas_academicas) ? $requerimiento->horas_academicas:old('horas_academicas')}}" required min="1" max="80">
      {!! $errors->first('horas_academicas','<div class="invalid-feedback">:message</div>') !!}

    </div>
    <div class="form-group">
      <!-- label: destino -->
      <label for="destino" class="h5">{{'Destino: '}}</label> &nbsp;&nbsp;&nbsp;&nbsp;
      <!-- lista desplegable items -->
      <select id="destino" class="form-control" name="destino">
        @foreach ($items as $item)
        <option value="{{$item->nombre}}">{{$item->nombre}}</option>
        @endforeach
      </select>
    </div>
    <div>
      <!-- botones guardar y cancelar -->
      <button type="submit" class="btn btn-success">Guardar</button>
      <a href="{{ route('detalle.index', [ 'convocatoria_id' =>$convocatoria_id ]) }} " class="btn btn-secondary">Cancelar</a>
    </div>
  </form>
</div>

@endsection