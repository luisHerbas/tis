<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

  <title>Administrador de Convocatorias</title>
  <!-- Styles -->
  <style>
    #header {
      background-color: #172962 !important;
    }

    #footer {
      background-color: #172962 !important;
    }

    #footerCopyright {
      background-color: #020B28 !important;
    }

    #header img {
      width: 40px;
    }

    #main .carousel-inner img {
      max-height: 70vh;
      object-fit: cover;
    }

    #carousel {
      position: relative;
    }

    #carousel .overlay {
      position: absolute;
      top: 0;
      bottom: 0;
      left: 0;
      right: 0;
      background-color: rgba(0, 0, 0, 0.5);
      color: white;
      z-index: 1;
    }

    #carousel .overlay .container,
    #carousel .overlay .row {
      height: 100%;
    }

    #footer a {
      color: white;
    }

    #footerCopyright p {
      color: white;
    }

    #headerCard {
      color: #EF0B0B;

    }

    h5 {
      color: #284DC4;
    }

    #opcionesTitle {
      color: #172962;
    }
  </style>
</head>

<body data-spy="scroll" data-target="#navbar" data-offset="66">

  <!-- Header -->
  <nav id="header" class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
    <div class="container">
      <a class="navbar-brand" href="#">
        <img src="images/UMSS.png">
        Administrador de Convocatorias
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbar">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="#main">Convocatoria actual<span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#opciones">Opciones generales</a>
          </li>
          <!-- <li class="nav-item">
              <a class="nav-link" href="#convocatoriasRecientes">Convocatorias recientes</a>
            </li> -->
          @if (Auth::guest())
          <li class="nav-item">
            <a class="nav-link text-danger" href="{{ route('login') }}">Iniciar sesión</a>
          </li>
          <!-- <li><a href="{{ route('register') }}">Register</a></li> -->
          @else
          <li class="dropdown nav-item">
            <a href="#" class="dropdown-toggle nav-link text-info" data-toggle="dropdown" role="button" aria-expanded="false">
              {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
              <li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                  Salir
                </a>

                <div class="dropdown-divider"></div>

                <a href="/administrador">
                  Administracion
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
                </form>
              </li>
            </ul>
          </li>
          @endif
        </ul>
      </div>

    </div>
  </nav>
  <!-- /Header -->


  <!-- Main -->
  <main id="main">
    <div id="carousel" class="carousel slide carousel-fade" data-ride="carousel" data-pause="false">
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img src="images/Fondo.jpg" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item">
          <img src="images/Fondo2.jpg" class="d-block w-100" alt="...">
        </div>
        <!-- <div class="carousel-item">
            <img src="images/Fcyt.png" class="d-block w-100" alt="...">
          </div> -->
      </div>
      <div class="overlay">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-md-6 offset-md-6 text-center text-md-right">
              <h1>Convocatorias a auxiliares</h1>
              <p class="d-none d-md-block">
                Bienvenido a la p&aacute;gina oficial de convocatorias de auxiliares!
                Nuestro objetivo primordial es ayudarte a lo largo de este proceso,
                facilitandote el acceso a la informaci&oacute;n que necesitas, manejando tus registros
                y brindando apoyo administrativo.
              </p>
              <a href="postulant/register" class="btn btn-primary">Quiero postularme</a>

            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
  <!-- /Main -->

  <!-- Opciones generales -->
  <div class="album py-5" style="background-color: #C92023"><br>
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <div class="card mb-4 box-shadow">
            <img class="card-img-top" src="images/sansi9.jpg" alt="Card image cap">
            <div class="card-body">
              <h5 class="card-title">Convocatoria</h5>
              <p class="card-text">
                Podr&aacute; observar la convocatoria de esta gestión si esta ya fue publicada.
              </p>
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group"></div>
                <a href="{{ route('visualizar.index') }}" class="btn btn-outline-danger">Ver convocatoria</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card mb-4 box-shadow">
            <img class="card-img-top" src="images/sansi5.jpg" alt="Card image cap">
            <div class="card-body">
              <h5 class="card-title">Convocatorias Pasadas</h5>
              <p class="card-text">
                Una colección con todas las convocatorias pasadas divididas por gestiones y areas.
              </p>
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                </div>
                <a href="{{ route('historial.index') }}" class="btn btn-outline-danger">Ver historial de convocatorias</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card mb-4 box-shadow">
            <img class="card-img-top" src="images/sansi8.jpg" alt="Card image cap">
            <div class="card-body">
              <h5 class="card-title">Resultados</h5>
              <p class="card-text">En esta sección aparecerán los resultados finales de la convocatoria actual. ¡Exito!</p>
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                </div>
                <a href="#" class="btn btn-outline-danger">Ver resultados</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /Opciones generales -->

  <!-- Footer -->
  <footer id="footer" class="pb-4 pt-4">
    <div class="container">
      <div class="row text-center">
        <div class="col-12 col-lg">
          <a href="#">Preguntas frecuentes</a>
        </div>
        <div class="col-12 col-lg">
          <a href="#">Cont&aacute;ctanos</a>
        </div>
        <div class="col-12 col-lg">
          <a href="#">T&eacute;rminos y condiciones</a>
        </div>
        <div class="col-12 col-lg">
          <a href="/administrador">Privacidad</a>
        </div>
      </div>
    </div>
  </footer>
  <!-- Footer -->


  <!-- Copyright -->
  <footer id="footerCopyright" class="">
    <div class="container">
      <div class="row text-center">
        <div class="col-12 col-lg-6 offset-lg-3">
          <p><small>Contacto: +591 60756038 - SoftpitaDmani@gmail.com <br>
              Copyright © 2020 - SoftPita D'Man&iacute; - Todos los derechos reservados.</small></p>
          <p class="align-middle"><small>¡Con la calidad de la Llajta!</small></p>
        </div>
      </div>
    </div>
  </footer>
  <!-- /Copyright -->

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  <!-- <script src="index.js"></script> -->
</body>

</html>