<!DOCTYPE html>
<html lang="es">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Convocatorias</title>
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="css/bootstrap.css">
  <!-- Estilos en Css -->
  <style>
    .hoverable {
      display: inline-block;
      backface-visibility: hidden;
      vertical-align: middle;
      position: relative;
      box-shadow: 0 0 1px rgba(0, 0, 0, 0);
      transform: translateZ(0);
      transition-duration: .3s;
      transition-property: transform;
    }

    .hoverable:before {
      position: absolute;
      pointer-events: none;
      z-index: -1;
      content: '';
      top: 100%;
      left: 5%;
      height: 10px;
      width: 90%;
      opacity: 0;
      background: -webkit-radial-gradient(center, ellipse, rgba(255, 255, 255, 0.35) 0%, rgba(255, 255, 255, 0) 80%);
      background: radial-gradient(ellipse at center, rgba(255, 255, 255, 0.35) 0%, rgba(255, 255, 255, 0) 80%);
      /* W3C */
      transition-duration: 0.3s;
      transition-property: transform, opacity;
    }

    .hoverable:hover,
    .hoverable:active,
    .hoverable:focus {
      transform: translateY(-5px);
    }

    .hoverable:hover:before,
    .hoverable:active:before,
    .hoverable:focus:before {
      opacity: 1;
      transform: translateY(-5px);
    }

    /* --- flexslider --- */
    .site-footer {
      background-color: #172962;
      padding: 45px 0 20px;
      font-size: 15px;
      line-height: 24px;
      color: #737373;
    }

    .site-footer hr {
      border-top-color: #bbb;
      opacity: 0.5
    }

    .site-footer hr.small {
      margin: 20px 0
    }

    .site-footer h6 {
      color: #fff;
      font-size: 16px;
      text-transform: uppercase;
      margin-top: 5px;
      letter-spacing: 2px
    }

    .site-footer a {
      color: #737373;
    }

    .site-footer a:hover {
      color: #3366cc;
      text-decoration: none;
    }

    .footer-links {
      padding-left: 0;
      list-style: none
    }

    .footer-links li {
      display: block
    }

    .footer-links a {
      color: #737373
    }

    .footer-links a:active,
    .footer-links a:focus,
    .footer-links a:hover {
      color: #3366cc;
      text-decoration: none;
    }

    .footer-links.inline li {
      display: inline-block
    }

    .site-footer .social-icons {
      text-align: right
    }

    .site-footer .social-icons a {
      width: 40px;
      height: 40px;
      line-height: 40px;
      margin-left: 6px;
      margin-right: 0;
      border-radius: 100%;
      background-color: #33353d
    }

    .copyright-text {
      margin: 0
    }

    @media (max-width:991px) {
      .site-footer [class^=col-] {
        margin-bottom: 30px
      }
    }

    @media (max-width:767px) {
      .site-footer {
        padding-bottom: 0
      }

      .site-footer .copyright-text,
      .site-footer .social-icons {
        text-align: center
      }
    }

    .social-icons {
      padding-left: 0;
      margin-bottom: 0;
      list-style: none
    }

    .social-icons li {
      display: inline-block;
      margin-bottom: 4px
    }

    .social-icons li.title {
      margin-right: 15px;
      text-transform: uppercase;
      color: #96a2b2;
      font-weight: 700;
      font-size: 13px
    }

    .social-icons a {
      background-color: #eceeef;
      color: #818a91;
      font-size: 16px;
      display: inline-block;
      line-height: 44px;
      width: 44px;
      height: 44px;
      text-align: center;
      margin-right: 8px;
      border-radius: 100%;
      -webkit-transition: all .2s linear;
      -o-transition: all .2s linear;
      transition: all .2s linear
    }

    .social-icons a:active,
    .social-icons a:focus,
    .social-icons a:hover {
      color: #fff;
      background-color: #29aafe
    }

    .social-icons.size-sm a {
      line-height: 34px;
      height: 34px;
      width: 34px;
      font-size: 14px
    }

    .social-icons a.facebook:hover {
      background-color: #3b5998
    }

    .social-icons a.twitter:hover {
      background-color: #00aced
    }

    .social-icons a.linkedin:hover {
      background-color: #007bb6
    }

    .social-icons a.dribbble:hover {
      background-color: #ea4c89
    }

    @media (max-width:767px) {
      .social-icons li.title {
        display: block;
        margin-right: 0;
        font-weight: 600
      }
    }

    #featured .flexslider {
      padding: 0;
      margin: 50px 0 30px;
      background: #fff;
      position: relative;
      zoom: 1;
    }

    .flex-caption {
      background: none;
      -ms-filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#4C000000, endColorstr=#4C000000);
      filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#4C000000, endColorstr=#4C000000);
      zoom: 1;
    }

    .flex-caption {
      bottom: 35px;
      background-color: rgba(0, 0, 0, 0.8);
      color: #fff;
      margin: 0;
      padding: 25px 25px 25px 30px;
      position: absolute;
      right: 0;
      width: 295px;
    }

    .flex-caption h3 {
      color: #fff;
      letter-spacing: 1px;
      margin-bottom: 8px;
      text-transform: uppercase;
    }

    .flex-caption p {
      margin: 0 0 15px;
    }



    /* ===================================
    7. Section: call action
    ==================================== */
    section.callaction {
      background: #f9f9f9;
      padding: 50px 0 0 0;
    }


    /* Carousel base class */
    .carousel {
      margin-bottom: 3rem;
      margin-left: 7rem;
      margin-right: 7rem;
    }

    /* Since positioning the image, we need to help out the caption */
    .carousel-caption {
      bottom: 10rem;
      z-index: 10;
      background: rgba(0, 0, 0, 0.8);
    }

    /* Declare heights because of positioning of img element */
    .carousel-item {
      height: 32rem;

      background-color: #777;
    }

    .carousel-item>img {
      position: absolute;
      top: 0;
      left: 0;
      width: 85rem;
      height: 32rem;
    }

    :root {
      --jumbotron-padding-y: 1rem;
    }

    .jumbotron {
      padding-top: var(--jumbotron-padding-y);
      padding-bottom: var(--jumbotron-padding-y);
      margin-bottom: 0;
      background-color: #fff;
    }

    @media (min-width: 768px) {
      .jumbotron {
        padding-top: calc(var(--jumbotron-padding-y) * 2);
        padding-bottom: calc(var(--jumbotron-padding-y) * 2);
      }
    }

    .jumbotron p:last-child {
      margin-bottom: 0;
    }

    .jumbotron-heading {
      font-weight: 300;
    }

    .jumbotron .container {
      max-width: 40rem;
    }

    footer {
      padding-top: 3rem;
      padding-bottom: 3rem;
    }

    footer p {
      margin-bottom: .25rem;
    }

    .box-shadow {
      box-shadow: 0 .25rem .75rem rgba(0, 0, 0, .05);
    }

    .navbar-nav>li {
      padding-left: 20px;
      padding-right: 20px;
    }

    @keyframes bounce-animation {
      16.65% {
        -webkit-transform: translateY(8px);
        transform: translateY(8px);
      }

      33.3% {
        -webkit-transform: translateY(-6px);
        transform: translateY(-6px);
      }

      49.95% {
        -webkit-transform: translateY(4px);
        transform: translateY(4px);
      }

      66.6% {
        -webkit-transform: translateY(-2px);
        transform: translateY(-2px);
      }

      83.25% {
        -webkit-transform: translateY(1px);
        transform: translateY(1px);
      }

      100% {
        -webkit-transform: translateY(0);
        transform: translateY(0);
      }
    }

    .bounce {
      animation-name: bounce-animation;
      animation-duration: 2s;
    }

    #header {
      background-color: #172962 !important;
    }

    .imgRedonda {
      width: 300px;
      height: 300px;
      border-radius: 150px;
    }
  </style>

</head>

<body>
  <header class="blog-header">
    <!-- navbar responsive -->
    <nav id="header" class="navbar navbar-expand-lg navbar-dark bg-dark ">
      <div class="container">
        <a class="navbar-brand" href="/">Administrador de Convocatorias</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <!-- navbar azul -->
        <div class="collapse navbar-collapse" id="navbar">
          <ul class="navbar-nav ml-auto">
            @if (Auth::guest())
            <li class="nav-item">
              <a class="nav-link text-white" href="{{ route('login') }}">Iniciar sesión</a>
            </li>
            @else
            <li class="dropdown nav-item">
              <a href="#" class="dropdown-toggle nav-link text-white" data-toggle="dropdown" role="button" aria-expanded="false">
                {{ Auth::user()->name }} <span class="caret"></span>
              </a>
              <!-- lista desplegable sesion -->
              <ul class="dropdown-menu" role="menu">
                <li>
                  <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    Salir
                  </a>
                  <div class="dropdown-divider"></div>
                  <a href="/administrador">Administracion</a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                  </form>
                </li>
              </ul>
            </li>
            @endif
          </ul>
        </div>
      </div>
    </nav>
    <!-- imagenes navbar -->
    <div class="container">
      <div class="row flex-nowrap justify-content-between align-items-center">
        <div class="col-4 pt-1">
          <img src="images/umss_logo.png" width="240" height="90" alt="">
        </div>
        <div class="col-4 d-flex justify-content-end align-items-center">
          <img src="images/fcyt1.png" width="100" height="90" alt="">
        </div>
      </div>
    </div>
  </header>

  <!-- navbar rojo -->
  <div class="nav-scroller py-0 mb-5 sticky-top" style="background-color: #C92023">
    <nav class="nav d-flex justify-content-center">
      <li class="nav-item"><a class="nav-link hoverable" href="postulant/register">
          <font size="5" face="times new roman" color=#FFFFFF>Postulación</font>
        </a></li>
      <li class="nav-item"><a class="nav-link hoverable" href="{{ route('historial.index') }}">
          <font size="5" face="times new roman" color=#FFFFFF>Convocatorias pasadas</font>
        </a></li>
      <li class="nav-item"><a class="nav-link hoverable" href="/pdf">
          <font size="5" face="times new roman" color=#FFFFFF>Resultados</font>
        </a></li>
    </nav>
  </div>

  <main>
    <!-- cuerpo -->
    <section id="place-time">
      <div class="container">
        <div class="row">
          <div class="col-12 col-lg-6 pl-0 pr-0">
            <!-- imagen cochabamba -->
            <img src="images/Cochabamba.jpg" alt="Cochabamba1" style="max-width: 100%">
          </div>
          <div class="col col-lg-6 pt-4 pb-4">
            <h2>SoftPita D'Maní</h2>
            <hr>
            <!-- descripcion softpita -->
            <p>
              Softpita es una empresa desarrolladora de software y consultoría informática, basados tanto en tecnologías Web como tradicionales. Nuestras principales áreas de aplicación son: desarrollos de sistemas a medida, sitios Web, e-Commerce, Email Marketing, portales, administración de base de datos, seguridad informática, etc,para obtener soluciones de calidad y ajustadas a los requerimientos de nuestros clientes.
            </p>
            <p>Somos un grupo ágil de personas calificadas, con la necesidad permanente de superarnos día a día. Conscientes de que nuestra iniciativa, sumada a la capacidad de servir a los clientes, son claves para el éxito del emprendimiento.</p>
            <a href="https://es.wikipedia.org/wiki/Honolulu" class="btn btn-outline-light">Conoce más</a>
          </div>
        </div>
      </div>
    </section>
    <br>
    <h2 class="text-center">Equipo directivo</h2>
    <hr>
    <br><br>
    <!-- descripcion y datos equipo softpita -->
    <section>
      <div class="container">
        <div class="row">
        </div>
        <div class="row">
          <div class="col-12 col-md-6 col-lg-2 mb-4 offset-1">
            <div class="card">
              <img src="images/Dani.png" class="card-img-top" alt="...">
              <div class="card-body">
                <div class="badges">
                  <span class="badge badge-success">Scrum</span>
                  <span class="badge badge-danger">Html</span>
                </div>
                <h5 class="card-title mb-0 ">Daniela Santa Cruz</h5>
                <p class="card-text mt-2">Scrum Master</p>
                <p class="card-text mt-2">Developer</p><br>
              </div>
            </div>
          </div>
          <div class="col-12 col-md-6 col-lg-2 mb-4">
            <div class="card">
              <img src="images/Edgar1.jpeg" class="card-img-top" alt="..." style="height:158px">
              <div class="card-body">
                <div class="badges">
                  <span class="badge badge-danger">GitLab</span>
                  <span class="badge badge-warning">Bootstrap</span>
                </div>
                <h5 class="card-title mb-0">Edgar Balderrama</h5>
                <p class="card-text mt-2">Representante legal</p>
                <p class="card-text mt-2">Developer</p>
              </div>
            </div>
          </div>

          <div class="col-12 col-md-6 col-lg-2 mb-4">
            <div class="card">
              <img src="images/Diego.jpeg" class="card-img-top" alt="..." style="height:158px">
              <div class="card-body">
                <div class="badges">
                  <span class="badge badge-success">PHP</span>
                  <span class="badge badge-primary">Laravel</span>
                </div>
                <h5 class="card-title mb-0">Diego Choque</h5>
                <p class="card-text mt-2">Developer</p>
                
                <p class="card-text mt-2">Front end y &nbsp;Back end</p>
              </div>
            </div>
          </div>
          <div class="col-12 col-md-6 col-lg-2 mb-4">
            <div class="card">
              <img src="images/Luis.jpg" class="card-img-top" alt="...">
              <div class="card-body">
                <div class="badges">
                  <span class="badge badge-dark">Taiga</span>
                  <span class="badge badge-primary">Eloquent</span>
                </div>
                <h5 class="card-title mb-0">Luis Herbas</h5>
                <p class="card-text mt-2">Product owner</p><br><br>
              </div>
            </div>
          </div>
          <div class="col-12 col-md-6 col-lg-2 mb-4">
            <div class="card">
              <img src="images/Sandra2.jpeg" class="card-img-top" alt="..." style="height:158px">
              <div class="card-body">
                <div class="badges">
                  <span class="badge badge-success">QA</span>
                  <span class="badge badge-success">Selenium</span>
                </div>
                <h5 class="card-title mb-0">Sandra Renjel</h5>
                <p class="card-text mt-2">Tester</p>
                <p class="card-text mt-2">Documentary</p><br>
              </div>
            </div>
          </div>
        </div>
        <hr>
    </section>
    <br><br>

    <section>
      <div class="container">
        <div class="row">
          <!-- invitacion -->
          <div class="col col-lg-6 pt-4 pb-4">
            <h2>Unete a nosotros</h2>
            <hr>
            <p>
              Softpita D'Maní tiene las puertas abiertas a personas que trabajen en el mundo del software,
              tanto si eres developer como QA. Somos una empresa seria y responsable, con grandes ambiciones
              y un gran equipo de trabajo. ¡No esperes más!
            </p>
            <a href="https://es.wikipedia.org/wiki/Honolulu" class="btn btn-outline-light">Conoce más</a>
          </div>
          <div class="col-12 col-lg-6 pl-0 pr-0">
            <img src="images/Sopita.jpg" alt="Cochabamba" style="max-width: 100%">
          </div>
        </div>
      </div>
      <hr>
      <br><br>
    </section>

    <section>
      <!-- slogan -->
      <blockquote class="blockquote text-center">
        <p class="mb-0">Software hecho con la calidad de una sopita de maní</p>
        <footer class="blockquote-footer">Softpita D'Maní<cite title="Source Title"></cite></footer>
      </blockquote>
    </section>
  </main>

  <!-- Site footer -->
  <footer class="site-footer">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-6">
          <h6>Sobre Nosotros</h6>
          <p class="text-justify">La Universidad Mayor de San Simón (UMSS) cubre el área de la educación superior con sus funciones de enseñanza-aprendizaje, investigación científica y tecnológica e interacción social universitaria.</p>
        </div>

        <div class="col-xs-12 col-md-6 text-md-right">
          <h6>Enlaces</h6>
          <ul class="footer-links">
            <li><a href="/sobreNosotros">Sobre Nosotros</a></li>
            <li><a target="_blank" href="http://www.cs.umss.edu.bo/">CS</a></li>
            <li><a target="_blank" href="http://www.fcyt.umss.edu.bo/">FCyT</a></li>
            <li><a target="_blank" href="http://sagaa.fcyt.umss.edu.bo/admision/noticias.php">SAGAA</a></li>
            <li><a target="_blank" href="http://websis.umss.edu.bo/home.asp">webSISS</a></li>

          </ul>
        </div>
      </div>
      <hr>
    </div>
    <div class="container">
      <div class="row">
        <div class="col d-flex justify-content-center text-white">
          <p class="copyright-text">Derechos Reservados © 2020 · Universidad Mayor de San Simón · SoftpitaDmani
          </p>
        </div>
      </div>
    </div>
  </footer>

  <!-- jQuery first, then Tether, then Bootstrap JS. -->
  <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
  <script src="js/bootstrap.min.js"></script>
</body>

</html>