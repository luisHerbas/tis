@extends('layouts.app2')
@section('content')

<!-- tabla de resultados finales -->
<div class="container"><br>
    <h2 class="text-center">RESULTADOS FINALES</h2><br><br>
    <p> <strong> Nota: Mientras los resultados de conocimientos y meritos no se publiquen, no se mostraran en la seccion de avisos.</strong> </p>
    <div class="container">
        <!-- metodos para publicar o dejar de publicar resultados -->
        @foreach($convocatorias as $convocatoria)
        @if($convocatoria->publicacion_finales==1)
        <a href="{{ url('notasfinales/publicar') }}" type="button" class="btn btn-success float-right">Publicar</a>
        @elseif($convocatoria->publicacion_finales == 2)
        <a href="{{ url('notasfinales/publicar') }}" type="button" class="btn btn-danger float-right">Dejar de publicar</a>
        @endif
        @endforeach
        
    </div><br>
    <div class="panel-body"><br>
        <table class="table table-hover table-bordered">
            <thead class="thead-light">
                <tr>
                    <th scope="col" class="text-center">CODIGO SIS</th>
                    <th scope="col" class="text-center">APELLIDO</th>
                    <th scope="col" class="text-center">NOMBRE</th>
                    <th scope="col" class="text-center">ASIGNATURA</th>
                    <th scope="col" class="text-center">NOTA CONOCIMIENTO</th>
                    <th scope="col" class="text-center">NOTA MERITOS</th>
                    <th scope="col" class="text-center">NOTA FINAL</th>
                </tr>
            </thead>
            <tbody class="text-center">
                @foreach($finales as $final)
                <tr>
                    <td>{{$final->cod_sis}}</td>
                    <td>{{$final->last_name}}</td>
                    <td>{{$final->name}}</td>
                    <td>{{$final->destino}}</td>
                    <td>{{$final->nota_conocimientos}}</td>
                    <td>{{$final->nota}}</td>
                    <td>{{$final->nota_final}}</td>
                </tr>
                @endforeach
            </tbody>
        </table><br><br>
    </div>
</div>

@endsection

<!-- script JavaScript -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>