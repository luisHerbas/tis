@extends('layouts.app3')
@section('content')

<!-- inicio de historial de convocatorias -->
<div class="container"><br>
    <div class="h1 text-center">
        <label for="fecha_publicacion" class="h1">SELECCIONE UN DEPARTAMENTO</label> &nbsp;&nbsp;&nbsp;&nbsp; <br><br>
    </div>
    <!-- boton volver home -->
    <a href="{{ route('home') }}" class="btn btn-secondary float-right">Volver al inicio</a><br><br>
    <div class="form-group">
        <!-- lista desplegable de departamentos -->
        <label for="departamento" class="h3">{{'Departamento: '}}</label> &nbsp;&nbsp;&nbsp;&nbsp;
        {!! Form::select('departamento', $departamentos, null, ['placeholder' => 'Selecciona un departamento', 'class' => 'form-control', 'id'=>'departamento']) !!}
    </div>
    <div class="form-group">
        <!-- lista desplegable de subunidades -->
        <label for="subunidad" class="h3">{{'Subunidad: '}}</label>
        <select class="form-control {{$errors->has('subunidad')?'is-invalid':'' }}" name="subunidad" id="subunidad">
        </select>
        {!! $errors->first('subunidad','<div class="invalid-feedback">:message</div>') !!}
    </div>
    <!-- tabla de convocatorias -->
    <table class="table table-hover table-bordered">
        <thead class="thead-light">
            <tr>
                <th scope="col" class="text-center">TITULO DE LA CONVOCATORIA</th>
                <th scope="col" class="text-center">FECHA DE PUBLICACION</th>
            </tr>
        </thead>
        <tbody class="text-center" name="convocatoria" id="convocatoria">
        </tbody>
    </table><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

    @endsection

    @section('script')
    <!-- script para verificar subunidad y departamento -->
    <script>
        $("#departamento").change(function(event) {
            jQuery.get("subunidades/" + event.target.value + "", function(response, departamento) {
                console.log(response);
                $("#subunidad").empty();
                $("#subunidad").append("<option value=''>Seleccione una subunidad </option>");
                for (i = 0; i < response.length; i++) {
                    $("#subunidad").append("<option value='" + response[i].id + "'> " + response[i].name + "</option>");
                }
            });
        });
        $("#subunidad").change(function(event) {
            jQuery.get("convocatorias/" + event.target.value + "", function(response, subunidad) {
                console.log(response);
                $("#convocatoria").empty();
                for (i = 0; i < response.length; i++) {
                    $("#convocatoria").append("<tr><td value='" + response[i].id + "'> <a href='" + response[i].id + "'>" + response[i].titulo_convocatoria + "</td></a><td> " + response[i].fecha_publicacion + "</td></tr>");
                }
            });
        });
    </script>

    @endsection