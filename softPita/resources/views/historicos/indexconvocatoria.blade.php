<!-- estilos usados -->
<style>
    div {
        word-wrap: break-word;
    }

    #tablerequerimientos th,
    tr,
    td {
        word-wrap: break-word;
    }
</style>

@extends('layouts.app3')
<!-- link de bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
@section('content')
<!-- visualizacion de convocatoria "convocatorias pasadas" una vez elegida la convocatoria y el departamento y la subunidad 
a la que pertenecen-->
<div class="container"><br>
    <!-- componentes de convocatoria -->
    <section id="prueba">
        <h2 class="text-center ">{{ $titulos->titulo_convocatoria}}</h2> <br /> <br />
    </section>
    <p class="">Descripcion: El {{$departamentos->name}} convoca al concurso de méritos y examen de competencia para la provision de Auxiliares del Departamento</p>
    <!-- tabla de requerimientos -->
    <p class=""><strong>1.- REQUERIMIENTOS</strong></p>
    <table class="table table-bordered text-center" id="tablerequerimiento">
        <thead class="thead-light">
            <tr>
                <th class="text-center">Items</th>
                <th class="text-center">Cantidad auxiliares</th>
                <th class="text-center">Horas Academicas</th>
                <th class="text-center">Destino</th>
            </tr>
        </thead>
        <tbody>
            @foreach($requerimientos as $requerimiento)
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$requerimiento->cantidad_auxiliares}} aux</td>
                <td>{{$requerimiento->horas_academicas}} Hrs/mes</td>
                <td>{{$requerimiento->destino}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <!-- tabla de requisitos -->
    <p class=""><strong>2.- REQUISITOS</strong></p>
    <table class="table table-light ">
        <tbody>
            @foreach($requisitos as $requisito)
            <li style='margin-left: 3em' class="">{{$requisito->requisito}}</li>
            @endforeach
        </tbody>
    </table>
    <!-- tabla de documentos a presentar -->
    <p class=""><strong>3.- DOCUMENTOS A PRESENTAR</strong></p>
    <table class="table table-light ">
        <tbody>
            @foreach($documentos as $documento)
            <li style='margin-left: 3em' class="">{{$documento->documento}}</li>
            @endforeach
        </tbody>
    </table>

    <p class=""><strong>NOTA: Toda la documentación se legalizará gratuitamente en secretaría del {{$departamentos->name}} (presentar original y fotocopias). La documentación no será devuelta. </strong></p>
    <p class=""><strong>4.- DE LA FORMA</strong></p>
    <p class="">Presentación de la documentación en sobre manila cerrado y con el rótulo generado al momento del registro</p>
    <p class=""><strong>5.- FECHA DE PRESENTACION</strong></p>
    <p class="">Presentación de la documentación en sobre manila cerrado y con el rótulo generado al momento del registro en Secretaría del {{$departamentos->name}}, hasta la hora y fecha especificada en el punto <strong>FECHAS DE PRUEBA</strong> </p>
    <p class=""><strong>6.- DE LA CALIFICACION DE MERITOS</strong></p>
    <p class="">La calificacion de meritos se basara en los documentos presentados por el postulante y se realizara sobre la base de 100 puntos que representa el <strong>{{$titulos->porcentaje_meritos}}%</strong> del puntaje final y se respondera de la siguiente manera:</strong> </p>

    <!-- tabla de meritos: rendimiento academico -->
    <table class="table table-bordered">
        <thead class="thead-light">
            <tr>
                <th>Rendimiento Academico </th>
                @php
                $suma2=0;
                @endphp
                <th>
                    @foreach($meritos as $merito)
                    @php
                    $suma2+=$merito->porcentaje;
                    @endphp
                    @endforeach
                    {{$suma2}}
                    %</th>
            </tr>
        </thead>
        <tbody>
            @foreach($meritos as $merito)
            <tr>
                <td>{{$merito->nombre}}</td>
                <td>{{$merito->porcentaje}} %</td>
            </tr>
            @endforeach
        </tbody>
        <!-- tabla de meritos de experiencia universitaria -->
        <thead class="thead-light">
            <tr>
                <th>Documentos de experiencia universitaria</th>
                @php
                $suma1=0;
                @endphp
                <th>
                    @foreach($meritos1 as $merito1)
                    @php
                    $suma1+=$merito1->porcentaje;
                    @endphp
                    @endforeach
                    {{$suma1}}
                    %</th>
            </tr>
        </thead>
        <tbody>
            @foreach($meritos1 as $merito1)
            <tr>
                <td>{{$merito1->nombre}}</td>
                <td>{{$merito1->porcentaje}} %</td>
            </tr>
            @endforeach
        </tbody>
        <!-- tabla de meritos: experiencia extrauniversitaria -->
        <thead class="thead-light">
            <tr>
                <th>Documentos de experiencia extrauniversitaria</th>
                @php
                $suma=0;
                @endphp
                <th>
                    @foreach($mertos as $merto)
                    @php
                    $suma+=$merto->porcentaje;
                    @endphp
                    @endforeach
                    {{$suma}}
                    %</th>
            </tr>
        </thead>
        <tbody>
            @foreach($mertos as $merto)
            <tr>
                <td>{{$merto->nombre}}</td>
                <td>{{$merto->porcentaje}} %</td>
            </tr>
            @endforeach
        </tbody>
        <!-- caluclo de porcentaje total -->
        <thead class="thead-light">
            <tr>
                <th>Porcentaje Total</th>
                <th>{{$sumaTotal=$suma+$suma1+$suma2}} %</th>
            </tr>
        </thead>
    </table>

    <p class=""><strong>7.- Calificacion de conocimientos</strong></p>
    <p class="">La calificación de conocimientos se realiza sobre la base de 100 puntos, equivalentes al <strong>{{$titulos->porcentaje_conocimiento}}%</strong> de la calificación final. Se realizarán las siguiente pruebas:</p>
    <p class="">a) Examen escrito de conocimientos (prueba de pre selección) ..............................................................40%</p>
    <p class="">b) Examen oral, donde se evaluarán aspectos didácticos y pedagógicos sobre conocimiento y dominio de la materia.</p>
    <p class="">Tendrá una duración máxima de 25 min: 15 min de exposición y 10 min de preguntas...........60%</p>
    <p class=""><strong>8.- De los tribunales</strong></p>
    <p class="">Lo honorables Consejos de Carrera del {{$departamentos->name}} designarán respectivamente; para la calificación de méritos: 1 docente y 1 delegado estudiante y para la comisión de conocimientos: 1 docente por asignatura convocada más 1 estudiante veedor.</p>
    <!-- opcional si se desea mostrar los docentes que estaran en la comisiones 
        {{$titulos->comision_conocimiento}}
            {{$titulos->comision_merito}} -->
    <p class=""><strong>9.- Fechas de prueba</strong></p>
    <p class="">Las pruebas escritas serán sobre el contenido de la materia a la que se postula con nota de aprobación mayor o igual a 51. Las pruebas orales, se tomarán sólo a los postulantes que hayan vencido la prueba escrita y de acuerdo a pertinencia y contenido de la materia a la que se postula. Fechas importantes a considerar:</p>

    <!-- tabla de fechas -->
    <table class="table table-bordered text-center">
        <thead class="thead-light">
            <tr>
                <th>Eventos</th>
                <th>Fechas</th>
            </tr>
        </thead>
        <tbody>
            @foreach($fechas as $fecha)
            <tr>
                <td>{{$fecha->evento}}</td>
                <td>{{$fecha->fecha}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>

    <p class=""><strong>10.- Del nombramiento</strong></p>
    <p class="">Los nombramientos de auxiliar universitario titular recaerán sobre aquellos postulantes que hubieran aprobado y obtenido mayor calificación. Caso contrario se precederá con el nombramiento de aquel que tenga la calificación mayor como auxiliar invitado. Cabe resaltar que un auxiliar invitado sólo tendrá nombramiento por los periodos académicos de la gestion <strong>{{$titulos->gestion}} </strong> </p><br />


    

    <p> <strong>POSTULANTES INHABILITADOS</strong> </p> 

<table class="table table-hover table-bordered">
    <thead class="thead-light">
        <tr>
            <th scope="col" class="text-center">CODIGO SIS</th>
            <th scope="col" class="text-center">APELLIDO</th>
            <th scope="col" class="text-center">NOMBRE</th>
            <th scope="col" class="text-center">ASIGNATURA</th>
            <th scope="col" class="text-center">ESTADO</th>
            <th scope="col" class="text-center">OBSERVACION</th>
        </tr>
    </thead>
    <tbody class="text-center">
        @foreach($inhabilitados as $Inhabilitado)
        <tr>
            <td>{{$Inhabilitado->cod_sis}}</td>
            <td>{{$Inhabilitado->last_name}}</td>
            <td>{{$Inhabilitado->name}}</td>
            <td>{{$Inhabilitado->destino}}</td>
            <td>Inhabilitado</td>
            <td>{{$Inhabilitado->observaciones}}</td>
        </tr>
        @endforeach
    </tbody>
</table><br>

    <!-- tabla de resultados conocimiento -->
    <p class=""><strong>RESULTADOS CONOCIMIENTO</strong></p>
    <table class="table table-bordered text-center">
        <thead class="thead-light">
            <tr>
                <th>Asignatura</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Nombre nota</th>
                <th>Nota</th>
            </tr>
        </thead>
        <tbody>
            @foreach($postulantes as $postul)
            <tr>
                <td>{{$postul->destino}}</td>
                <td>{{$postul->name}}</td>
                <td>{{$postul->last_name}}</td>
                <td>{{$postul->nombre_nota}}</td>
                <td>{{$postul->notaC}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>

    <!-- tabla de resultados finales -->
    <p class=""><strong>RESULTADOS FINALES</strong></p>
    <table class="table table-bordered text-center">
        <thead class="thead-light">
            <tr>
                <th>Asignatura</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Nota conocimientos</th>
                <th>Nota meritos</th>
                <th>Nota final</th>
            </tr>
        </thead>
        <tbody>
            @foreach($finales as $postulante)
            <tr>
                <td>{{$postulante->destino}}</td>
                <td>{{$postulante->name}}</td>
                <td>{{$postulante->last_name}}</td>
                <td>{{$postulante->nota_conocimientos}}</td>
                <td>{{$postulante->nota}}</td>
                <td>{{$postulante->nota_final}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>



    <div class="text-center">
        <!-- boton para volver atras -->
        <a href="{{ URL::previous() }} " class="btn btn-secondary">Regresar</a><br /><br />
    </div>

</div>
@endsection