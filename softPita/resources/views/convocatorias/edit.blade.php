@extends('layouts.app2')
@section('content')

<script>
    function calcular_total() {
        nota_final = 0
        $(".nota_parcial").each(
            function(index, value) {
                nota_final = nota_final + eval($(this).val());
            }
        );
        $("#total").val(nota_final);
    }
</script>
<!-- link de bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<!-- formulario para editar una convocatoria -->
<div class="container"><br>
    <h2 class="text-center">MODIFICAR CONVOCATORIA</h2>
    <br><br>
    <!-- se obtiene la convocatoria a editar por su id -->
    <form action="{{ url('/convocatorias/' . $convocatoria->id) }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        {{ method_field('PATCH') }}

        <div class="form-group">
            <!-- label: titulo de la convocatoria -->
            <label for="titulo_convocatoria" class="control-label h3">{{'Titulo de la Convocatoria: '}}</label>
            <!-- input: titulo de convocatoria -->
            <input type="text" class="form-control {{$errors->has('titulo_convocatoria')?'is-invalid':'' }}" name="titulo_convocatoria" id="titulo_convocatoria" value="{{ isset($convocatoria->titulo_convocatoria) ? $convocatoria->titulo_convocatoria:old('titulo_convocatoria')}}">
            {!! $errors->first('titulo_convocatoria','<div class="invalid-feedback">:message</div>') !!}

        </div>

        <div class="form-group">
            <!-- label: fecha de publicacion -->
            <label for="fecha_publicacion" class="h3">{{'Fecha de publicacion: '}}</label> &nbsp;&nbsp;&nbsp;&nbsp;
            <!-- input(date): fecha de publicacion  -->
            <input type="date" class=" h5 {{$errors->has('fecha_publicacion')?'is-invalid':'' }}" name="fecha_publicacion" id="fecha_publicacion" value="{{ isset($convocatoria->fecha_publicacion) ? $convocatoria->fecha_publicacion:old('fecha_publicacion')}}">
            {!! $errors->first('fecha_publicacion','<div class="invalid-feedback">:message</div>') !!}
        </div>

        <div class="form-group">
            <!-- label: departamento -->
            <label for="departamento" class="h3">{{'Departamento: '}}</label> &nbsp;&nbsp;&nbsp;&nbsp;
            <!-- lista desplegable de departamentos -->
            {!! Form::select('departamento', $departamentos, null, ['placeholder' => 'Selecciona un departamento', 'class' => 'form-control', 'id'=>'departamento']) !!}
        </div>

        <div class="form-group">
            <!-- label: subunidad -->
            <label for="subunidad" class="h3">{{'Subunidad: '}}</label>
            <!-- lista desplegable de subunidad -->
            <select class="form-control {{$errors->has('subunidad')?'is-invalid':'' }}" name="subunidad" id="subunidad" value="{{ isset($subunidades->id) ? $subunidades->id:old('id')}}" required>
            </select>
            {!! $errors->first('subunidad','<div class="invalid-feedback">:message</div>') !!}
        </div>

        <div class="form-group">
            <!-- label: porcentaje de conocimientos -->
            <label for="porcentaje_conocimiento" class="control-label h3">{{'Porcentaje Conocimientos: '}}</label>
            <!-- input: porcentaje de conocimientos -->
            <input type="number" class="form-control nota_parcial {{$errors->has('porcentaje_conocimiento')?'is-invalid':'' }}" name="porcentaje_conocimiento" id="porcentaje_conocimiento" value="{{ isset($convocatoria->porcentaje_conocimiento) ? $convocatoria->porcentaje_conocimiento:old('porcentaje_conocimiento')}}" required min="1" max="100">
            {!! $errors->first('porcentaje_conocimiento','<div class="invalid-feedback">:message</div>') !!}

        </div>

        <div class="form-group">
            <!-- label: porcentaje meritos -->
            <label for="porcentaje_meritos" class="control-label h3">{{'Porcentaje Meritos: '}}</label>
            <!-- input: porcentaje meritos -->
            <input type="number" class="form-control nota_parcial {{$errors->has('porcentaje_meritos')?'is-invalid':'' }}" name="porcentaje_meritos" id="porcentaje_meritos" value="{{ isset($convocatoria->porcentaje_meritos) ? $convocatoria->porcentaje_meritos:old('porcentaje_meritos')}}" required min="1" max="100">
            {!! $errors->first('porcentaje_meritos','<div class="invalid-feedback">:message</div>') !!}

        </div>
        <div>
            <!-- label: porcentaje total -->
            <label for="total" class="h3">Total Porcentaje: <input type="number" class="form-control" name="total" id="total" value="total" required min="100" max="100" />
        </div>

        <div class="form-group">
            <!-- label: comision merito -->
            <label for="comision_merito" class="h3">{{'Comision merito: '}}</label> &nbsp;&nbsp;&nbsp;&nbsp;
            <!-- lista desplegable de docentes -->
            <select id="comision_merito" class="form-control" name="comision_merito">
                @foreach ($docentes as $docente)
                <option value="{{$docente->nombre}}">{{$docente->nombre}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <!-- label: comision conocimiento -->
            <label for="comision_conocimiento" class="h3">{{'Comision conocimiento: '}}</label> &nbsp;&nbsp;&nbsp;&nbsp;
            <!-- lista desplegable de docentes -->
            <select id="comision_conocimiento" class="form-control" name="comision_conocimiento">
                @foreach ($docentes as $docente)
                <option value="{{$docente->nombre}}">{{$docente->nombre}}</option>
                @endforeach
            </select>
        </div>
        <!-- label: gestion -->
        <label for="" class="h3">Gestion</label>
        <!-- lista desplegable de gestiones -->
        <select class="form-control h3 {{$errors->has('gestion')?'is-invalid':'' }}" name="gestion" value="{{ isset($convocatoria->gestion) ? $convocatoria->gestion:old('gestion')}}" required>
            <option>1</option>
            <option>2</option>
            <option>1 y 2</option>
            <option>2 de la presente gestion y 1 de la siguiente gestion</option>
            <option>Invierno</option>
            <option>Verano</option>
        </select>
        <!-- mesaje de error si el campo no esta llenado -->
        {!! $errors->first('gestion','<div class="invalid-feedback">:message</div>') !!}
        <br><br><br>
        <!-- boton guardar cambios -->
        <button class="btn btn-success" type="submit" onclick="calcular_total()">Aceptar</button>
        <!-- boton cancelar -->
        <a href="{{ url('convocatorias') }} " class="btn btn-secondary">Cancelar</a>
    </form>
</div>

@endsection
@section('script')
<!-- script para verificar subunidad y departamento -->
<script>
    $("#departamento").change(function(event) {
        jQuery.get("subunidades/" + event.target.value + "", function(response, departamento) {
            $("#subunidad").empty();
            for (i = 0; i < response.length; i++) {
                $("#subunidad").append("<option value='" + response[i].id + "'> " + response[i].name + "</option>");
            }
        });

    });
</script>

@endsection