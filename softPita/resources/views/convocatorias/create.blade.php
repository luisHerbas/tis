@extends('layouts.app2')
@section('content')

<!-- calculo del porcentaje total de notas -->
<script>
    function calcular_total() {
        nota_final = 0
        $(".nota_parcial").each(
            function(index, value) {
                nota_final = nota_final + eval($(this).val());
            }
        );
        $("#total").val(nota_final);
    }
</script>

<!-- formulario para crear convocatoria -->
<div class="container"><br>
    <h2 class=" text-center">CREAR CONVOCATORIA</h2>
    <form action="{{ url('/convocatorias')}}" method="post" enctype="multipart/form-data">
        <br><br>
        {{ csrf_field() }}
        <p> <strong>Nota: El campo gestion indica el periodo de tiempo que los auxiliares ganadores estaran vigentes en las respectivas asiganturas, siendo asi 1 para la primera parte de la gestion en que fue lanzada la convocatoria (Fecha de Publicacion), 2 para la segunda parte, y verano e invierno para los respectivos cursos </strong> </p>
        <div class="form-group">
            <!-- label: titulo de la convocatoria -->
            <label for="titulo_convocatoria" class="control-label h3">{{'Titulo de la Convocatoria: '}}</label>
            <!-- input: titulo de convocatoria -->
            <input type="text" class="form-control {{$errors->has('titulo_convocatoria')?'is-invalid':'' }}" name="titulo_convocatoria" id="titulo_convocatoria" value="{{ isset($convocatoria->titulo_convocatoria) ? $convocatoria->titulo_convocatoria:old('titulo_convocatoria')}}" required maxlength="150">
            {!! $errors->first('titulo_convocatoria','<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            <!-- label: fecha de publicacion -->
            <label for="fecha_publicacion" class="h3">{{'Fecha de publicacion: '}}</label> &nbsp;&nbsp;&nbsp;&nbsp;
            <!-- input(date): fecha de publicacion  -->
            <input type="date" class=" h4 {{$errors->has('fecha_publicacion')?'is-invalid':'' }}" name="fecha_publicacion" id="fecha_publicacion" value="{{ isset($convocatoria->fecha_publicacion) ? $convocatoria->fecha_publicacion:old('fecha_publicacion')}}" required>
            {!! $errors->first('fecha_publicacion','<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            <!-- label: departamento -->
            <label for="departamento" class="h3" required>{{'Departamento: '}}</label> &nbsp;&nbsp;&nbsp;&nbsp;
            <!-- lista desplegable de departamentos -->
            {!! Form::select('departamento', $departamentos, null, ['placeholder' => 'Selecciona un departamento', 'class' => 'form-control', 'id'=>'departamento']) !!}
        </div>
        <div class="form-group">
            <!-- label: subunidad -->
            <label for="subunidad" class="control-label h3">{{'Subunidad: '}}</label>
            <!-- lista desplegable de subunidad -->
            <select class="form-control {{$errors->has('subunidad')?'is-invalid':'' }}" name="subunidad" id="subunidad" required>
            </select>
            {!! $errors->first('subunidad','<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            <!-- label: porcentaje de conocimientos -->
            <label for="porcentaje_conocimiento" class="control-label h3">{{'Porcentaje Conocimientos: '}}</label>
            <!-- input: porcentaje de conocimientos -->
            <input type="number" class="form-control nota_parcial {{$errors->has('porcentaje_conocimiento')?'is-invalid':'' }}" name="porcentaje_conocimiento" id="porcentaje_conocimiento" value="{{ isset($convocatoria->porcentaje_conocimiento) ? $convocatoria->porcentaje_conocimiento:old('porcentaje_conocimiento')}}" required min="1" max="100">
            {!! $errors->first('porcentaje_conocimiento','<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            <!-- label: porcentaje meritos -->
            <label for="porcentaje_meritos" class="control-label h3">{{'Porcentaje Meritos: '}}</label>
            <!-- input: porcentaje meritos -->
            <input type="number" class="form-control nota_parcial {{$errors->has('porcentaje_meritos')?'is-invalid':'' }}" name="porcentaje_meritos" id="porcentaje_meritos" value="{{ isset($convocatoria->porcentaje_meritos) ? $convocatoria->porcentaje_meritos:old('porcentaje_meritos')}}" required min="1" max="100">
            {!! $errors->first('porcentaje_meritos','<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div>
            <!-- label: porcentaje total -->
            <label for="total" class="h3">Total Porcentaje: <input type="number" class="form-control" name="total" id="total" value="total" required min="100" max="100" /><br>
        </div>
        <div class="form-group">
            <!-- label: comision merito -->
            <label for="comision_merito" class="h3">{{'Comision merito: '}}</label> &nbsp;&nbsp;&nbsp;&nbsp;
            <!-- lista desplegable de docentes -->
            <select id="comision_merito" class="form-control" name="comision_merito">
                @foreach ($docentes as $docente)
                <option value="{{$docente->nombre}}">{{$docente->nombre}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <!-- label: comision conocimiento -->
            <label for="comision_conocimiento" class="h3">{{'Comision conocimiento: '}}</label> &nbsp;&nbsp;&nbsp;&nbsp;
            <!-- lista desplegable de docentes -->
            <select id="comision_conocimiento" class="form-control" name="comision_conocimiento">
                @foreach ($docentes as $docente)
                <option value="{{$docente->nombre}}">{{$docente->nombre}}</option>
                @endforeach
            </select>
        </div>
        <!-- label: gestion -->
        <label for="" class="h3">Gestion</label>
        <!-- lista desplegable de gestiones -->
        <select class="form-control h3 {{$errors->has('gestion')?'is-invalid':'' }}" name="gestion" value="{{ isset($convocatoria->gestion) ? $convocatoria->gestion:old('gestion')}}" required>
            <option>1</option>
            <option>2</option>
            <option>1 y 2</option>
            <option>2 de la presente gestion y 1 de la siguiente gestion</option>
            <option>Invierno</option>
            <option>Verano</option>
        </select>
        <!-- mesaje de error si el campo no esta llenado -->
        {!! $errors->first('gestion','<div class="invalid-feedback">:message</div>') !!}
        <br><br><br>
        <div>
            <form method=post onsubmit="return doSomething()">
                <!-- boton guardar cambios -->
                <input type=submit class="btn btn-success" value="Guardar" onclick="calcular_total()">
            </form>
            <!-- boton cancelar -->
            <a href="{{ url('convocatorias') }} " class="btn btn-secondary">Cancelar</a>
        </div>
    </form>
</div>

@endsection
@section('script')
<!-- script para verificar subunidad y departamento -->
<script>
    $("#departamento").change(function(event) {
        jQuery.get("subunidades/" + event.target.value + "", function(response, departamento) {
            $("#subunidad").empty();
            for (i = 0; i < response.length; i++) {
                $("#subunidad").append("<option value='" + response[i].id + "'> " + response[i].name + "</option>");
            }
        });
    });
</script>

@endsection