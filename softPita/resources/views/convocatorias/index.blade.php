@extends('layouts.app2')
@section('content')
<!-- pagina de inicio de convocatorias -->
<div class="container"><br>
    <h2 class="text-center">CONVOCATORIAS</h2><br><br>
    <a href="convocatorias/create" class="btn float-right " style="background:#172962; color:white">Nueva convocatoria</a><br><br>
    <p> <strong> Nota: Mientras una convocatoria este habilitada, esta se mostrara en la seccion de avisos en la parte de convocatorias vigentes, haciendo posible la postulacion a esta. Si desea dejar dejar de mostrar la convocatoria presione en Inhabilitar </strong> </p>
    <!-- tabla de convocaotrias -->
    <table class="table table-hover table-bordered">
        <thead class="thead-light">
            <tr>
                <th>#</th>
                <th scope="col" class="text-center">Departamento</th>
                <th scope="col" class="text-center">Subunidad</th>
                <th scope="col" class="text-center">Titulo de la convocatoria</th>
                <th scope="col" class="text-center">Fecha de publicacion</th>
                <th scope="col" class="text-center">Gestion</th>
                <th scope="col" class="text-center">Datos</th>
                <th scope="col" class="text-center">Estado</th>
                <th scope="col" class="text-center">Accion</th>
            </tr>
        </thead>
        <!-- se recuperan los datos de convocatoria -->
        <tbody class="text-center">
            @foreach($convocatorias as $convocatoria)
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$convocatoria->dep}}</td>
                <td>{{$convocatoria->name}}</td>
                <td>{{$convocatoria->titulo_convocatoria}}</td>
                <td>{{$convocatoria->fecha_publicacion}}</td>
                <td>{{$convocatoria->gestion}}</td>
                <td>
                    <!-- enlace agregar datos a la convocatoria -->
                    <p><a href="{{ route('detalle.index', [ 'convocatoria_id' =>$convocatoria->id ]) }}" class="text-primary">Agregar datos a la convocatoria</a></p>
                    <!-- habilitar o inhabilitar una convocatoria -->
                <td>
                    {{$convocatoria->estado}}
                    @if($convocatoria->estado == 'Habilitada')
                    <p><strong>Convocatoria publicada</strong></p>
                    @elseif($convocatoria->estado == 'Inhabilitada')
                    <p><strong>Convocatoria no publicada</strong></p>
                    @endif
                </td>
                <td>
                    <!-- botones ver, editar, borrar, habilitar/inhabilitar -->
                    <a class="btn btn-info" href="{{ route('visualizar.indexAdmin', [ 'convocatoria_id' =>$convocatoria->id ]) }}">Ver</a>
                    <a class="btn btn-warning text-white" href="{{ url('/convocatorias/'.$convocatoria->id. '/edit')}}">Editar</a>
                    <form method="post" action="{{ url('/convocatorias/'.$convocatoria->id)}}" style="display:inline">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button class="btn btn-danger" style="background:#C92023" type="submit" onclick="return confirm('¿Esta seguro de que desea eliminar el elemento?');">Borrar</button>
                    </form>
                    @if($convocatoria->estado == 'Habilitada')
                    <a class="btn btn-secondary text-white" href="{{ url('/convocatorias/'.$convocatoria->id. '/inhabilitar')}}">inhabilitar</a>
                    @elseif($convocatoria->estado == 'Inhabilitada')
                    <a class="btn btn-success text-white" href="{{ url('/convocatorias/'.$convocatoria->id. '/inhabilitar')}}">Habilitar</a>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection

<!-- script JavaScript -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>