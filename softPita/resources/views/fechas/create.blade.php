@extends('layouts.app2')
@section('content')

<!-- link de bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<!-- formulario crear fecha -->
<div class="container h5"><br>
    <h2 class="text-center">CREAR FECHA/EVENTO</h2><br><br>
    <form method="POST" action="{{ route('fecha.store', [ 'convocatoria_id' =>$convocatoria_id ]) }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
            <!-- label: evento -->
            <label for="">Evento:</label>
            <!-- input: evento -->
            <input type="text" class="form-control {{$errors->has('evento')?'is-invalid':'' }}" name="evento" value="{{ isset($fecha->evento) ? $fecha->evento:old('evento')}}">
            {!! $errors->first('evento','<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            <!-- label: fecha -->
            <label for="">Fecha:</label>
            <!-- input: fecha -->
            <input type="date" class="form {{$errors->has('fecha')?'is-invalid':'' }}" name="fecha" value="{{ isset($fecha->fecha) ? $fecha->fecha:old('fecha')}}">
            {!! $errors->first('fecha','<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div>
            <!-- botones guardar y cancelar -->
            <button class="btn btn-success" type="submit">Guardar</button>
            <a href="{{ route('detalle.index', [ 'convocatoria_id' =>$convocatoria_id ]) }} " class="btn btn-secondary">Cancelar</a>
        </div>
    </form>
</div>

@endsection