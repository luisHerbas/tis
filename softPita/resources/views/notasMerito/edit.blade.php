@extends('layouts.app2')
<!-- link bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
@section('content')

<!-- vista que manda a form -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><br>
                    <h2 class="text-center">INGRESAR NOTA MERITO</h2><br><br>
                </div>
                <!-- tabla de notas de postulantes form-->
                <div class="panel-body">
                    {!! Form::model($notasMerito, ['route' => ['notasMerito.update', $convocatoria_id, $notasMerito->id,],
                    'method' => 'PUT']) !!}
                    @include('notasMerito.partials.form')
                    {!! Form::close() !!}
                    <!-- boton cancelar -->
                    <a href="{{ URL::previous() }}" class="btn btn-secondary">Cancelar</a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection