@extends('layouts.app2')
@section('content')

<!-- link bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<!-- formulario modificar item -->
<div class="container h5"><br>
    <h2 class="text-center">MODIFICAR ITEM</h2><br><br>
    <form method="POST" action="{{ route('items.update', [ 'id_dep'=> $id_departamento, 'subunidad_id' =>$subunidad_id, 'item_id'=> $item->id ]) }}">
        {{ method_field('PUT') }}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
            <!-- label: nombre -->
            <label for="">Nombre</label>
            <!-- input nombre -->
            <input type="text" class="form-control {{$errors->has('nombre')?'is-invalid':'' }}" name="nombre" value="{{ isset($item->nombre) ? $item->nombre:old('nombre')}}">
            {!! $errors->first('nombre','<div class="invalid-feedback">:message</div>') !!}
        </div>

        <div class="form-group">
            <!-- lista desplegable: nivel -->
            <label for="">Nivel</label>
            <select class="form-control" name="nivel" value="{{ isset($item->nivel) ? $item->nivel:old('nivel')}}">
                <option>A</option>
                <option>B</option>
                <option>C</option>
                <option>D</option>
                <option>E</option>
                <option>F</option>
                <option>G</option>
                <option>H</option>
                <option>I</option>
                <option>J</option>
            </select>

            <div class="form-group">
                <!-- lista desplegable: electiva -->
                <label for="">Electiva</label>
                <select class="form-control" name="electiva" value="{{ isset($item->electiva) ? $item->electiva:old('electiva')}}">
                    <option>No</option>
                    <option>Si</option>
                </select>
                {!! $errors->first('electiva','<div class="invalid-feedback">:message</div>') !!}
            </div>

            <div>
                <!-- botones guardar y cancelar -->
                <button class="btn btn-success" type="submit">Guardar</button>
                <a href="{{ route('items.index', [ 'id_dep'=> $id_departamento, 'subunidad_id' =>$subunidad_id ]) }}" class="btn btn-secondary">Cancelar</a>

            </div>

    </form>
</div>
@endsection