@extends('layouts.app2')
@section('content')

<!-- link bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<!-- inicio items-subunidades -->
<div class="container"><br>
    <h2 class="text-center">ITEMS SUBUNIDADES</h2><br>
    <!-- boton regresar departamento -->
    <a href="{{route('subunidad.index', ['id_dep'=> $id_departamento])}}" class="btn btn-secondary float-right">Regresar a subunidades</a>
    <a href="" class="btn float-right hide"></a>
    <!-- boton crear item -->
    <a href="{{route('items.create', [ 'subunidad_id' => $subunidad_id, 'id_dep'=> $id_departamento])}}" class="btn float-right" style="background:#172962; color:white">Nuevo item</a><br><br>
    <!-- tabla de items -->
    <table class="table table-hover table-bordered">
        <thead class="thead-light">
            <tr>
                <th>#</th>
                <th scope="col" class="text-center">NOMBRE</th>
                <th scope="col" class="text-center">NIVEL</th>
                <th scope="col" class="text-center">ELECTIVA</th>
                <th scope="col" class="text-center">ACCION</th>
            </tr>
</div>
</thead>
<tbody class="text-center">
    @foreach($items as $item)
    <tr>
        <td>{{$loop->iteration}}</td>
        <td>{{$item->nombre}}</td>
        <td>{{$item->nivel}}</td>
        <td>{{$item->electiva}}</td>
        <td>
            <!-- botones editar y borrar -->
            <a class="btn btn-warning text-white" href="{{ route('items.edit', [ 'id_dep'=> $id_departamento, 'subunidad_id' => $subunidad_id, 'item_id'=> $item->id ])}}">Editar</a>
            <form method="post" action="{{ route('items.destroy', [ 'id_dep'=> $id_departamento, 'subunidad_id' => $subunidad_id, 'item_id'=> $item->id ])}}" style="display:inline">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
                <button class="btn btn-danger" style="background-color: #C92023" type="submit" onclick="return confirm('¿Esta seguro que desea eliminar el elemento?');">Borrar</button>
            </form>
        </td>
    </tr>
    @endforeach
</tbody>
</table>
</div>

@endsection