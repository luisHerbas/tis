<title>Registro de un Nueva Subunidad</title>

@extends('layouts.app2')
<!-- link bootstrap -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
@section('content')
<!-- formulario de registro de subunidad -->
<div class="container h5">
    <div><br>
        <h2 Class="text-center">REGISTRO DE SUBUNIDAD</h2><br><br>
        <div class="col-8">
            <form action="{{ route('subunidad.store', [ 'id_departamento' =>$id_departamento ]) }}" method="POST" class="pb-5">
                <div class="form-group">
                    <!-- label: nombre subunidad -->
                    <label>Nombre de la Subunidad:</label>
                    <!-- input: nombre subunidad -->
                    <input type="text" class="form-control {{$errors->has('name')?'is-invalid':'' }}" name="name" id="name" value="{{ isset($subunidad->name) ? $subunidad->name:old('name')}}">
                    {!! $errors->first('name','<div class="invalid-feedback">:message</div>') !!}

                </div>
                {!! csrf_field() !!}
                <!-- botones acepatar y cancelar -->
                <button type="submit" class="btn btn-success">Aceptar</button>
                <a href="{{ route('subunidad.index', [ 'id_departamento' =>$id_departamento ]) }} " class="btn btn-secondary">Cancelar</a>
            </form>
        </div>
    </div>
</div>

@endsection