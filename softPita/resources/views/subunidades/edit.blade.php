<title>Edicion de una Subunidad</title>

@extends('layouts.app2')
<!-- link bootstrap -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
@section('content')
<!-- formulario edicion subunidad -->
<div class="container h5">
    <div><br>
        <h2 class="text-center">EDITAR SUBUNIDAD</h2><br><br>
        <div class="col-8">
            <form action="{{route('subunidad.update', [ 'id_dep'=> $id_dep, 'id_sub' => $id_sub ]) }}" method="post" class="pb-5">
                {{ method_field('PUT') }}
                <div class="form-group">
                    <!-- label: nombre subunidad -->
                    <label>Nombre de la Subunidad:</label>
                    <!-- input: subunidad -->
                    <input type="text" class="form-control {{$errors->has('name')?'is-invalid':'' }}" name="name" id="name" value="{{ isset($subunidad->name) ? $subunidad->name:old('name')}}">
                    {!! $errors->first('name','<div class="invalid-feedback">:message</div>') !!}

                </div>
                {!! csrf_field() !!}
                <!-- botones aceptar y guardar -->
                <button type="submit" class="btn btn-success ">Aceptar</button>
                <a href="{{ route('subunidad.index', [ 'id_dep' =>$id_dep ]) }} " class="btn btn-secondary">Cancelar</a>
            </form>
        </div>
    </div>
</div>

@endsection