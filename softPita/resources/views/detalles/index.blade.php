<style>
    div {
        word-wrap: break-word;
    }
</style>

@extends('layouts.app2')
@section('content')
<!-- link bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<!-- formulario agregar datos a una convocatoria -->
<div class="container">
    <section id="prueba"><br>
        <h2 class="text-center ">{{ $titulos->titulo_convocatoria}}</h2> <br /> <br />
    </section>
    <p class="">Descripcion: El {{$departamentos->name}} convoca al concurso de méritos y examen de competencia para la provision de Auxiliares</p>
    <dl class="row">
        <dt class="col-sm-3 h4">1.- Requerimientos</dt>
        <dd class="col-sm-9 h4">
            <!-- tabla de requerimientos -->
            <table class="table table-hover table-bordered text-center">
                <thead class="thead-light">
                    <tr>
                        <th class="text-center">Items</th>
                        <th class="text-center">Cantidad auxiliares</th>
                        <th class="text-center">Horas Academicas</th>
                        <th class="text-center">Destino</th>
                        <th class="text-center">Accion</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($requerimientos as $requerimiento)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$requerimiento->cantidad_auxiliares}} aux</td>
                        <td>{{$requerimiento->horas_academicas}} Hrs/mes</td>
                        <td>{{$requerimiento->destino}}</td>
                        <!-- botones de editar y borrar de requerimientos -->
                        <td><a class="btn btn-warning text-white" href="{{ route('reque.edit', [ 'convocatoria_id' => $convocatoria_id, 'requerimiento_id'=> $requerimiento->id ])}}">Editar</a>
                            <form method="post" action="{{ route('reque.delete', [ 'convocatoria_id' => $convocatoria_id, 'requerimiento_id'=> $requerimiento->id ])}}" style="display:inline">
                                {{ method_field('DELETE') }}
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button class="btn btn-danger" style="background-color: #C92023" type="submit" onclick="return confirm('¿Esta seguro de que desea eliminar el elemento?');">Borrar</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <!-- boton crear requerimientos -->
            <a href="{{ route('reque.create', [ 'convocatoria_id' => $convocatoria_id ]) }}" class="btn" style="background:#172962; color:white">Crear Requerimiento</a>
        </dd>
    </dl>

    <dl class="row">
        <dt class="col-sm-3 h4">2.- Requisitos: </dt>
        <dd class="col-sm-9 h4">
            <!-- tabla de requisitos -->
            <table class="table table-hover table-bordered text-center">
                <thead class="thead-light">
                    <tr>
                        <th>#</th>
                        <th>Requisito</th>
                        <th>Accion</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($requisitos as $requisito)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$requisito->requisito}}</td>
                        <!-- botones editar y boorar de requisitos -->
                        <td><a class="btn btn-warning text-white" href="{{ route('requisito.edit', [ 'convocatoria_id' => $convocatoria_id, 'requisito_id'=> $requisito->id ])}}">Editar</a>
                            <form method="post" action="{{ route('requisito.delete', [ 'convocatoria_id' => $convocatoria_id, 'requisito_id'=> $requisito->id ])}}" style="display:inline">
                                {{ method_field('DELETE') }}
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button class="btn btn-danger" style="background-color: #C92023" type="submit" onclick="return confirm('¿Esta seguro de que desea eliminar el elemento?');">Borrar</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <!-- boton crear requisito -->
            <a href="{{ route('requisito.create', [ 'convocatoria_id' => $convocatoria_id ]) }}" class="btn" style="background:#172962; color:white">Crear Requisito</a>
        </dd>
    </dl>
    <p class="h5"><strong>3.- Documentos a presentar:</strong></p>
    <!-- tabla de documentos a presentar -->
    <table class="table table-hover table-bordered text-center">
        <thead class="thead-light">
            <tr>
                <th>Documento</th>
                <th>Accion</th>
            </tr>
        </thead>
        <tbody class="text-center">
            @foreach($documentos as $documento)
            <tr>
                <td>{{$documento->documento}}</td>
                <!-- botones editar y borrar documentos a presentar -->
                <td><a class="btn btn-warning text-white" href="{{ route('documento.edit', [ 'convocatoria_id' => $convocatoria_id, 'documento_id'=> $documento->id ])}}">Editar</a>
                    <form method="post" action="{{ route('documento.delete', [ 'convocatoria_id' => $convocatoria_id, 'documento_id'=> $documento->id ])}}" style="display:inline">
                        {{ method_field('DELETE') }}
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button class="btn btn-danger" style="background-color: #C92023" type="submit" onclick="return confirm('¿Esta seguro de que desea eliminar el elemento?');">Borrar</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <!-- boton crear documento -->
    <a href="{{ route('documento.create', [ 'convocatoria_id' => $convocatoria_id ]) }}" class="btn" style="background:#172962; color:white">Crear documento</a><br /><br />

    <p class=""><strong>NOTA: Toda la documentación se legalizará gratuitamente en secretaría del {{$departamentos->name}} (presentar original y fotocopias). La documentación no será devuelta. </strong></p>
    <p class="h5"><strong>4.- De la forma</strong></p>
    <p class="">Presentación de la documentación en sobre manila cerrado y con el rótulo generado al momento del registro</p>
    <p class="h5"><strong>5.- Fecha de presentacion</strong></p>
    <p class="">Presentación de la documentación en sobre manila cerrado y con el rótulo generado al momento del registro en Secretaría del {{$departamentos->name}}, hasta la hora y fecha especificada en el punto <strong>Fechas de prueba</strong> </p>
    <p class="h5"><strong>6.- De la calificacion de meritos</strong></p>
    <p class="">La calificacion de meritos se basara en los documentos presentados por el postulante y se realizara sobre la base de 100 puntos que representa el <strong>{{$titulos->porcentaje_meritos}}%</strong> del puntaje final y se respondera de la siguiente manera:</strong> </p>


    @php
    $sumaTotal=0;
    @endphp

    <!-- tabla de meritos -->
    <table class="table table-bordered table-hover">
        <!-- primer grupo de meritos -->
        <thead class="thead-light">
            <tr>
                <!-- boton crear merito para 1er grupo de meritos -->
                <th>Rendimiento Academico <a href="{{ route('merito.create', [ 'convocatoria_id' => $convocatoria_id ]) }}" class="btn float-right" style="background:#172962; color:white">Crear Merito</a></th>
                @php
                $suma2=0;
                @endphp
                <th>
                    @foreach($meritos as $merito)
                    @php
                    $suma2+=$merito->porcentaje;
                    @endphp
                    @endforeach
                    {{$suma2}}
                    %</th>
                <th>Accion</th>
            </tr>
        </thead>
        <tbody>
            @foreach($meritos as $merito)
            <tr>
                <td>{{$merito->nombre}}</td>
                <td>{{$merito->porcentaje}} %</td>
                <!-- botones editar y eliminar 1er grupo de meritos -->
                <td><a class="btn btn-warning text-white" href="{{ route('merito.edit', [ 'convocatoria_id' => $convocatoria_id, 'merito_id'=> $merito->id ])}}">Editar</a>
                    <form method="post" action="{{ route('merito.delete', [ 'convocatoria_id' => $convocatoria_id, 'merito_id'=> $merito->id ])}}" style="display:inline">
                        {{ method_field('DELETE') }}
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button class="btn btn-danger" style="background-color: #C92023" type="submit" onclick="return confirm('¿Esta seguro de que desea eliminar el elemento?');">Borrar</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
        <!-- segundo grupo de meritos -->
        <thead class="thead-light">
            <tr>
                <!-- boton crear merito 2do grupo de meritos -->
                <th>Documentos de experiencia universitaria<a href="{{ route('merito1.create', [ 'convocatoria_id' => $convocatoria_id ]) }}" class="btn float-right" style="background:#172962; color:white">Crear Merito</a></th>
                @php
                $suma1=0;
                @endphp
                <th>
                    @foreach($meritos1 as $merito1)
                    @php
                    $suma1+=$merito1->porcentaje;
                    @endphp
                    @endforeach
                    {{$suma1}}
                    %</th>
                <th>Accion</th>
            </tr>
        </thead>
        <tbody>
            @foreach($meritos1 as $merito1)
            <tr>
                <td>{{$merito1->nombre}}</td>
                <td>{{$merito1->porcentaje}} %</td>
                <!-- botones editar y eliminar 2do grupo de meritos -->
                <td><a class="btn btn-warning text-white" href="{{ route('merito1.edit', [ 'convocatoria_id' => $convocatoria_id, 'merito1_id'=> $merito1->id ])}}">Editar</a>
                    <form method="post" action="{{ route('merito1.delete', [ 'convocatoria_id' => $convocatoria_id, 'merito1_id'=> $merito1->id ])}}" style="display:inline">
                        {{ method_field('DELETE') }}
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button class="btn btn-danger" style="background-color: #C92023" type="submit" onclick="return confirm('¿Esta seguro de que desea eliminar el elemento?');">Borrar</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
        <!-- tercer grupo de meritos -->
        <thead class="thead-light">
            <tr>
                <!-- boton crear 3er grupo de meritos -->
                <th>Documentos de experiencia extrauniversitaria<a href="{{ route('merto.create', [ 'convocatoria_id' => $convocatoria_id ]) }}" class="btn float-right" style="background:#172962; color:white">Crear Merito</a></th>
                @php
                $suma=0;
                @endphp
                <th>
                    @foreach($mertos as $merto)
                    @php
                    $suma+=$merto->porcentaje;
                    @endphp
                    @endforeach
                    {{$suma}}
                    %</th>
                <th>Accion</th>
            </tr>
        </thead>
        @php
        $sumaTotal=$suma+$suma1+$suma2;
        @endphp
        <tbody>
            @foreach($mertos as $merto)
            <tr>
                <td>{{$merto->nombre}}</td>
                <td>{{$merto->porcentaje}} %</td>
                <!-- botones editar y eliminar 3er grupo de meritos -->
                <td><a class="btn btn-warning text-white" href="{{ route('merto.edit', [ 'convocatoria_id' => $convocatoria_id, 'merto_id'=> $merto->id ])}}">Editar</a>
                    <form method="post" action="{{ route('merto.delete', [ 'convocatoria_id' => $convocatoria_id, 'merto_id'=> $merto->id ])}}" style="display:inline">
                        {{ method_field('DELETE') }}
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button class="btn btn-danger" style="background-color: #C92023" type="submit" onclick="return confirm('¿Esta seguro de que desea eliminar el elemento?');">Borrar</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
        <!-- sumatoria del procentaje total -->
        <thead class="thead-light">
            <tr>
                <th>Porcentaje Total</th>
                @if($sumaTotal == 100)
                <th>{{$sumaTotal}} %</th>
                @else
                <th style="color:crimson;">{{$sumaTotal}}%</th>
                @endif

            </tr>
        </thead>
    </table>

    <p class="h5"><strong>7.- Calificacion de conocimientos</strong></p>
    <p class="">La calificación de conocimientos se realiza sobre la base de 100 puntos, equivalentes al <strong>{{$titulos->porcentaje_conocimiento}}%</strong> de la calificación final. Se realizarán las siguiente pruebas:</p>
    <p class="">a) Examen escrito de conocimientos (prueba de pre selección) ..............................................................40%</p>
    <p class="">b) Examen oral, donde se evaluarán aspectos didácticos y pedagógicos sobre conocimiento y dominio de la materia.</p>
    <p class="">Tendrá una duración máxima de 25 min: 15 min de exposición y 10 min de preguntas...........60%</p>
    <p class="h5"><strong>8.- De los tribunales</strong></p>
    <p class="">Lo honorables Consejos del {{$departamentos->name}} designarán respectivamente; para la calificación de méritos: 1 docente y 1 delegado estudiante y para la comisión de conocimientos: 1 docente por asignatura convocada más 1 estudiante veedor.</p>
    <p class="h5"><strong>9.- Fechas/eventos importantes</strong></p>
    <p class="">Las pruebas escritas serán sobre el contenido de la materia a la que se postula con nota de aprobación mayor o igual a 51. Las pruebas orales, se tomarán sólo a los postulantes que hayan vencido la prueba escrita y de acuerdo a pertinencia y contenido de la materia a la que se postula. Fechas importantes a considerar:</p>

    <!-- tabla de fechas importantes -->
    <table class="table table-hover table-bordered text-center">
        <thead class="thead-light">
            <tr>
                <th>Eventos</th>
                <th>Fechas</th>
                <th>Accion</th>
            </tr>
        </thead>
        <tbody>
            @foreach($fechas as $fecha)
            <tr>
                <td>{{$fecha->evento}}</td>
                <td>{{$fecha->fecha}}</td>
                <!-- botones editar y eliminar fechas importantes -->
                <td><a class="btn btn-warning text-white" href="{{ route('fecha.edit', [ 'convocatoria_id' => $convocatoria_id, 'fecha_id'=> $fecha->id ])}}">Editar</a>
                    <form method="post" action="{{ route('fecha.delete', [ 'convocatoria_id' => $convocatoria_id, 'fecha_id'=> $fecha->id ])}}" style="display:inline">
                        {{ method_field('DELETE') }}
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button class="btn btn-danger" style="background-color: #C92023" type="submit" onclick="return confirm('¿Esta seguro de que desea eliminar este elemento?');">Borrar</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <!-- boton crear fecha importante -->
    <a href="{{ route('fecha.create', [ 'convocatoria_id' => $convocatoria_id ]) }}" class="btn" style="background:#172962; color:white">Crear Fecha/evento</a><br /><br />

    <p class="h5"><strong>10.- Del nombramiento</strong></p>
    <p class="">Los nombramientos de auxiliar universitario titular recaerán sobre aquellos postulantes que hubieran aprobado y obtenido mayor calificación. Caso contrario se precederá con el nombramiento de aquel que tenga la calificación mayor como auxiliar invitado. Cabe resaltar que un auxiliar invitado sólo tendrá nombramiento por los periodos académicos de la gestion <strong>{{$titulos->gestion}} </strong></p><br />
    <!-- botones guardar y cancelar -->
    <div class="text-center">
        @if($sumaTotal == 100)
        <a href="{{ url('convocatorias') }} " class="btn btn-success">Guardar datos</a>
        <a href="{{ route('convocatorias.index') }}" class="btn btn-secondary">Cancelar</a>
        @else
        <a href="{{ url('convocatorias') }} " class="btn btn-success disabled">Guardar datos</a>
        <p style="color:crimson;">Corregir el porcentaje de méritos</p>
        @endif
        <br /><br />
    </div>
</div>

@endsection