@extends('layouts.app2')
<!-- link bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
@section('content')

<!-- vista notas conocimiento AVISOS -->
<div class="text-center">
    <label for="" class="h2 text-center">Resultados Conocimientos</label> <br> <br> <br>
</div>
<!-- notas conocimientos -->
<table class="table table-hover table-bordered">
    <thead class="thead-light">
        <tr>
            <th>Apellidos</th>
            <th>Nombres</th>
            <th>Convocatoria</th>
            <th>Item</th>
            <th>Nota</th>
        </tr>
    </thead>
    <tbody>
        @foreach($postulantes as $postulante)
        <tr>
            <td>{{$postulante->last_name}}</td>
            <td>{{$postulante->name}}</td>
            <td>{{$postulante->titulo_convocatoria}}</td>
            <td>{{$postulante->destino}}</td>
            <td>{{$postulante->nota_conocimientos}}</td>
        </tr>
        @endforeach
</table>

@endsection