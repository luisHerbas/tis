@extends('layouts.app2')
@section('content')

<!-- link bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"><br>
<h2 class="text-center">DOCUMENTOS</h2><br><br>
<!-- formulario para habilitar o inhabilitar a un postulante -->
<div class="container" style="max-width: 65rem;">
    <!-- se habilita si todos los checkboxs estan tickeados -->
    <form action="{{ route('habilitar.store', ['convocatoria_id' => $convocatoria_id ])}}" method="post">
        @foreach($documents as $document)
        <div class="card border-dark mb-3">
            <div class="card-body text-dark" style="max-width: 18rem;">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" required>
                    <label class="form-check-label" for="defaultCheck1">
                        {{$document->documento}}
                    </label>
                </div>
            </div>
        </div>
        @endforeach
        {!! csrf_field() !!}
        <!-- botones habilitar, inhabilitar y cancelar -->
        <button type="submit" class="btn btn-success">Habilitar</button>
        <a class="btn btn-secondary btn-danger" style="background:#C92023" href="{{ route('inhabilitar.index', ['sis'=> $sis, 'convocatoria_id' =>$convocatoria_id ])}}">Inhabilitar</a>
        <a href="{{ route('listaspostulantes.index', ['convocatoria_id' =>$convocatoria_id ])}}" class="btn btn-secondary">Cancelar</a>
        <input type="hidden" name="sis" value="{{$sis}}">
    </form>
</div>
@endsection
