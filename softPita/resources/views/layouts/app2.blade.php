<!DOCTYPE html>
<html lang="es">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

  <title>Administrador de Convocatorias</title>
  <!-- estilos usados -->
  <style>
    #header {
      background-color: #172962 !important;
    }

    #footer {
      background-color: #172962 !important;
    }

    #footerCopyright {
      background-color: #020B28 !important;
    }

    .site-footer {
      background-color: #172962;
      padding: 45px 0 20px;
      font-size: 15px;
      line-height: 24px;
      color: #737373;
    }

    .site-footer h6 {
      color: #fff;
      font-size: 16px;
      text-transform: uppercase;
      margin-top: 5px;
      letter-spacing: 2px
    }

    .site-footer a {
      color: #737373;
    }

    .footer-links {
      padding-left: 0;
      list-style: none
    }

    .footer-links a {
      color: #737373
    }

    #header img {
      width: 40px;
    }

    #main .carousel-inner img {
      max-height: 70vh;
      object-fit: cover;
    }

    #carousel {
      position: relative;
    }

    #carousel .overlay {
      position: absolute;
      top: 0;
      bottom: 0;
      left: 0;
      right: 0;
      background-color: rgba(0, 0, 0, 0.5);
      color: white;
      z-index: 1;
    }

    #carousel .overlay .container,
    #carousel .overlay .row {
      height: 100%;
    }

    #footer a {
      color: white;
    }

    #footerCopyright p {
      color: white;
    }

    #headerCard {
      color: #EF0B0B;

    }

    h5 {
      color: #284DC4;
    }

    #opcionesTitle {
      color: #172962;
    }
  </style>
</head>

<body data-spy="scroll" data-target="#navbar" data-offset="66">
  <!-- navbar responsivo -->
  <nav id="header" class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container">
      <a class="navbar-brand" href="/">Administrador de Convocatorias</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <!-- navbar iniciar y cerrar sesion -->
      <div class="collapse navbar-collapse" id="navbar">
        <ul class="navbar-nav ml-auto">
          @if (Auth::guest())
          <li class="nav-item">
            <a class="nav-link text-white" href="{{ route('login') }}">Iniciar sesión</a>
          </li>
          @else
          <li class="dropdown nav-item">
            <a href="#" class="dropdown-toggle nav-link text-white" data-toggle="dropdown" role="button" aria-expanded="false">
              {{ Auth::user()->name }} <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
              <li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                  Salir
                </a>
                <!-- lista desplegable de opciones de sesion -->
                <div class="dropdown-divider"></div>
                <a href="/administrador">Administracion
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
                </form>
              </li>
            </ul>
          </li>
          @endif
        </ul>
      </div>
    </div>
  </nav><br>

  <main id="main">

    <div class="container-fluid d-flex">
      <div>
        <!-- menu lateral izquierdo ADMINISTRACION -->
        <div id="list-example" class="container list-group" style="max-width: 180px;">
          <a class="list-group-item text-white" style="background-color: #172962;">
            <font size="5" face="times new roman"> <small>Administracion</small> </font>
          </a>
          <a class="list-group-item list-group-item-action " href="{{ route('users.index')}}" style="background-color: #EEEEEE;">Usuarios</a>
          <a class="list-group-item list-group-item-action " href="{{ route('roles.index')}}" style="background-color: #EEEEEE;">Roles</a>
          <a class="list-group-item list-group-item-action " href="{{ route('convocatorias.index')}}" style="background-color: #EEEEEE;">Convocatorias</a>
          <a class="list-group-item list-group-item-action " href="{{ route('postulantes.filtraje')}}" style="background-color: #EEEEEE;">Postulantes</a>
          <a class="list-group-item list-group-item-action " href="{{ route('departamentos.show')}}" style="background-color: #EEEEEE;">Departamentos</a>
          <a class="list-group-item list-group-item-action " href="{{ route('notasConocimiento.preindex')}}" style="background-color: #EEEEEE;">Resultados Conocimiento</a>
          <a class="list-group-item list-group-item-action " href="{{ route('notasMerito.index')}}" style="background-color: #EEEEEE;">Resultados Méritos</a>
          <a class="list-group-item list-group-item-action " href="{{ route('finales.index')}}" style="background-color: #EEEEEE;">Resultados Finales</a>
        </div>
        <br>
        <!-- menu lateral izquierdo de avisos -->
        <div class="container text-muted text-white" style="max-width: 180px; background-color: #EEEEEE;">
          <div style="background-color: #172962;">
            <h6 class="dropdown-header d-flex justify-content-center">
              <font size="5" face="times new roman" color=#ffffff>Avisos</font></a>
            </h6>
          </div>
          <a href="{{ route('convocatorias.publicar')}}" style="background-color: #EEEEEE;">Convocatorias Vigentes</a> <br>
          <a href="{{ route('convocatorias.conocimiento')}}" style="background-color: #EEEEEE;">Notas Conocimiento</a> <br>
          <a href="{{ route('convocatorias.merito')}}" style="background-color: #EEEEEE;">Notas Merito</a>
          <a href="{{ route('finales.publicar')}}" style="background-color: #EEEEEE;">Resultados finales</a>
        </div>
      </div><br> <br> <br>
      <br>
      <!-- card -->
      <div class="container" style="max-width:1170px;">
        <div class="card-deck d-flex justify-content-center">
          <div class="card">
            <div class="card-block">
              <div class="container">

                @if(session('info'))
                <div class="container">
                  <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                      <div class="alert alert-success">
                        {{ session('info') }}
                      </div>
                    </div>
                  </div>
                </div>
                @endif

                @yield('content')
                <br>
                <br>
                <br>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div><br><br> <br> <br><br> <br> <br><br> <br> <br>
  </main>

  <!-- Footer -->
  <footer class="site-footer">
    <div class="container">
      <div class="row">
        <div class="col d-flex justify-content-center text-white">
          <p class="copyright-text">Derechos Reservados © 2020 · Universidad Mayor de San Simón · SoftpitaDmani
          </p>
        </div>
      </div>
    </div>
  </footer>

</html>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
@yield('script')

</body>

</html>