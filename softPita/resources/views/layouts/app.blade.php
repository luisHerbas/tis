<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
  <meta name="generator" content="Jekyll v3.8.6">
  <title>Administrador de convocatorias</title>

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <link rel="canonical" href="https://getbootstrap.com/docs/4.4/examples/jumbotron/">

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.css" rel="stylesheet">


  <style>
    .bd-placeholder-img {
      font-size: 1.125rem;
      text-anchor: middle;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
    }

    .slider {
      background: url("images/logotipo.jpeg");
      height: 18vh;
      background-size: cover;
      background-position: center;
    }

    .hoverable {
      display: inline-block;
      backface-visibility: hidden;
      vertical-align: middle;
      position: relative;
      box-shadow: 0 0 1px rgba(0, 0, 0, 0);
      tranform: translateZ(0);
      transition-duration: .3s;
      transition-property: transform;
    }

    .hoverable:before {
      position: absolute;
      pointer-events: none;
      z-index: -1;
      content: '';
      top: 100%;
      left: 5%;
      height: 10px;
      width: 90%;
      opacity: 0;
      background: -webkit-radial-gradient(center, ellipse, rgba(255, 255, 255, 0.35) 0%, rgba(255, 255, 255, 0) 80%);
      background: radial-gradient(ellipse at center, rgba(255, 255, 255, 0.35) 0%, rgba(255, 255, 255, 0) 80%);
      /* W3C */
      transition-duration: 0.3s;
      transition-property: transform, opacity;
    }

    .hoverable:hover,
    .hoverable:active,
    .hoverable:focus {
      transform: translateY(-5px);
    }

    .hoverable:hover:before,
    .hoverable:active:before,
    .hoverable:focus:before {
      opacity: 1;
      transform: translateY(-5px);
    }

    #header {
      background-color: #375e7a !important;
    }
  </style>


  </style>

</head>

<body>


  <header class="blog-header">
    <nav id="header" class="navbar navbar-expand-lg navbar-dark bg-dark ">
      <div class="container">
        <a class="navbar-brand" href="/">
          Administrador de Convocatorias
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbar ">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="container d-flex justify-content-center"></div>
          <div class="container d-flex justify-content-center">
            <!-- <a class="nav-link text-white hoverable" href="#"><svg class="bi bi-arrow-up" width="2em" height="2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                            <path fill-rule="evenodd" d="M8 3.5a.5.5 0 01.5.5v9a.5.5 0 01-1 0V4a.5.5 0 01.5-.5z" clip-rule="evenodd"/>
                                                                            <path fill-rule="evenodd" d="M7.646 2.646a.5.5 0 01.708 0l3 3a.5.5 0 01-.708.708L8 3.707 5.354 6.354a.5.5 0 11-.708-.708l3-3z" clip-rule="evenodd"/>
                                                                        </svg></a> -->
            <a class="nav-link text-white hoverable" href="{{ route('administrador.home')}}"><svg class="bi bi-house-door-fill" width="2em" height="2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path d="M6.5 10.995V14.5a.5.5 0 01-.5.5H2a.5.5 0 01-.5-.5v-7a.5.5 0 01.146-.354l6-6a.5.5 0 01.708 0l6 6a.5.5 0 01.146.354v7a.5.5 0 01-.5.5h-4a.5.5 0 01-.5-.5V11c0-.25-.25-.5-.5-.5H7c-.25 0-.5.25-.5.495z" />
                <path fill-rule="evenodd" d="M13 2.5V6l-2-2V2.5a.5.5 0 01.5-.5h1a.5.5 0 01.5.5z" clip-rule="evenodd" />
              </svg></a>
            <!-- <a class="nav-link text-white hoverable" href="#"><svg class="bi bi-arrow-down" width="2em" height="2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                                  <path fill-rule="evenodd" d="M4.646 9.646a.5.5 0 01.708 0L8 12.293l2.646-2.647a.5.5 0 01.708.708l-3 3a.5.5 0 01-.708 0l-3-3a.5.5 0 010-.708z" clip-rule="evenodd"/>
                                                                                  <path fill-rule="evenodd" d="M8 2.5a.5.5 0 01.5.5v9a.5.5 0 01-1 0V3a.5.5 0 01.5-.5z" clip-rule="evenodd"/>
                                                                      </svg></a> -->
          </div>
          <div class="container d-flex justify-content-end collapse navbar-collapse">

          </div>

        </div>

        <div class="collapse navbar-collapse" id="navbar">
          <ul class="navbar-nav ml-auto">
            @if (Auth::guest())
            <li class="nav-item">
              <a class="nav-link text-white" href="{{ route('login') }}">Iniciar sesión</a>
            </li>
            <!-- <li><a href="{{ route('register') }}">Register</a></li> -->
            @else
            <li class="dropdown nav-item">
              <a href="#" class="dropdown-toggle nav-link text-white" data-toggle="dropdown" role="button" aria-expanded="false">
                {{ Auth::user()->name }} <span class="caret"></span>
              </a>

              <ul class="dropdown-menu" role="menu">
                <li>
                  <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                    Salir
                  </a>

                  <div class="dropdown-divider"></div>

                  <a href="/administrador">
                    Administracion
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                  </form>
                </li>
              </ul>
            </li>
            @endif
          </ul>
        </div>

      </div>
    </nav>

    <div id="app">
      <!-- <section class="container-fluid slider d-flex justify-content-center align-items-center">
        </section>     -->
      <!-- <nav id="navheader" class="navbar navbar-default navbar-expand-sm py-0 mb-2 sticky-top" style="background-color: #527996;">
          <div class="collapse navbar-collapse d-flex" id="navbar ">
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
              </button>
              <div class="container d-flex justify-content-center"></div>
                  <div class="container d-flex justify-content-center">
                      <a class="nav-link text-white hoverable" href="#"><svg class="bi bi-arrow-up" width="2em" height="2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                            <path fill-rule="evenodd" d="M8 3.5a.5.5 0 01.5.5v9a.5.5 0 01-1 0V4a.5.5 0 01.5-.5z" clip-rule="evenodd"/>
                                                                            <path fill-rule="evenodd" d="M7.646 2.646a.5.5 0 01.708 0l3 3a.5.5 0 01-.708.708L8 3.707 5.354 6.354a.5.5 0 11-.708-.708l3-3z" clip-rule="evenodd"/>
                                                                        </svg></a>
                      <a class="nav-link text-white hoverable" href="{{ route('administrador.home')}}"><svg class="bi bi-house-door-fill" width="2em" height="2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                                  <path d="M6.5 10.995V14.5a.5.5 0 01-.5.5H2a.5.5 0 01-.5-.5v-7a.5.5 0 01.146-.354l6-6a.5.5 0 01.708 0l6 6a.5.5 0 01.146.354v7a.5.5 0 01-.5.5h-4a.5.5 0 01-.5-.5V11c0-.25-.25-.5-.5-.5H7c-.25 0-.5.25-.5.495z"/>
                                                                                  <path fill-rule="evenodd" d="M13 2.5V6l-2-2V2.5a.5.5 0 01.5-.5h1a.5.5 0 01.5.5z" clip-rule="evenodd"/>
                                                                        </svg></a>
                      <a class="nav-link text-white hoverable" href="#"><svg class="bi bi-arrow-down" width="2em" height="2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                                  <path fill-rule="evenodd" d="M4.646 9.646a.5.5 0 01.708 0L8 12.293l2.646-2.647a.5.5 0 01.708.708l-3 3a.5.5 0 01-.708 0l-3-3a.5.5 0 010-.708z" clip-rule="evenodd"/>
                                                                                  <path fill-rule="evenodd" d="M8 2.5a.5.5 0 01.5.5v9a.5.5 0 01-1 0V3a.5.5 0 01.5-.5z" clip-rule="evenodd"/>
                                                                      </svg></a>
                  </div>
                  <div class="container d-flex justify-content-end collapse navbar-collapse">
                    
                  </div>
              
          </div>
          
        </nav> -->
      <div class="nav-scroller py-0 mb-2 sticky-top" style="background-color: #EEEEEE">
  </header>
  <main role="main"><br>

    <div class="container-fluid d-flex">
      <div>
        <div id="list-example" class="container list-group" style="max-width: 180px;">
          <a class="list-group-item text-white" style="background-color: #527996;">
            <font size="5" face="times new roman"> <small>Administracion</small> </font>
          </a>


          <a class="list-group-item list-group-item-action " href="{{ route('users.index')}}" style="background-color: #EEEEEE;">Usuarios</a>
          <a class="list-group-item list-group-item-action " href="{{ route('roles.index')}}" style="background-color: #EEEEEE;">Roles</a>
          <a class="list-group-item list-group-item-action " href="{{ route('convocatorias.index')}}" style="background-color: #EEEEEE;">Convocatorias</a>
          <a class="list-group-item list-group-item-action " href="{{ route('listaspostulantes.index')}}" style="background-color: #EEEEEE;">Postulantes</a>
          <a class="list-group-item list-group-item-action " href="{{ route('notasConocimiento.index')}}" style="background-color: #EEEEEE;">Resultados Conocimiento</a>
          <a class="list-group-item list-group-item-action " href="{{ route('notasMerito.index')}}" style="background-color: #EEEEEE;">Resultados Méritos</a>
          <a class="list-group-item list-group-item-action " href="" style="background-color: #EEEEEE;">Resultados Finales</a>
          <a class="list-group-item list-group-item-action " href="{{ route('departamentos.show')}}" style="background-color: #EEEEEE;">Departamentos</a>
        </div>
        <br>
        <div class="container text-muted text-white" style="max-width: 180px; background-color: #EEEEEE;">
          <div style="background-color: #527996;">
            <h6 class="dropdown-header d-flex justify-content-center">
              <font size="5" face="times new roman" color=#ffffff>Avisos</font></a>
            </h6>
          </div>
          <p class="d-flex justify-content-center align-items-center">Titulo.</p>
          <p class="mb-0">Contenido del aviso.</p>
        </div>
      </div>
      <br>
      <div class="container" style="max-width:1170px;">
        <div class="card-deck d-flex justify-content-center">
          <div class="card">
            <div class="card-block">
              <div class="container">

                @if(session('info'))
                <div class="container">
                  <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                      <div class="alert alert-success">
                        {{ session('info') }}
                      </div>
                    </div>
                  </div>
                </div>
                @endif

                @yield('content')
                <br>
                <br>
                <br>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>

    <hr>

  </main>

  <footer class="container-fluid " style="background-color: #375e7a;">
    <section class="container-fluid d-flex justify-content-center align-items-center text-white">
      <p>&copy; Copyright 2020 · Universidad Mayor de San Simon · SoftpitaDmani</p>
  </footer>
  </section>
  </footer>
  </div>
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->

  <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  @yield('script')
</body>

<!-- Modal 1 
                     Button trigger modal 
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">
                        Más Información
                      </button>
                         Modal 
                        <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLongTitle">Convocatoria</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                <p>En esta seccion se muestra la convocatoria</p>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                              </div>
                            </div>
                          </div>
                        </div>
                           Fin Modal 1 -->

</html>