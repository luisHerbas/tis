@extends('layouts.app')
<!-- link bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
@section('content')

<!-- mensaje de pertenencia a la institucion como estudiante -->
<div class="container" style="max-width:1170px;"><br><br>
  <div class="card-deck d-flex justify-content-center">
    <div class="card">
      <div class="card-block">
        <div class="container">
          <br><br>
          @if($name)
          <h1 class="display-5 font-weight-light text-center"> {{$name}} no es miembro de esta Universidad</h1>
          <p>{{$name}} no se encuentra registrado en el sistema Websis de la Universidad Mayor de San Simon, por lo tanto no puede registrarse en este sistema</p>
          <div class="col text-center">
            <a href="{{ URL::previous() }}" class="btn btn-primary btn-lg">Entendido</a>
            @else
            <h1 class="display-5 font-weight-light text-center"> Usted no es miembro de esta Universidad</h1>
            <p>Usted no se encuentra registrado en el sistema Websis de la Universidad Mayor de San Simon, por lo tanto no puede registrarse en este sistema</p>
            <div class="col text-center">
              <a href="{{ URL::previous() }}" class="btn btn-primary btn-lg">Entendido</a>
              @endif
            </div>
            <br>
            <br>
            <br>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>

@endsection