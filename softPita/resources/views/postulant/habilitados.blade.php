@extends('layouts.app2')
@section('content')

<br>
<!-- vista de habilitados -->
<h2 class="text-center">HABILITADOS</h2><br>
<nav class="navbar navbar-default navbar-static-top container">
    <div class="container">
        <div class="navbar-header">
        </div>
</nav>
<!-- navbar de estados de postulantes -->
<div class="navbar" id="app-navbar">
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link active" href="{{ route('listaspostulantes.index', [ 'convocatoria_id' =>$convocatoria_id ]) }} ">Postulantes</a>
        </li>
        <li class="nav-item">
            <a class="nav-link active" href="{{ route('inhabilitados.index', [ 'convocatoria_id' =>$convocatoria_id ]) }} ">Inhabilitados</a>
        </li>
        <li class="nav-item">
            <a class="nav-link active" href="{{ route('habilitados.index', [ 'convocatoria_id' =>$convocatoria_id ]) }} ">Habilitados</a>
        </li>
    </ul>
</div>
<div class="container">
    <!-- tabla de habilitados -->
    <table class="table table-light table-responsive table-bordered table-hover">
        <thead class="thead-light">
            <tr>
                <th>#</th>
                <th>Numero de Asignatura</th>
                <th>Apellido</th>
                <th>Nombre</th>
                <th>Asignatura</th>
                <th>Convocatoria</th>
                <th>Fecha de Convocatoria</th>
                <th>Estado</th>
            </tr>

            @foreach($postulantes as $postulante)
            <tr>
                <td>{{$postulante->id}}</td>
                <td>{{$postulante->requerimiento_id}}</td>
                <td>{{$postulante->last_name}}</td>
                <td>{{$postulante->name}}</td>
                <td>{{$postulante->destino}}</td>
                <td>{{$postulante->titulo_convocatoria}}</td>
                <td>{{$postulante->fecha_publicacion}}</td>
                <td>Habilitado</td>
            </tr>
            @endforeach
    </table>
</div>

@endsection