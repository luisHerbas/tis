@extends('layouts.app3')
@section('content')

<!-- formulario de registro de postulantes -->
<div class="container">
    <title>Registro de Postulantes</title>
    <div class="container"><br>
        <div class="row justify-content-center align-items-center min-vh-100">
            <div class="col-8">
                <h2 class="text-center">REGISTRAR POSTULANTE</h2>
                <form action="{{ url('/postulant')}}" method="post" enctype="multipart/form-data">
                    <br><br>
                    {{ csrf_field() }}

                    <div class="form-group">
                        <!-- label: codigo sis -->
                        <label for="cod_sis" class="control-label h3">{{'Código SIS: '}}</label>
                        <!-- input: codigo sis -->
                        <input type="text" class="form-control {{$errors->has('cod_sis')?'is-invalid':'' }}" name="cod_sis" id="cod_sis" value="{{ isset($postulant->cod_sis) ? $postulant->cod_sis:old('cod_sis')}}" required maxlength="55">
                        {!! $errors->first('cod_sis','<div class="invalid-feedback">:message</div>') !!}

                    </div>

                    <div class="form-group">
                        <!-- label: ci -->
                        <label for="ci" class="control-label h3">{{'Carnet de identidad: '}}</label>
                        <!-- input: ci -->
                        <input type="number" class="form-control {{$errors->has('ci')?'is-invalid':'' }}" name="ci" id="ci" value="{{ isset($postulant->ci) ? $postulant->ci:old('ci')}}" required>
                        {!! $errors->first('ci','<div class="invalid-feedback">:message</div>') !!}

                    </div>

                    <div class="form-group">
                        <!-- label: direccion -->
                        <label for="address" class="control-label h3">{{'Direccion: '}}</label>
                        <!-- input: direccion -->
                        <input type="text" class="form-control {{$errors->has('address')?'is-invalid':'' }}" name="address" id="address" value="{{ isset($postulant->address) ? $postulant->address:old('address')}}" required maxlength="55">
                        {!! $errors->first('address','<div class="invalid-feedback">:message</div>') !!}

                    </div>

                    <div class="form-group">
                        <!-- label: telefono -->
                        <label for="phone" class="control-label h3">{{'Telefono: '}}</label>
                        <!-- input: telefono -->
                        <input type="text" class="form-control {{$errors->has('phone')?'is-invalid':'' }}" name="phone" id="phone" value="{{ isset($postulant->phone) ? $postulant->phone:old('phone')}}" required maxlength="8" minlength="8">
                        {!! $errors->first('phone','<div class="invalid-feedback">:message</div>') !!}

                    </div>

                    <div class="form-group">
                        <!-- label: email -->
                        <label for="phone" class="control-label h3">{{'Email: '}}</label>
                        <!-- input: email -->
                        <input type="email" class="form-control {{$errors->has('email')?'is-invalid':'' }}" name="email" id="email" value="{{ isset($postulant->email) ? $postulant->email:old('email')}}" required>
                        {!! $errors->first('email','<div class="invalid-feedback">:message</div>') !!}

                    </div>

                    <div class="form-group">
                        <!-- label: N° documentos -->
                        <label for="nro_documents" class="control-label h3">{{'Numero de documentos: '}}</label>
                        <!-- input: N° documentos -->
                        <input type="number" class="form-control {{$errors->has('nro_documents')?'is-invalid':'' }}" name="nro_documents" id="nro_documents" value="{{ isset($postulant->nro_documents) ? $postulant->nro_documents:old('nro_documents')}}" required>
                        {!! $errors->first('nro_documents','<div class="invalid-feedback">:message</div>') !!}

                    </div>
                    <div class="form-group">
                        <!-- label: N° certificados -->
                        <label for="nro_certificates" class="control-label h3">{{'Numero de certificados: '}}</label>
                        <!-- input: N° certificados -->
                        <input type="number" class="form-control {{$errors->has('nro_certificates')?'is-invalid':'' }}" name="nro_certificates" id="nro_certificates" value="{{ isset($postulant->nro_certificates) ? $postulant->nro_certificates:old('nro_certificates')}}" required>
                        {!! $errors->first('nro_certificates','<div class="invalid-feedback">:message</div>') !!}

                    </div>

                    <div class="form-group">
                        <!-- label: departamento -->
                        <label for="departamento" class="h3">{{'Departamento: '}}</label> &nbsp;&nbsp;&nbsp;&nbsp;
                        <!-- lista desplegable de departamentos -->
                        {!! Form::select('departamento', $departamentos, null, ['placeholder' => 'Selecciona un departamento', 'class' => 'form-control', 'id'=>'departamento']) !!}
                    </div>


                    <div class="form-group">
                        <!-- label: subunidad -->
                        <label for="subunidad" class="h3">{{'Subunidad: '}}</label>
                        <!-- lista desplegable de subunidades -->
                        <select class="form-control {{$errors->has('subunidad')?'is-invalid':'' }}" name="subunidad" id="subunidad" required>
                        </select>
                        {!! $errors->first('subunidad','<div class="invalid-feedback">:message</div>') !!}
                    </div>


                    <div class="form-group">
                        <!-- label: asignatura -->
                        <label for="requerimiento" class="h3">{{'Asignatura: '}}</label>
                        <!-- lista desplegable de asignaturas -->
                        <select class="form-control {{$errors->has('requerimiento')?'is-invalid':'' }}" name="requerimiento" id="requerimiento" required>
                        </select>
                        {!! $errors->first('requerimiento','<div class="invalid-feedback">:message</div>') !!}
                    </div>
                    <br>
                    <!-- botones aceptar y cancelar -->
                    <button class="btn btn-success" type="submit">Aceptar</button>
                    <a href="{{ url('/') }} " class="btn btn-secondary">Cancelar</a>
                </form>
            </div>
        </div>
    </div>
</div>
<br><br><br>

@endsection

@section('script')
<!-- verificacion de departamentos y subunidades -->
<script>
    $("#departamento").change(function(event) {
        jQuery.get("subunidades/" + event.target.value + "", function(response, departamento) {
            console.log(response);
            $("#subunidad").empty();
            $("#subunidad").append("<option value=''>Seleccione una subunidad </option>");
            for (i = 0; i < response.length; i++) {
                $("#subunidad").append("<option value='" + response[i].id + "'> " + response[i].name + "</option>");
            }
        });

    });

    $("#subunidad").change(function(event) {
        jQuery.get("convocatorias/" + event.target.value + "", function(response, subunidad) {
            console.log(response);
            $("#requerimiento").empty();
            for (i = 0; i < response.length; i++) {
                $("#requerimiento").append("<option value='" + response[i].id + "'> " + response[i].destino + "</option>");
            }
        });
    });
</script>
@endsection