@extends('layouts.app2')
@section('content')

<br>
<!-- vista de todos los postulantes -->
<h2 class="text-center">POSTULANTES</h2><br>
<nav class="navbar navbar-default navbar-static-top container">
    <div class="container">
        <div class="navbar-header">
        </div>
    </div>
</nav>
<h6> <strong>Nota: Una vez que se inhabilite a un postulante este se mostrara en la seccion de avisos en el apartado de resultados finales, indicando el nombre del postulante y la observacion que tuvo</strong> </h6>
<!-- navbar de estados de postulantes -->
<div class="navbar" id="app-navbar">
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link active" href="{{ route('listaspostulantes.index', [ 'convocatoria_id' =>$convocatoria_id ]) }} ">Postulantes</a>
        </li>
        <li class="nav-item">
            <a class="nav-link active" href="{{ route('inhabilitados.index', [ 'convocatoria_id' =>$convocatoria_id ]) }} ">Inhabilitados</a>
        </li>
        <li class="nav-item">
            <a class="nav-link active" href="{{ route('habilitados.index', [ 'convocatoria_id' =>$convocatoria_id ]) }} ">Habilitados</a>
        </li>
    </ul>
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js" type="text/javascript"></script>

<div class="container">
    <!-- tabla de postulantes -->
    <table class="table table-light table-responsive table-bordered table-hover">
        <thead class="thead-light">
            <tr>
                <th>#</th>
                <th>Numero de Asignatura</th>
                <th>Apellido</th>
                <th>Nombre</th>
                <th>Asignatura</th>
                <th>Convocatoria</th>
                <th>Fecha de Convocatoria</th>
                <th>Estado</th>
                <th>Habilitar/Inhabilitar</th>
            </tr>

            @foreach($postulantes as $postulante)
            <tr>
                <td>{{$postulante->id}}</td>
                <td>{{$postulante->requerimiento_id}}</td>
                <td>{{$postulante->last_name}}</td>
                <td>{{$postulante->name}}</td>
                <td>{{$postulante->destino}}</td>
                <td>{{$postulante->titulo_convocatoria}}</td>
                <td>{{$postulante->fecha_publicacion}}</td>
                <td>{{$postulante->id_status}}</td>
                <td>
                    <a class="btn btn-info">Documentos</a>
                </td>
            </tr>
            @endforeach
    </table>
</div>
<!-- verificacion de documentos -->
<script>
    $(document).ready(function() {
        $(".btn").click(function() {
            var sis = $(this).parents("tr").find("td").eq(0).html();
            var convocatoria = $(this).parents("tr").find("td").eq(1).html();
            location.href = "documents/show/" + sis + "/" + convocatoria
        });
    });
</script>

@endsection