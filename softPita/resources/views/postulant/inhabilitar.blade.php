@extends('layouts.app2')
<!-- link boostrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
@section('content')

<!-- formulario de inhabilitacion -->
<div class="container"><br>
    <form action="{{ route('inhabilitar.store', [ 'convocatoria_id' =>$convocatoria_id ]) }}" method="post">
        <div class="form-group">
            <h3 class="text-center">INDIQUE LAS RAZONES PARA LA INHABILITACION</h3><br><br>
            <!-- textarea: razones de inhabilitacion -->
            <textarea class="form-control" name="observaciones" rows="3" required></textarea>
        </div>
        {!! csrf_field() !!}
        <!-- botones aceptar y cancelar -->
        <button type="submit" class="btn btn-success">Aceptar</button>
        <a href="{{ URL::previous() }}" class="btn btn-secondary">Cancelar</a>
        <input type="hidden" name="sis" value="{{$sis}}">
    </form>
</div>

@endsection