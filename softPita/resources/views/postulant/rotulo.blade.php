@extends('layouts.app3')
@section('content')

<!-- vista rotulo del postulante -->
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-lg-8 offset-lg-2 "><br>
            <div class="card">
                <div class="card-header text-uppercase">
                    <h1 class="text-center"> Vista previa del r&oacute;tulo</h1>
                </div><br>

                <!-- contenido del rotulo -->
                <div class="card-body">
                    <h3>C&oacute;digo SIS: <small class="text-muted"> {{ $postulant->cod_sis }}</small></h3>
                    <h3>Nombre(s): <small class="text-muted"> {{ $postulant->name }}</small></h3>
                    <h3>Apellidos: <small class="text-muted"> {{ $postulant->last_name }}</small></h3>
                    <h3>Tel&eacute;fono: <small class="text-muted"> {{ $postulant->phone }}</small></h3>
                    <h3>Correo: <small class="text-muted"> {{ $postulant->email }}</small></h3>
                    <h3>Asignatura: <small class="text-muted"> {{ $postulant->destino }}</small></h3>
                    <h3>Subunidad: <small class="text-muted"> {{ $postulant->subunidadname }}</small></h3>
                    <h3>Departamento: <small class="text-muted"> {{ $postulant->departamentoname}}</small></h3><br><br>

                    <!-- botones pdf, salir, volver -->
                    <button type="button" class="btn btn-success btn-lg" onclick="javascript:window.print()">PDF</button>
                    <a href="{{  route('home') }}" class="btn btn-danger btn-lg" style="background-color: #C92023">Salir</a>
                    <a href="{{ URL::previous() }}" class="btn btn-secondary btn-lg">volver</a>
                </div>
            </div>
            <br><br>
        </div>
    </div>
</div>

@endsection