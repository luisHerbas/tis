@extends('layouts.app3')
@section('content')

<!-- Formulario de inicio de sesión -->
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default"><br>
                <div class="panel-heading h2">Iniciar sesi&oacute;n</div><br>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <!-- label: correo electrónico -->
                            <label for="email" class="col-md-4 control-label">Correo electr&oacute;nico</label>

                            <div class="col-md-6">
                                <!-- input: correo electrónico -->
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <!-- label: contraseña -->
                            <label for="password" class="col-md-4 control-label">Contraseña</label>
                            <div class="col-md-6">
                                <!-- input: contraseña -->
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <!-- radio button -->
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Recu&eacute;rdame
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <!-- botones: ingresar y cancelar -->
                                <button type="submit" class="btn btn-success">Ingresar</button>
                                <a href="{{ URL::previous() }}" class="btn btn-secondary">Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection