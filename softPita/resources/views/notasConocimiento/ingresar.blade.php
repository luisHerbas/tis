@extends('layouts.app2')
<!-- link bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
@section('content')
<!-- vista que manda a form1 -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><br>
                    <h2 class="text-center">INGRESAR NOTA</h2>
                </div>
                <!-- tabla de notas de postulantes form1-->
                <div class="panel-body">
                    {!! Form::model($notasConocimiento, ['route' => ['notasConocimiento.update', 'convocatoria_id' =>$convocatoria_id, 'postulante_id' =>$postulante_id ],
                    'method' => 'PUT']) !!}

                    @include('notasConocimiento.partials.form1')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection