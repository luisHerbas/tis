@extends('layouts.app2')
@section('content')

<!-- link bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<!-- formulario para modificar nota -->
<div class="container h4"><br>
  <h2 class="text-center">MODIFICAR NOTA</h2><br><br>
  <form method="post" action="{{ route('notasConocimiento.calificarupdate', [ 'convocatoria_id' =>$convocatoria_id, 'postulante_id'=> $postulante_id, 'notaConocimiento_id'=> $nota->id ]) }}">
    {{ method_field('PUT') }}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="form-group">
      <!-- label: nota -->
      <label for="">Ingresar Nota (El valor maximo para esta nota es de {{$notas->porcentaje}} pts) :</label>
      <!-- input: nota -->
      <input type="number" class="form-control" name="nota" value="{{ isset($nota->nota) ? $nota->nota:old('nota')}}" required min="1" max="{{$notas->porcentaje}}">
    </div>

    <div>
      <!-- botones aceptar y cancelar -->
      <button class="btn btn-success" type="submit">Aceptar</button>
      <a href="{{ URL::previous() }}" class="btn btn-secondary">Cancelar</a>
    </div>
  </form>
</div>

@endsection