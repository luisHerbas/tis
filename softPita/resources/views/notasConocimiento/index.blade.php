@extends('layouts.app2')
<!-- link bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
@section('content')

<!-- inicio notas conocimiento -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><br>
                    <h2 class="text-center">NOTAS CONOCIMIENTO</h2><br><br>
                    <p> <strong> Nota: Mientras los resultados de conocimientos no se publiquen, no se mostraran en la seccion de avisos.</strong> </p>
                    <!-- boton publicar notas conocimiento -->
                    @if($convocatoria->publicacion_conocimientos == 1)
                    <p> <strong> Los resultados de conocimientos de esta convocatoria no se publicaron aun </strong> </p>
                    <a class="btn btn-success text-white" href="{{ url('notasConocimiento/publicar/'.$convocatoria->id)}}">Publicar Resultados</a>
                    <!-- boton dejar de publicar notas conocimiento -->
                    @elseif($convocatoria->publicacion_conocimientos == 2)
                    <p> <strong> Los resultados de conocimientos de esta convocatoria se estan publicando </strong> </p>
                    <a class="btn btn-danger text-white" style="background:#C92023" href="{{ url('notasConocimiento/publicar/'.$convocatoria->id)}}">Dejar de publicar Resultados</a>
                    @endif
                    <!-- boton volver -->
                    <a class="btn btn-secondary float-right" href="{{ route('notasConocimiento.preindex')}}">Volver</a><br>
                    <a class="btn  hide text-white" href="{{ route('notasConocimiento.preindex')}}">.</a>
                    <!-- tabla de asignaturas -->
                    @foreach($nombres as $nombre)
                    <div class="panel-body">
                        <table class="table table-hover table-bordered">
                            <thead class="thead-light">
                                <tr>
                                    <th>Asignatura</th>
                                    <th>Accion</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        {{$nombre->destino}}
                                    </td>
                                    <td width="200px">
                                        <a class="btn" style="background-color: #172962; color:white" href="{{ route('nota.index', [ 'convocatoria_id' => $convocatoria_id, 'requerimiento_id' => $nombre->requerimiento_id ])}}">Ver examenes que se calificaran</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- tabla de postulantes -->
                        @endforeach
                        <table class="table table-hover table-bordered">
                            <thead class="thead-light">
                                <tr>
                                    <th>Apellidos</th>
                                    <th>Nombres</th>
                                    <th>Item</th>
                                    <th>Nota</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach($postulantes as $postulante)
                                <tr>
                                    <td>{{$postulante->last_name}}</td>
                                    <td>{{$postulante->name}}</td>
                                    <td>{{$postulante->destino}}</td>
                                    <td>{{$postulante->nota_conocimientos}}</td>
                                    <td>
                                        <!-- boton calificar -->
                                        <a class="btn btn-info" href="{{ route('notasConocimiento.ingresar', [ 'convocatoria_id'=>$convocatoria_id, 'postulante_id' => $postulante->id ])}}">Calificar</a>
                                    </td>
                                </tr>
                                @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection