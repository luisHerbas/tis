@extends('layouts.app2')
<!-- link bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
@section('content')

    <!-- filtraje por departamento y subunidad al inicio de notas concimiento -->
    <div class="form-group"><br>
        <h2 class="text-center">NOTAS CONOCIMIENTO</h2><br><br>
        <!-- label: departamento -->
        <label for="departamento" class="h4">{{'Departamento: '}}</label> &nbsp;&nbsp;&nbsp;&nbsp;
        <!-- lista desplegable departamentos -->
        <select id="departamento" class="form-control" name="departamento">
            <option value="">Selecciona un departamento</option>
            @foreach ($departamentos as $departamento)
            <option value="{{$departamento->id}}">{{$departamento->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <!-- label: subunidad -->
        <label for="subunidad" class="h4">{{'Subunidad: '}}</label>
        <!-- lista desplegable subunidades -->
        <select class="form-control {{$errors->has('subunidad')?'is-invalid':'' }}" name="subunidad" id="subunidad">
        </select>
        {!! $errors->first('subunidad','<div class="invalid-feedback">:message</div>') !!}
    </div>

    <!-- tabla de convocatorias -->
    <table class="table table-hover table-bordered">
        <thead class="thead-light">
            <tr>
                <th scope="col" class="text-center">Nombre Convocatoria</th>
            </tr>
        </thead>
        <tbody class="text-center" name="convocatoria" id="convocatoria">
        </tbody>
    </table>

@endsection
@section('script')

<!-- script de verificacion de departamento y subunidades -->
<script>
    $("#departamento").change(function(event) {
        jQuery.get("notasMerito/subunidades/" + event.target.value + "", function(response, departamento) {
            console.log(response);
            $("#subunidad").empty();
            $("#subunidad").append("<option value=''>Seleccione una subunidad </option>");
            for (i = 0; i < response.length; i++) {
                $("#subunidad").append("<option value='" + response[i].id + "'> " + response[i].name + "</option>");
            }
        });
    });

    $("#subunidad").change(function(event) {
        jQuery.get("notasConocimiento/convocatorias/" + event.target.value + "", function(response, subunidad) {
            console.log(response);
            $("#convocatoria").empty();
            for (i = 0; i < response.length; i++) {
                $("#convocatoria").append("<tr><td value='" + response[i].id + "'> <a href='notasConocimiento/index/" + response[i].id + "'>" + response[i].titulo_convocatoria + "</td></a></tr>");
            }
        });
    });
</script>

@endsection