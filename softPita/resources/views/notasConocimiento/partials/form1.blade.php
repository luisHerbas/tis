<!-- calculo total -->
<script>
    function calcular_total() {
        nota_final = 0
        nota_conocimiento = 0
        $(".nota_parcial").each(
            function(index, value) {
                nota_final = nota_final + eval($(this).val());
            }
        );
        $("#total").val(nota_final);
    }
</script>
<div class="form-group"><br><br>
    <hr>
    <h4>Nombre postulante: {{$notasConocimiento1->last_name}} {{$notasConocimiento1->name}}</h4><br>
    <!-- tabla de notas conocimiento por postulante -->
    <table class="table table-hover table-bordered text-center">
        <thead class="thead-light">
            <tr>
                <th>Nombre evaluacion</th>
                <th width="10px">Porcentaje</th>
                <th width="10px">Nota</th>
                <th>Accion</th>
            </tr>
        </thead>
        <tbody>
            @php
            $contador=0;
            $suma=0;
            @endphp
            @foreach($notasConocimiento as $nota)
            <tr>
                @php
                $suma+=$nota->nota;
                @endphp
                <td>
                    {{$nota->nombre_nota}}
                </td>
                <td>
                    {{$nota->porcentaje}} pts.
                </td>
                <td><label class="nota_parcial">{{$nota->nota}}</td>
                <td width="10px">
                    <!-- boton modificar nota -->
                    <a class="btn btn-warning text-white" href="{{ route('notasConocimiento.calificar', [ 'convocatoria_id'=>$convocatoria_id, 'postulante_id' => $nota->postulante_id, 'nota_id'=>$nota->id ])}}">Modificar</a>
                </td>
                @php
                $contador++;
                @endphp
            </tr>
            @endforeach
            <input type="hidden" id="nota_conocimiento" name="nota_conocimiento" min="1" max="100" class="form-control" value="{{$suma}}" />
        </tbody>

    </table><br>
</div>
<div class="form-group">
    <!-- botones guardar y cancelar -->
    <form method=post onsubmit="return doSomething()">
    <input type=submit class="btn btn-success" value="Guardar">
        <a href="{{ route('notasConocimiento.index', [ 'convocatoria_id' =>$convocatoria_id ]) }}" class="btn btn-secondary">Cancelar</a>
    </form>
</div>