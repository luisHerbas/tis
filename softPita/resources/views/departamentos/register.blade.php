
<title>Registro de un Nuevo Departamento</title>

@extends('layouts.app2')
<!-- link bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
@section('content')

<!-- formulario para registrar departamento -->
<div class="container h5">
    <div><br>
        <h2 class="text-center">REGISTRAR DEPARTAMENTO</h2><br><br>
        <div class="col-8">
            <form action="{{url('departamento')}}" method="POST" class="pb-5">
                <div class="form-group">
                    <!-- label: nombre departamento -->
                    <label>Nombre del departamento:</label>
                    <!-- input: nombre departamento -->
                    <input type="text" class="form-control {{$errors->has('name')?'is-invalid':'' }}" name="name" id="name" value="{{ isset($departamento->name) ? $departamento->name:old('name')}}">
                    {!! $errors->first('name','<div class="invalid-feedback">:message</div>') !!}

                </div>
                {!! csrf_field() !!}
                <!-- botones aceptar y cancelar -->
                <button type="submit" class="btn btn-primary">Aceptar</button>
                <a href="{{ url('departamentos') }}" class="btn btn-secondary">Cancelar</a>
            </form>
        </div>
    </div>
</div>
@endsection