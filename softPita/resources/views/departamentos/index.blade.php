
<title>Departamentos</title>

@extends('layouts.app2')
@section('content')

<!-- pagina de incio de departamentos -->
<div class="container">
    <!-- tabla de departamentos -->
    <table class="table table-bordered table-hover"><br>
        <thead class="thead-light">
            <h2 class="text-center">DEPARTAMENTOS</h2><br><br>
            <!-- boton agregar departamento -->
            <a class="btn  float-right" style="background:#172962; color:white" href="{{url('departamento/register')}}">Agregar Departamento</a><br><br>
            <tr>
                @foreach($cabeceras as $cabecera)
                <th>{{$cabecera}}</th>
                @endforeach
                <th>SubUnidades</th>
                <th>Acciones</th>
        </thead>
        </tr>
        @foreach($departamentos as $departamento)
        <tr>
            @foreach($cabeceras as $cabeza)
            <td>{{ $departamento->$cabeza }}</td>
            @endforeach
            <td>
                <!-- boton ver -->
                <a class="btn btn-info" href="{{route('subunidad.index', ['departamento_id' => $departamento->id ])}}">Ver SubUnidades</a>
            </td>
            <td>
                <form method="post" action="{{ url('departamentos/'.$departamento->id)}}">
                    <!-- boton editar, borrar -->
                    <a class="btn btn-warning text-white" href="{{url('departamentos/edit/'.$departamento->id)}}">Editar</a>
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <button type="submit" class="btn btn-danger" style="background-color: #C92023" onclick="return confirm('¿Esta seguro de que desea eliminar el elemento?');">Eliminar</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
</div><br><br>

@endsection
<!-- scripts javascript -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>