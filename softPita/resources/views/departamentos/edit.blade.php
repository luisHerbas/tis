<title>Edicion de un Departamento</title>

@extends('layouts.app2')
<!-- link de bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
@section('content')

<!-- formulario para editar un departamento -->
<div class="container h5">
    <div><br>
        <h2 class="text-center">EDITAR DEPARTAMENTO</h2><br><br>
        <div class="col-8">
            <!-- recupera los datos anteriores -->
            <form action="{{route('departamentos.update', [ 'id'=> $id ]) }}" method="post" class="pb-5">
                {{ method_field('PUT') }}
                <div class="form-group">
                    <!-- label: departamento -->
                    <label>Nombre del departamento:</label>
                    <!-- input: departamento -->
                    <input type="text" class="form-control {{$errors->has('name')?'is-invalid':'' }}" name="name" id="name" value="{{ isset($departamento->name) ? $departamento->name:old('name')}}">
                    {!! $errors->first('name','<div class="invalid-feedback">:message</div>') !!}
                </div>
                {!! csrf_field() !!}
                <!-- botones de aceptar y cancelar -->
                <button type="submit" class="btn btn-primary">Aceptar</button>
                <a href="{{ url('departamentos') }}" class="btn btn-secondary">Cancelar</a>
            </form>
        </div>
    </div>
</div>

@endsection