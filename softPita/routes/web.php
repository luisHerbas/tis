<?php

use App\Convocatoria;
use App\Requerimiento;
use App\Documento;
use App\Postulant;


// departamentos
Route::get('departamento/register', 'DepartamentoController@create')
    ->middleware('permission:departamentos.create');
Route::post('departamento/', 'DepartamentoController@store');
Route::get('departamentos', 'DepartamentoController@index')->name('departamentos.show')
    ->middleware('permission:departamentos.show');

Route::get('departamentos/edit/{id}', 'DepartamentoController@edit')
    ->middleware('permission:departamentos.edit');
Route::put('departamentos/{id}', 'DepartamentoController@update')->name('departamentos.update');
Route::delete('departamentos/{id}', 'DepartamentoController@destroy')->name('departamentos.destroy')
    ->middleware('permission:departamentos.destroy');
// subunidades

Route::get('departamentos/{id_departamento}/subUnidades', 'SubunidadController@index')->name('subunidad.index')
    ->middleware('permission:subunidades.index');
Route::get('departamentos/{id_departamento}/subUnidades/create', 'SubunidadController@create')->name('subunidad.create')
    ->middleware('permission:subunidades.create');
Route::post('departamentos/{id_departamento}/subUnidades', 'SubunidadController@store')->name('subunidad.store');
Route::get('departamentos/{id_dep}/subUnidades/{id_sub}', 'SubunidadController@edit')->name('subunidad.edit')
    ->middleware('permission:subunidades.edit');
Route::put('departamentos/{id_dep}/subUnidades/{id_sub}', 'SubunidadController@update')->name('subunidad.update');
Route::delete('departamentos/{id_dep}/subUnidades/{id_sub}', 'SubunidadController@destroy')->name('subunidad.destroy')
    ->middleware('permission:subunidades.destroy');


Route::get('lista/documents/show/{sis}/{convocatoria}', function ($sis, $convocatoria) {
    $requerimientos = Requerimiento::where('id', $convocatoria)->first();
    $documents = Documento::where('convocatoria_id', $requerimientos->convocatoria_id)->get();
    return view('documentos.show', ['documents' => $documents, 'sis' => $sis, 'convocatoria_id' => $requerimientos->convocatoria_id]);
})->name('documents')->middleware('permission:documentos.show');

//Postulantes

Route::post('postulant/', 'PostulantController@store');
Route::get('postulant/register', 'PostulantController@create');
Route::get('postulant/subunidades/{id}', 'PostulantController@getSubunidades');
Route::get('postulant/convocatorias/{id}', 'PostulantController@getConvocatorias');
Route::get('postulant/rotulo/{cod_sis}', 'PostulantController@create_rotulo');
Route::post('habilitar/{convocatoria_id}', 'PostulantController@habilitar')->name('habilitar.store');
Route::post('inhabilitar/{convocatoria_id}', 'PostulantController@inhabilitar')->name('inhabilitar.store');

Route::get('lista/{convocatoria_id}', 'PostulantController@index')->name('listaspostulantes.index')
    ->middleware('permission:listaspostulantes.index');

Route::get('lista_habilitados/{covocatoria_id}', 'PostulantController@index_habilitados')->name('habilitados.index');

Route::get('lista_inhabilitados/{covocatoria_id}', 'PostulantController@index_inhabilitados')->name('inhabilitados.index');
Route::get('inhabilitar_postulante/{sis}/{convocatoria_id}', 'PostulantController@create_inhabilitar')->name('inhabilitar.index');

Route::get('postulantes', 'PostulantController@filtraje')->name('postulantes.filtraje')
    ->middleware('permission:listaspostulantes.index');

//Notas conocimiento
Route::get('notasConocimiento/subunidades/{id}', 'NotaConocimientoController@getSubunidades');
Route::get('notasConocimiento/convocatorias/{id}', 'NotaConocimientoController@getConvocatorias');

Route::resource('convocatorias', 'ConvocatoriaController');
Route::resource('documentos', 'DocumentoController');
Route::resource('fechas', 'FechaPruebaController');

Route::get('notasMerito/subunidades/{id}', 'NotaMeritoController@getSubunidades');
Route::get('notasMerito/postulantes/{id}', 'NotaMeritoController@getPostulantes');

Route::get('convocatorias/{convocatoria_id}/requerimientos', 'RequerimientoController@index')->name('reque.index');
Route::get('convocatorias/{convocatoria_id}/requerimientos/create', 'RequerimientoController@create')->name('reque.create');
Route::post('convocatorias/{convocatoria_id}/requerimientos', 'RequerimientoController@store')->name('reque.store');
Route::get('convocatorias/{convocatoria_id}/requerimientos/{requerimiento_id}', 'RequerimientoController@edit')->name('reque.edit');
Route::put('convocatorias/{convocatoria_id}/requerimientos/{requerimiento_id}', 'RequerimientoController@update')->name('reque.update');
Route::delete('convocatorias/{convocatoria_id}/requerimientos/{requerimiento_id}', 'RequerimientoController@destroy')->name('reque.delete');

Route::get('convocatorias/{convocatoria_id}/requisitos', 'RequisitoController@index')->name('requisito.index');
Route::get('convocatorias/{convocatoria_id}/requisitos/create', 'RequisitoController@create')->name('requisito.create');
Route::post('convocatorias/{convocatoria_id}/requisitos', 'RequisitoController@store')->name('requisito.store');
Route::get('convocatorias/{convocatoria_id}/requisitos/{requisito_id}', 'RequisitoController@edit')->name('requisito.edit');
Route::put('convocatorias/{convocatoria_id}/requisitos/{requisito_id}', 'RequisitoController@update')->name('requisito.update');
Route::delete('convocatorias/{convocatoria_id}/requisitos/{requisito_id}', 'RequisitoController@destroy')->name('requisito.delete');

Route::get('convocatorias/{convocatoria_id}/documentos', 'DocumentoController@index')->name('documento.index');
Route::get('convocatorias/{convocatoria_id}/documentos/create', 'DocumentoController@create')->name('documento.create');
Route::post('convocatorias/{convocatoria_id}/documentos', 'DocumentoController@store')->name('documento.store');
Route::get('convocatorias/{convocatoria_id}/documentos/{requisito_id}', 'DocumentoController@edit')->name('documento.edit');
Route::put('convocatorias/{convocatoria_id}/documentos/{requisito_id}', 'DocumentoController@update')->name('documento.update');
Route::delete('convocatorias/{convocatoria_id}/documentos/{requisito_id}', 'DocumentoController@destroy')->name('documento.delete');

Route::get('convocatorias/{convocatoria_id}/fechas', 'FechaPruebaController@index')->name('fecha.index');
Route::get('convocatorias/{convocatoria_id}/fechas/create', 'FechaPruebaController@create')->name('fecha.create');
Route::post('convocatorias/{convocatoria_id}/fechas', 'FechaPruebaController@store')->name('fecha.store');
Route::get('convocatorias/{convocatoria_id}/fechas/{requisito_id}', 'FechaPruebaController@edit')->name('fecha.edit');
Route::put('convocatorias/{convocatoria_id}/fechas/{requisito_id}', 'FechaPruebaController@update')->name('fecha.update');
Route::delete('convocatorias/{convocatoria_id}/fechas/{requisito_id}', 'FechaPruebaController@destroy')->name('fecha.delete');

Route::get('convocatorias/{convocatoria_id}/detalle', 'DetalleController@index')->name('detalle.index');


Route::get('convocatorias/visualizar/convocatoria_vigente', 'VisualizarConvocatoriaController@index')->name('visualizar.index');

Route::get('departamentos/visualizar/historial', 'Historial@index')->name('historial.index');
Route::get('departamentos/visualizar/{departamento_id}', 'Historial@indexconvocatoria')->name('historial.indexconvocatoria');
Route::get('departamentos/visualizar/subunidades/{id}', 'Historial@getSubunidades');
Route::get('departamentos/visualizar/convocatorias/{id}', 'Historial@getConvocatorias');
Route::get('departamentos/{departamento_id}/visualizar/{convocatoria_id}', 'Historial@indexVisualizar')->name('historial.indexVisualizar');

Route::get('convocatorias/{convocatoria_id}/visualizar/convocatorias_vigente', 'VisualizarConvocatoriaController@indexAdmin')->name('visualizar.indexAdmin');

Route::get('convocatorias/{convocatoria_id}/visualizar/convocatorias_vigente2', 'VisualizarConvocatoriaController@indexparaadmin')->name('visualizar.indexparadmin');


Route::get('convocatorias/{convocatoria_id}/meritos', 'MeritosController@index')->name('merito.index');
Route::get('convocatorias/{convocatoria_id}/meritos/create', 'MeritosController@create')->name('merito.create');
Route::post('convocatorias/{convocatoria_id}/meritos', 'MeritosController@store')->name('merito.store');
Route::get('convocatorias/{convocatoria_id}/meritos/{merito_id}', 'MeritosController@edit')->name('merito.edit');
Route::put('convocatorias/{convocatoria_id}/meritos/{merito_id}', 'MeritosController@update')->name('merito.update');
Route::delete('convocatorias/{convocatoria_id}/meritos/{merito_id}', 'MeritosController@destroy')->name('merito.delete');

Route::get('convocatorias/{convocatoria_id}/meritos1', 'Meritos1Controller@index')->name('merito1.index');
Route::get('convocatorias/{convocatoria_id}/meritos1/create', 'Meritos1Controller@create')->name('merito1.create');
Route::post('convocatorias/{convocatoria_id}/meritos1', 'Meritos1Controller@store')->name('merito1.store');
Route::get('convocatorias/{convocatoria_id}/meritos1/{merito1_id}', 'Meritos1Controller@edit')->name('merito1.edit');
Route::put('convocatorias/{convocatoria_id}/meritos1/{merito1_id}', 'Meritos1Controller@update')->name('merito1.update');
Route::delete('convocatorias/{convocatoria_id}/meritos1/{merito1_id}', 'Meritos1Controller@destroy')->name('merito1.delete');


Route::get('convocatorias/{convocatoria_id}/mertos', 'MertosController@index')->name('merto.index');
Route::get('convocatorias/{convocatoria_id}/mertos/create', 'MertosController@create')->name('merto.create');
Route::post('convocatorias/{convocatoria_id}/mertos', 'MertosController@store')->name('merto.store');
Route::get('convocatorias/{convocatoria_id}/mertos/{merto_id}', 'MertosController@edit')->name('merto.edit');
Route::put('convocatorias/{convocatoria_id}/mertos/{merto_id}', 'MertosController@update')->name('merto.update');
Route::delete('convocatorias/{convocatoria_id}/mertos/{merto_id}', 'MertosController@destroy')->name('merto.delete');

Route::get('departamentos/{departamento_id}/itemsDep', 'ItemDepController@index')->name('itemsDep.index');
Route::get('departamentos/{departamento_id}/itemsDep/create', 'ItemDepController@create')->name('itemsDep.create');
Route::post('departamentos/{departamento_id}/itemsDep', 'ItemDepController@store')->name('itemsDep.store');
Route::get('departamentos/{departamento_id}/itemsDep/{itemDep_id}', 'ItemDepController@edit')->name('itemsDep.edit');
Route::put('departamentos/{departamento_id}/itemsDep/{itemDep_id}', 'ItemDepController@update')->name('itemsDep.update');
Route::delete('departamentos/{departamento_id}/itemsDep/{itemDep_id}', 'ItemDepController@destroy')->name('itemsDep.delete');

Route::get('convocatorias/subunidades/{id}', 'ConvocatoriaController@getSubunidades');
Route::get('convocatorias/{convocatoria}/subunidades/{id}', 'ConvocatoriaController@getSubunidades1');

Route::get('/home_diego', function () {
    return view('home_diego');
});

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/sobreNosotros', function () {
    return view('sobreNosotros');
});

Route::get('avisos/', 'ConvocatoriaController@publicar')->name('convocatorias.publicar');

Route::get('avisos/conocimiento', 'ConvocatoriaController@conocimiento')->name('convocatorias.conocimiento');

Route::get('avisos/meritos', 'ConvocatoriaController@merito')->name('convocatorias.merito');

Route::get('avisos/finales', 'NotaController@publicar')->name('finales.publicar');

Route::get('avisos/resultadosfinales/{convocatoria_id}', 'NotaController@publicarfinales')->name('resultadosfinales.publicar');



Route::resource('requerimientos', 'RequerimientoController');
Route::resource('requisitos', 'RequisitoController');

Route::get('/', 'HomeController@index');

Route::get('/administrador', 'AdministradorController@index')->name('administrador.home');;

Route::get('/listapostulantes', function () {
    return view('mostrarPostulantesLista');
});

Route::get('/listahabilitados', function () {
    return view('mostrarListaHabilitados');
});

Route::get('/listainhabilitados', function () {
    return view('mostrarListaInhabilitados');
});

Route::get('/listaresultados', function () {
    return view('mostrarListaResultados');
});

Route::get('/pdf', function () {
    return view('vistaPdf');
});

Route::get('/homeAntiguo', function () {
    return view('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


// Middleware para permisos del sistema
Route::middleware(['auth'])->group(function () {
    //Roles

    Route::post('roles/store', 'RoleController@store')->name('roles.store')
        ->middleware('permission:roles.create');

    Route::get('roles/index', 'RoleController@index')->name('roles.index')
        ->middleware('permission:roles.index');

    Route::get('roles/create', 'RoleController@create')->name('roles.create')
        ->middleware('permission:roles.create');

    Route::put('roles/{role}', 'RoleController@update')->name('roles.update')
        ->middleware('permission:roles.edit');

    Route::get('roles/{role}', 'RoleController@show')->name('roles.show')
        ->middleware('permission:roles.show');

    Route::delete('roles/{role}', 'RoleController@destroy')->name('roles.destroy')
        ->middleware('permission:roles.destroy');

    Route::get('roles/{role}/edit', 'RoleController@edit')->name('roles.edit')
        ->middleware('permission:roles.edit');


    //Convocatorias
    //Navegar convocatoria
    Route::post('convocatorias/store', 'ConvocatoriaController@store')->name('convocatorias.store')
        ->middleware('permission:convocatorias.create');
    // Ver detalle de la convocatoria
    Route::get('convocatorias', 'ConvocatoriaController@index')->name('convocatorias.index')
        ->middleware('permission:convocatorias.index');
    // Creacion de una convocatoria-
    Route::get('convocatorias/create', 'ConvocatoriaController@create')->name('convocatorias.create')
        ->middleware('permission:convocatorias.create');
    // Editar una convocatoria-

    Route::get('convocatorias/{convocatoria}/edit', 'ConvocatoriaController@edit')->name('convocatorias.edit')
        ->middleware('permission:convocatorias.edit');

    Route::get('convocatorias/{convocatoria}/inhabilitar', 'ConvocatoriaController@inhabilitar')->name('convocatorias.inhabilitar')
        ->middleware('permission:convocatorias.edit');




    // Ver 1 convocatoria
    Route::get('convocatorias/{convocatoria_id}/visualizar/convocatorias_vigente', 'VisualizarConvocatoriaController@indexAdmin')->name('visualizar.indexAdmin')
        ->middleware('permission:visualizar.indexAdmin');
    // Eliminar una convocatoria
    Route::delete('convocatorias/{convocatoria}', 'ConvocatoriaController@destroy')->name('convocatorias.destroy')
        ->middleware('permission:convocatorias.destroy');
    // Editar una ls detalles de la convocatoria-
    Route::get('convocatorias/{convocatoria_id}/detalle', 'DetalleController@index')->name('detalle.index')
        ->middleware('permission:detalle.index');


    //Convocatorias pasadas
    // Ver detalle de la convocatoria
    Route::get('historicoConvocatorias', 'HistoricoConvocatoriaController@index')->name('historicoConvocatorias.index')
        ->middleware('permission:convocatorias.index');
    // Ver 1 convocatoria
    Route::get('historicoConvocatorias/{convocatoria_id}/visualizar/convocatorias_vigente', 'HistoricoConvocatoriaController@indexAdmin')->name('historicoConvocatorias.detail')
        ->middleware('permission:visualizar.indexAdmin');

    //Usuarios
    Route::post('users/store', 'UserController@store')->name('users.store')
        ->middleware('permission:users.create');

    Route::get('users', 'UserController@index')->name('users.index')
        ->middleware('permission:users.index');

    Route::get('users/create', 'UserController@create')->name('users.create')
        ->middleware('permission:users.create');

    Route::put('users/{user}', 'UserController@update')->name('users.update')
        ->middleware('permission:users.edit');

    Route::get('users/{user}', 'UserController@show')->name('users.show')
        ->middleware('permission:users.show');

    Route::delete('users/{user}', 'UserController@destroy')->name('users.destroy')
        ->middleware('permission:users.destroy');

    Route::get('users/{user}/edit', 'UserController@edit')->name('users.edit')
        ->middleware('permission:users.edit');


    //Notas conocimiento
    Route::post('notasConocimiento/store', 'NotaConocimientoController@store')->name('notasConocimiento.store')
        ->middleware('permission:conocimiento.ingresar');

    Route::get('notasConocimiento/index/{convocatoria_id}', 'NotaConocimientoController@index')->name('notasConocimiento.index')
        ->middleware('permission:conocimiento.index');

    Route::get('notasConocimiento/publicar/{convocatoria_id}', 'NotaConocimientoController@publicar')->name('notasConocimiento.publicar')
        ->middleware('permission:conocimiento.ingresar');

    Route::get('notasConocimiento', 'NotaConocimientoController@preindex')->name('notasConocimiento.preindex')
        ->middleware('permission:conocimiento.index');

    Route::get('notasConocimiento/create', 'NotaConocimientoController@create')->name('notasConocimiento.create')
        ->middleware('permission:conocimiento.ingresar');

    Route::put('notasConocimiento/{convocatoria_id}/{postulante_id}', 'NotaConocimientoController@update')->name('notasConocimiento.update')
        ->middleware('permission:conocimiento.ingresar');

    Route::get('notasConocimiento/{convocatoria_id}/{postulante_id}/{notaConocimiento_id}', 'NotaConocimientoController@calificar')->name('notasConocimiento.calificar')
        ->middleware('permission:conocimiento.ingresar');

    Route::put('notasConocimiento/{convocatoria_id}/{postulante_id}/{notaConocimiento_id}', 'NotaConocimientoController@calificarupdate')->name('notasConocimiento.calificarupdate')
        ->middleware('permission:conocimiento.ingresar');

    // Route::put('notasConocimiento/{notaConocimiento}', 'NotaConocimientoController@addNewNote')->name('notasConocimiento.addNewNote')
    // ->middleware('permission:conocimiento.add');

    Route::get('notasConocimiento/{notaConocimiento}', 'NotaConocimientoController@show')->name('notasConocimiento.show')
        ->middleware('permission:conocimiento.show');

    Route::delete('notasConocimiento/{notaConocimiento}', 'NotaConocimientoController@destroy')->name('notasConocimiento.destroy')
        ->middleware('permission:conocimiento.destroy');

    Route::get('notasConocimiento/{notaConocimiento}/edit', 'NotaConocimientoController@edit')->name('notasConocimiento.edit')
        ->middleware('permission:conocimiento.edit');

    Route::get('notasConocimiento/{subunidad_id}/{postulante_id}', 'NotaConocimientoController@ingresar')->name('notasConocimiento.ingresar')
        ->middleware('permission:conocimiento.ingresar');

    Route::get('documents/show/{sis}', function ($sis) {
        $documents = Documento::all();
        return view('documentos.show', ['documents' => $documents, 'sis' => $sis]);
    });

    //Items
    //Navegar
    Route::get('departamentos/{id_departamento}/subUnidades/{subunidad_id}/items', 'ItemController@index')->name('items.index')
        ->middleware('permission:items.index');
    //Crear
    Route::get('departamentos/{id_departamento}/subUnidades/{subunidad_id}/items/create', 'ItemController@create')->name('items.create')
        ->middleware('permission:items.create');
    //Guardar
    Route::post('departamentos/{id_departamento}/subUnidades/{subunidad_id}/items', 'ItemController@store')->name('items.store')
        ->middleware('permission:items.create');
    //Editar
    Route::get('departamentos/{id_departamento}/subUnidades/{subunidad_id}/items/{item_id}', 'ItemController@edit')->name('items.edit')
        ->middleware('permission:items.edit');
    //Guardar cambios
    Route::put('departamentos/{id_departamento}/subUnidades/{subunidad_id}/items/{item_id}', 'ItemController@update')->name('items.update')
        ->middleware('permission:items.edit');
    //Eliminar
    Route::delete('departamentos/{id_departamento}/subUnidades/{subunidad_id}/items/{item_id}', 'ItemController@destroy')->name('items.destroy')
        ->middleware('permission:items.destroy');

    //Notas meritos
    Route::post('notasMerito/store', 'NotaMeritoController@store')->name('notasMerito.store')
        ->middleware('permission:merito.create');

    Route::get('notasMerito', 'NotaMeritoController@index')->name('notasMerito.index')
        ->middleware('permission:merito.index');

    Route::get('notasMerito/postindex/{convocatoria_id}', 'NotaMeritoController@postindex')->name('notasMerito.postindex')
        ->middleware('permission:merito.index');

    Route::get('notasMerito/meritos/{convocatoria_id}', 'NotaMeritoController@publicar')->name('notasMerito.publicar')
        ->middleware('permission:merito.edit');

    Route::get('notasMerito/{notaMerito}/create', 'NotaMeritoController@create')->name('notasMerito.create')
        ->middleware('permission:merito.create');

    Route::put('notasMerito/{convocatoria_id}/{notaMerito}', 'NotaMeritoController@update')->name('notasMerito.update')
        ->middleware('permission:merito.edit');

    Route::get('notasMerito/{notaMerito}', 'NotaMeritoController@show')->name('notasMerito.show')
        ->middleware('permission:merito.show');

    Route::delete('notasMerito/{notaMerito}', 'NotaMeritoController@destroy')->name('notasMerito.destroy')
        ->middleware('permission:merito.destroy');

    Route::get('notasMerito/{convocatoria_id}/{postulante_id}/edit', 'NotaMeritoController@edit')->name('notasMerito.edit')
        ->middleware('permission:merito.edit');
});

// notas
Route::get('notas/conocimientos', 'NotaController@indexConocimiento')->name('notasConocimiento.show');
Route::get('notas/meritos', 'NotaController@indexMeritos')->name('notasMeritos.show');
Route::get('notas/finales', 'NotaController@indexNotaFinal')->name('notasFinal.show');
Route::get('notas/meritos/{notaMerito}/edit', 'NotaController@editMerito')->name('notaMerito.edit');
Route::put('notas/merito/{notaMerito}', 'NotaController@updateMerito')->name('notaMerito.update');
Route::get('notas/conocimiento/{notaConocimiento}/ingresar', 'NotaController@ingresar')->name('conocimiento.ingresar');
Route::post('notas/conocimiento/{notaConocimiento}', 'NotaController@storeConocimiento')->name('conocimiento.store');
Route::put('notas/conocimiento/{notaConocimiento}', 'NotaController@updateConocimiento')->name('conocimiento.update');
Route::get('notas/conocimientos/{notaConocimiento}/edit', 'NotaController@editConocimiento')->name('notaMerito.edit');


Route::get('notas/{subunidad_id}/{requerimiento_id}/create', 'NotaController@create')->name('nota.create');
Route::post('notas/{subunidad_id}/{requerimiento_id}', 'NotaController@store')->name('nota.store');
Route::get('notas/{subunidad_id}/{requerimiento_id}', 'NotaController@index')->name('nota.index')
    ->middleware('permission:nota.index');

Route::get('notas/{subunidad_id}/{requerimiento_id}/{nota_id}/edit', 'NotaController@edit')->name('nota.edit');
Route::put('notas/{subunidad_id}/{requerimiento_id}/{nota_id}', 'NotaController@update')->name('nota.update');
Route::delete('notas/{subunidad_id}/{requerimiento_id}/{nota_id}', 'NotaController@destroy')->name('nota.delete');


//Resultados finales
Route::get('notasfinales/resultados', 'NotaController@finales')->name('finales.index')
    ->middleware('permission:finales.index');

Route::get('notasfinales', 'NotaController@filtraje')->name('finales.filtraje')
    ->middleware('permission:finales.index');

Route::get('notasfinales/publicar', 'NotaController@publics')->name('finales.publics')
    ->middleware('permission:finales.edit');
