<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meritos extends Model
{
    protected $connection = 'mysql';

    public $timestamps = false;
}
