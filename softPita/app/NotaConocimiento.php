<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotaConocimiento extends Model
{
    protected $connection = 'mysql';

    public function postulante(){

        return $this->belongsTo(Postulant::class);
    }

    public function convocatoria(){

        return $this->belongsTo(Convocatoria::class);
    }
    public function requerimiento(){

        return $this->belongsTo(Requerimiento::class);
    }
}