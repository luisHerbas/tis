<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Docente extends Model
{
    protected $connection = 'websis';

    public $timestamps = false;
    
    public function informacionPersonal(){

        return $this->belongsTo(InformacionPersonal::class);
    }}
