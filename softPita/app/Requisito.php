<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Requisito extends Model
{
    protected $connection = 'mysql';

    public $timestamps = false;

    protected $fillable = [

        'convocatoria_id', 'requisito'
    ];

    public function convocatoria(){

        return $this->belongsTo(Convocatoria::class);
    }

    public function subunidad(){

        return $this->belongsTo(Subunidad::class);
    }
}
