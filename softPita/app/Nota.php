<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nota extends Model
{
    protected $connection = 'mysql';
    public $timestamps = false;
    protected $fillable = [

        'nota', 'id_postulante', 'id_tipo_nota'
    ];
    public function tipoNota(){

        return $this->belongsTo(TipoNota::class);
    }

    public function notasConocimiento(){

        return $this->hasMany(Conocimiento::class);
    }
}
