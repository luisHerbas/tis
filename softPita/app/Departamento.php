<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{

    protected $connection = 'mysql';
    public $timestamps = false;
    protected $fillable = ['name'];

    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }
}
