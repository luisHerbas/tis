<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InformacionPersonal extends Model
{
    protected $connection = 'websis';

    public $timestamps = false;

    public function estudiantes(){

        return $this->hasMany(Estudiante::class);
    }

    public function docentes(){

        return $this->hasMany(Docente::class);
    }
}
