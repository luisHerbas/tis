<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Requerimiento extends Model
{
    protected $connection = 'mysql';

    public $timestamps = false;
    protected $fillable = [

        'convocatoria_id', 'cantidad_auxiliares', 'horas_academicas', 'destino'
    ];

    public function convocatoria(){

        return $this->belongsTo(Convocatoria::class);
    }
    public static function requerimientos($id){
        return Requerimiento::where('convocatoria_id', '=', $id)->get();
    }
    public function notaConocimiento(){

        return $this->hasMany(NotaConocimiento::class);
    }
}
