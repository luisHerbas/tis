<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoNota extends Model
{
    protected $connection = 'mysql';

    public $timestamps = false;

    public function notas(){

        return $this->hasMany(Nota::class);
    }
}
