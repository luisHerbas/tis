<?php

namespace App\Http\Controllers;

use App\Convocatoria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HistoricoConvocatoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos['convocatorias'] = Convocatoria::orderBy('fecha_publicacion', 'DESC')->get();


        return view('historicoConvocatorias.index', $datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('historicoConvocatorias.create');
    }

    public function indexAdmin($convocatoria_id)
    {
        $titulos = DB::table('convocatorias')->find($convocatoria_id);
        $requerimientos = DB::table('requerimientos')->where('convocatoria_id', $titulos->id)->get();
        $requisitos = DB::table('requisitos')->where('convocatoria_id', $titulos->id)->get();
        $fechas = DB::table('fecha_pruebas')->where('convocatoria_id', $titulos->id)->get();
        $documentos = DB::table('documentos')->where('convocatoria_id', $titulos->id)->get();
        $meritos = DB::table('meritos')->where('convocatoria_id', $convocatoria_id)->get();
        $meritos1 = DB::table('meritos1s')->where('convocatoria_id', $convocatoria_id)->get();
        $mertos = DB::table('mertos')->where('convocatoria_id', $convocatoria_id)->get();
        return View("historicoConvocatorias.detail", ['titulos' => $titulos, 'requerimientos' => $requerimientos, 'requisitos' => $requisitos, 'fechas' => $fechas, 'documentos' => $documentos, 'meritos' => $meritos, 'meritos1' => $meritos1, 'mertos' => $mertos]);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Convocatoria  $convocatoria
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
    }
}
