<?php

namespace App\Http\Controllers;

use App\ItemDep;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ItemDepController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($departamento_id)
    {
        $itemsDep = DB::table('item_deps')->where('departamento_id', $departamento_id)->orderBy('nivel', 'ASC')->get();

        return View("itemsDep/index", ['itemsDep' => $itemsDep, 'departamento_id' => $departamento_id]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($departamento_id)
    {
        return View("itemsDep/create", ['departamento_id' => $departamento_id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $departamento_id)
    {
        $campos = [
            'nombre' => 'required|string|max:255',
            'nivel' => 'required',
            'electiva' => 'required',
        ];
        $Mensaje = ["required" => 'El :attribute es requerido'];

        $this->validate($request, $campos, $Mensaje);

        $nombre = $request->input("nombre");
        $nivel = $request->input("nivel");
        $electiva = $request->input("electiva");
        $itemDep_nuevo = new ItemDep();
        $itemDep_nuevo->departamento_id = $departamento_id;
        $itemDep_nuevo->nombre = $nombre;
        $itemDep_nuevo->nivel = $nivel;
        $itemDep_nuevo->electiva = $electiva;
        $itemDep_nuevo->save();
        return redirect(route("itemsDep.index", ['departamento_id' => $departamento_id]))->with('Mensaje', 'Item creado con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Requisito  $requisito
     * @return \Illuminate\Http\Response
     */
    public function show(ItemDep $itemDep)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Requisito  $requisito
     * @return \Illuminate\Http\Response
     */
    public function edit($departamento_id, $itemDep_id)
    {
        $item = ItemDep::findOrFail($itemDep_id);

        return View("itemsDep/edit", ['item' => $item, 'departamento_id' => $departamento_id, 'itemDep_id' => $itemDep_id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Requisito  $requisito
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $departamento_id, $itemDep_id)
    {
        $campos = [
            'nombre' => 'required|string|max:255',
            'nivel' => 'required',
            'electiva' => 'required',
        ];
        $Mensaje = ["required" => 'El :attribute es requerido'];

        $this->validate($request, $campos, $Mensaje);

        $nombre = $request->input("nombre");
        $nivel = $request->input("nivel");
        $electiva = $request->input("electiva");
        $item_nuevo = ItemDep::findOrFail($itemDep_id);
        $item_nuevo->departamento_id = $departamento_id;
        $item_nuevo->nombre = $nombre;
        $item_nuevo->nivel = $nivel;
        $item_nuevo->electiva = $electiva;
        $item_nuevo->save();
        return redirect(route("itemsDep.index", ['departamento_id' => $departamento_id]))->with('Mensaje', 'Item creado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Requisito  $requisito
     * @return \Illuminate\Http\Response
     */
    public function destroy($departamento_id, $itemDep_id)
    {
        ItemDep::destroy($itemDep_id);
        return redirect(route("itemsDep.index", ['departamento_id' => $departamento_id]))->with('Mensaje', 'Merito eliminado con exito');
    }
}
