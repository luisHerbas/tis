<?php

namespace App\Http\Controllers;

use App\Meritos;
use App\Mertos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MertosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($convocatoria_id)
    {
        $mertos = DB::table('mertos')->where('convocatoria_id', $convocatoria_id)->get();

        return View("mertos/index", ['mertos' => $mertos, 'convocatoria_id' => $convocatoria_id]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($convocatoria_id)
    {
        return View("mertos/create", ['convocatoria_id' => $convocatoria_id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $convocatoria_id)
    {
        $campos = [
            'nombre' => 'required|string|max:255',
            'porcentaje' => 'required|integer|min:1|max:100',
        ];
        $Mensaje = ["required" => 'El :attribute es requerido'];

        $this->validate($request, $campos, $Mensaje);

        $nombre = $request->input("nombre");
        $porcentaje = $request->input("porcentaje");
        $merito_nuevo = new Mertos();
        $merito_nuevo->convocatoria_id = $convocatoria_id;
        $merito_nuevo->nombre = $nombre;
        $merito_nuevo->porcentaje = $porcentaje;
        $merito_nuevo->save();
        return redirect(route("detalle.index", ['convocatoria_id' => $convocatoria_id]))->with('Mensaje', 'Merito creado con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Requisito  $requisito
     * @return \Illuminate\Http\Response
     */
    public function show(Mertos $mertos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Requisito  $requisito
     * @return \Illuminate\Http\Response
     */
    public function edit($convocatoria_id, $merto_id)
    {
        $merto = Mertos::findOrFail($merto_id);

        return View("mertos/edit", ['merto' => $merto, 'convocatoria_id' => $convocatoria_id, 'merto_id' => $merto_id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Requisito  $requisito
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $convocatoria_id, $merto_id)
    {
        $campos = [
            'nombre' => 'required|string|max:255',
            'porcentaje' => 'required|integer|min:1|max:100',
        ];
        $Mensaje = ["required" => 'El :attribute es requerido'];

        $this->validate($request, $campos, $Mensaje);

        $nombre = $request->input("nombre");
        $porcentaje = $request->input("porcentaje");
        $merito_nuevo = Mertos::findOrFail($merto_id);
        $merito_nuevo->convocatoria_id = $convocatoria_id;
        $merito_nuevo->nombre = $nombre;
        $merito_nuevo->porcentaje = $porcentaje;
        $merito_nuevo->save();
        return redirect(route("detalle.index", ['convocatoria_id' => $convocatoria_id]))->with('Mensaje', 'Merito modificado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Requisito  $requisito
     * @return \Illuminate\Http\Response
     */
    public function destroy($convocatoria_id, $merto_id)
    {
        Mertos::destroy($merto_id);
        return redirect(route("detalle.index", ['convocatoria_id' => $convocatoria_id]))->with('Mensaje', 'Merito eliminado con exito');
    }
}
