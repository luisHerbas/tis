<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id_departamento, $subunidad_id)
    {

        $items = DB::table('items')->where('subunidad_id', $subunidad_id)->orderBy('nivel', 'ASC')->get();

        return View("items/index", ['items' => $items, 'id_departamento' => $id_departamento, 'subunidad_id' => $subunidad_id]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id_departamento, $subunidad_id)
    {
        return View("items/create", ['id_departamento' => $id_departamento, 'subunidad_id' => $subunidad_id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id_departamento, $subunidad_id)
    {
        $campos = [
            'nombre' => 'required|string|max:255',
            'nivel' => 'required',
            'electiva' => 'required',
        ];
        $Mensaje = ["required" => 'El :attribute es requerido'];

        $this->validate($request, $campos, $Mensaje);

        $nombre = $request->input("nombre");
        $nivel = $request->input("nivel");
        $electiva = $request->input("electiva");
        $item_nuevo = new Item();
        $item_nuevo->subunidad_id = $subunidad_id;
        $item_nuevo->nombre = $nombre;
        $item_nuevo->nivel = $nivel;
        $item_nuevo->electiva = $electiva;
        $item_nuevo->save();
        return redirect(route("items.index", ['id_departamento' => $id_departamento, 'subunidad_id' => $subunidad_id]))->with('Mensaje', 'Item creado con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Requisito  $requisito
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Requisito  $requisito
     * @return \Illuminate\Http\Response
     */
    public function edit($id_departamento, $subunidad_id, $item_id)
    {
        $item = Item::findOrFail($item_id);

        return View("items/edit", ['id_departamento' => $id_departamento, 'item' => $item, 'subunidad_id' => $subunidad_id, 'item_id' => $item_id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Requisito  $requisito
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_departamento, $subunidad_id, $item_id)
    {
        $campos = [
            'nombre' => 'required|string|max:255',
            'nivel' => 'required',
            'electiva' => 'required',
        ];
        $Mensaje = ["required" => 'El :attribute es requerido'];

        $this->validate($request, $campos, $Mensaje);

        $nombre = $request->input("nombre");
        $nivel = $request->input("nivel");
        $electiva = $request->input("electiva");
        $item_nuevo = Item::findOrFail($item_id);
        $item_nuevo->subunidad_id = $subunidad_id;
        $item_nuevo->nombre = $nombre;
        $item_nuevo->nivel = $nivel;
        $item_nuevo->electiva = $electiva;
        $item_nuevo->save();
        return redirect(route("items.index", ['id_departamento' => $id_departamento, 'subunidad_id' => $subunidad_id]))->with('Mensaje', 'Item creado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Requisito  $requisito
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_departamento, $subunidad_id, $item_id)
    {
        Item::destroy($item_id);
        return redirect(route("items.index", ['id_departamento' => $id_departamento, 'subunidad_id' => $subunidad_id]))->with('Mensaje', 'Merito eliminado con exito');
    }
}
