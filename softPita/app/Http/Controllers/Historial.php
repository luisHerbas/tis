<?php

namespace App\Http\Controllers;

use App\Convocatoria;
use App\Subunidad;
use App\Departamento;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class Historial extends Controller
{
    public function index()
    {
        $departamentos = Departamento::pluck('name', 'id')->all();
        return View("historicos/index", ['departamentos' => $departamentos]);
    }
    public function getSubunidades(Request $request, $id)
    {
        if ($request->ajax()) {
            $subunidades = Subunidad::subunidades($id);
            return response()->json($subunidades);
        }
    }
    public function getConvocatorias(Request $request, $id)
    {
        if ($request->ajax()) {
            $convocatorias = Convocatoria::convocatoriasNew($id);
            return response()->json($convocatorias);
        }
    }

    public function indexconvocatoria($convocatoria_id)
    {
        $titulos = DB::table('convocatorias')->find($convocatoria_id);
        $requerimientos = DB::table('requerimientos')->where('convocatoria_id', $titulos->id)->get();
        $requisitos = DB::table('requisitos')->where('convocatoria_id', $titulos->id)->get();
        $fechas = DB::table('fecha_pruebas')->where('convocatoria_id', $titulos->id)->get();
        $documentos = DB::table('documentos')->where('convocatoria_id', $titulos->id)->get();
        $meritos = DB::table('meritos')->where('convocatoria_id', $convocatoria_id)->get();
        $meritos1 = DB::table('meritos1s')->where('convocatoria_id', $convocatoria_id)->get();
        $mertos = DB::table('mertos')->where('convocatoria_id', $convocatoria_id)->get();

        $postulantes = DB::table('requerimientos')->where('requerimientos.convocatoria_id', $convocatoria_id)
            ->join('postulants', 'postulants.requerimiento_id', 'requerimientos.id')
            ->join('nota_conocimientos', 'nota_conocimientos.postulante_id', 'postulants.id')
            ->join('nota_meritos', 'nota_meritos.postulante_id', 'postulants.id')
            ->join('notas', 'notas.id', 'nota_conocimientos.nota_id')
            ->select(
                'postulants.name',
                'postulants.last_name',
                'requerimientos.destino',
                'postulants.nota_conocimientos',
                'nota_meritos.nota',
                'requerimientos.destino',
                'notas.nombre_nota',
                'nota_conocimientos.nota as notaC',
                'postulants.nota_final'
            )
            ->where('postulants.id_status', 2)
            ->orderby('notas.nombre_nota', 'ASC')
            ->orderby('postulants.last_name', 'ASC')
            ->get();


            $inhabilitados = DB::table('requerimientos')->where('requerimientos.convocatoria_id', $convocatoria_id)
            ->join('postulants', 'postulants.requerimiento_id', 'requerimientos.id')
            ->join('nota_meritos', 'nota_meritos.postulante_id', 'postulants.id')
            ->join('convocatorias', 'convocatorias.id', 'requerimientos.convocatoria_id')
            ->select('postulants.name', 'postulants.last_name', 'postulants.cod_sis', 'requerimientos.destino', 'postulants.nota_conocimientos', 'nota_meritos.nota', 'postulants.nota_final', 'postulants.observaciones', 'convocatorias.publicacion_finales')
            ->where('postulants.id_status', 3)
            ->get();

        $departamentos = DB::table('convocatorias')
            ->where('convocatorias.id', $convocatoria_id)
            ->join('subunidades', 'subunidades.id', 'convocatorias.subunidad_id')
            ->join('departamentos', 'departamentos.id', 'subunidades.id_departamento')
            ->select('departamentos.name', 'departamentos.id', 'subunidades.name')
            ->first();

        $notas_meritos = DB::table('requerimientos')->where('requerimientos.convocatoria_id', $convocatoria_id)
            ->join('postulants', 'postulants.requerimiento_id', 'requerimientos.id')
            ->join('nota_meritos', 'nota_meritos.postulante_id', 'postulants.id')
            ->select('postulants.name', 'postulants.last_name', 'requerimientos.destino', 'postulants.nota_conocimientos', 'nota_meritos.nota', 'nota_meritos.nota')
            ->where('postulants.id_status', 2)
            ->orderby('postulants.last_name', 'ASC', 'requerimientos.destino')
            ->get();

        $finales = DB::table('requerimientos')->where('requerimientos.convocatoria_id', $convocatoria_id)
            ->join('postulants', 'postulants.requerimiento_id', 'requerimientos.id')
            ->join('nota_meritos', 'nota_meritos.postulante_id', 'postulants.id')
            ->select(
                'postulants.name',
                'postulants.last_name',
                'requerimientos.destino',
                'postulants.nota_conocimientos',
                'postulants.nota_final',
                'nota_meritos.nota'
            )
            ->where('postulants.id_status', 2)
            ->orderby('postulants.last_name', 'ASC', 'requerimientos.destino')
            ->get();

        return View("historicos.indexconvocatoria", compact('departamentos', 'titulos', 'requerimientos', 'requisitos', 'fechas', 'documentos', 'meritos', 'meritos1', 'mertos', 'postulantes', 'departamentos', 'notas_meritos', 'finales', 'inhabilitados'));
    }

    public function indexVisualizar($departamento_id, $convocatoria_id)
    {
        $titulos = DB::table('convocatorias')->find($convocatoria_id);
        $requerimientos = DB::table('requerimientos')->where('convocatoria_id', $titulos->id)->get();
        $requisitos = DB::table('requisitos')->where('convocatoria_id', $titulos->id)->get();
        $fechas = DB::table('fecha_pruebas')->where('convocatoria_id', $titulos->id)->get();
        $documentos = DB::table('documentos')->where('convocatoria_id', $titulos->id)->get();
        $meritos = DB::table('meritos')->where('convocatoria_id', $convocatoria_id)->get();
        $meritos1 = DB::table('meritos1s')->where('convocatoria_id', $convocatoria_id)->get();
        $mertos = DB::table('mertos')->where('convocatoria_id', $convocatoria_id)->get();

        $postulantes = DB::table('requerimientos')->where('requerimientos.convocatoria_id', $convocatoria_id)
            ->join('postulants', 'postulants.requerimiento_id', 'requerimientos.id')
            ->join('nota_conocimientos', 'nota_conocimientos.postulante_id', 'postulants.id')
            ->join('nota_meritos', 'nota_meritos.postulante_id', 'postulants.id')
            ->join('notas', 'notas.id', 'nota_conocimientos.nota_id')
            ->select('postulants.name', 'postulants.last_name', 'requerimientos.destino', 'postulants.nota_conocimientos', 'nota_meritos.nota', 'requerimientos.destino', 'notas.nombre_nota', 'nota_conocimientos.nota')
            ->orderby('notas.nombre_nota', 'ASC')
            ->orderby('postulants.last_name', 'ASC')
            ->get();

        $departamentos = DB::table('convocatorias')
            ->where('convocatorias.id', $convocatoria_id)
            ->join('subunidades', 'subunidades.id', 'convocatorias.subunidad_id')
            ->join('departamentos', 'departamentos.id', 'subunidades.id_departamento')
            ->select('departamentos.name', 'departamentos.id', 'subunidades.name')
            ->first();

        $notas_meritos = DB::table('requerimientos')->where('requerimientos.convocatoria_id', $convocatoria_id)
            ->join('postulants', 'postulants.requerimiento_id', 'requerimientos.id')
            ->join('nota_meritos', 'nota_meritos.postulante_id', 'postulants.id')
            ->select('postulants.name', 'postulants.last_name', 'requerimientos.destino', 'postulants.nota_conocimientos', 'nota_meritos.nota', 'nota_meritos.nota')
            ->orderby('postulants.last_name', 'ASC', 'requerimientos.destino')
            ->get();

        $finales = DB::table('requerimientos')->where('requerimientos.convocatoria_id', $convocatoria_id)
            ->join('postulants', 'postulants.requerimiento_id', 'requerimientos.id')
            ->join('nota_meritos', 'nota_meritos.postulante_id', 'postulants.id')
            ->select('postulants.name', 'postulants.last_name', 'requerimientos.destino', 'postulants.nota_conocimientos', 'postulants.nota_final', 'nota_meritos.nota', 'nota_meritos.nota')
            ->orderby('postulants.last_name', 'ASC', 'requerimientos.destino')
            ->get();

        return View("historicos.indexVisualizar", ['titulos' => $titulos, 'requerimientos' => $requerimientos, 'departamentos' => $departamentos, 'requisitos' => $requisitos, 'fechas' => $fechas, 'documentos' => $documentos, 'meritos' => $meritos, 'meritos1' => $meritos1, 'mertos' => $mertos, '$departamento_id' => $departamento_id, 'convocatoria_id' => $convocatoria_id, 'postulantes' => $postulantes, 'notas_meritos' => $notas_meritos, 'finales' => $finales]);
    }
}
