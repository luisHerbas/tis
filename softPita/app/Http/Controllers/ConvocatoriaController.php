<?php

namespace App\Http\Controllers;

use App\Convocatoria;
use App\Departamento;
use App\Docente;
use App\Postulant;
use App\Subunidad;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ConvocatoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $convocatorias = DB::table('subunidades')
        ->join('departamentos', 'departamentos.id', 'subunidades.id_departamento')
            ->join('convocatorias', 'subunidades.id', '=', 'convocatorias.subunidad_id')
            ->select('subunidades.name', 'convocatorias.id', 'convocatorias.titulo_convocatoria', 'convocatorias.fecha_publicacion', 'departamentos.name as dep', 'convocatorias.estado', 'convocatorias.gestion')
            ->orderBy('fecha_publicacion', 'DESC')
            ->get();

            //dd($convocatorias);
        return view('convocatorias.index', compact('convocatorias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departamentos = Departamento::pluck('name','id')->all();
        $docentes = Docente::get();
        //dd($docentes);
        return view('convocatorias.create', compact('departamentos', 'docentes'));
    }

    public function getSubunidades(Request $request, $id)
    {
        if($request->ajax()){
            $subunidades = Subunidad::subunidades($id);
            return response()->json($subunidades);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campos=[
            'titulo_convocatoria'=>'required|string|max:255',
            'fecha_publicacion' => 'required|date|after:yesterday',
            'subunidad' => 'required',
            'porcentaje_conocimiento' => 'required|numeric|min:1|max:100',
            'porcentaje_meritos' => 'required',
            'total' => 'numeric|min:100|max:100',
            'gestion' => 'required'
        ];
        $Mensaje=["required"=>'El :attribute es requerido'];
        
        $this->validate($request,$campos,$Mensaje);

        $fecha = Carbon::parse($request->fecha_publicacion);
        $mfecha = $fecha->month;
        $dfecha = $fecha->day;
        $afecha = $fecha->year;
        
        $titulo_convocatoria = $request->input("titulo_convocatoria");
        $fecha_publicacion = $request->input("fecha_publicacion");
        $subunidad_id = $request->input("subunidad");
        $comision_conocimiento = $request->input("comision_conocimiento");
        $comision_merito = $request->input("comision_merito");
        $porcentaje_conocimiento = $request->input("porcentaje_conocimiento");
        $porcentaje_meritos = $request->input("porcentaje_meritos");
        $gestion = $request->input("gestion");
        $convocatoria_nueva = new Convocatoria();
        $convocatoria_nueva->titulo_convocatoria = $titulo_convocatoria;
        $convocatoria_nueva->fecha_publicacion = $fecha_publicacion;
        $convocatoria_nueva->subunidad_id = $subunidad_id;
        $convocatoria_nueva->porcentaje_conocimiento = $porcentaje_conocimiento;
        $convocatoria_nueva->porcentaje_meritos = $porcentaje_meritos;
        $convocatoria_nueva->comision_conocimiento = $comision_conocimiento;
        $convocatoria_nueva->comision_merito = $comision_merito;
        if($gestion == "2 de la presente gestion y 1 de la siguiente gestion"){
            $convocatoria_nueva->gestion = '2-'.$afecha.' y 1-'.($afecha+1);

        }else{
            $convocatoria_nueva->gestion = $gestion.' - '.$afecha;
        }
        
        $convocatoria_nueva->save();
        
        //return response()->json($datosConvocatoria);
        return redirect('convocatorias')->with('Mensaje','Convocatoria creada con exito');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Convocatoria  $convocatoria
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Convocatoria  $convocatoria
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $convocatoria=Convocatoria::findOrFail($id);
        //$departamentos = DB::table('departamentos')->get();
        $departamentos = Departamento::pluck('name','id')->all();
        $docentes = Docente::get();

        return view('convocatorias.edit',compact('convocatoria', 'departamentos', 'docentes'));
    }
    public function getSubunidades1(Request $request, $convocatoria_id, $id)
    {
        if($request->ajax()){
            $subunidades = Subunidad::subunidades($id);
            return response()->json($subunidades);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Convocatoria  $convocatoria
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $campos=[
            'titulo_convocatoria'=>'required|string|max:255',
            'fecha_publicacion' => 'required|date|after:yesterday',
            'subunidad' => 'required',
            'porcentaje_conocimiento' => 'required',
            'porcentaje_meritos' => 'required',
            'gestion' => 'required',
        ];
        $Mensaje=["required"=>'El :attribute es requerido'];
        
        $this->validate($request,$campos,$Mensaje);

        $fecha = Carbon::parse($request->fecha_publicacion);
        $mfecha = $fecha->month;
        $dfecha = $fecha->day;
        $afecha = $fecha->year;

        $titulo_convocatoria = $request->input("titulo_convocatoria");
        $fecha_publicacion = $request->input("fecha_publicacion");
        $subunidad_id = $request->input("subunidad");
        $porcentaje_conocimiento = $request->input("porcentaje_conocimiento");
        $porcentaje_meritos = $request->input("porcentaje_meritos");
        $comision_conocimiento = $request->input("comision_conocimiento");
        $comision_merito = $request->input("comision_merito");
        $gestion = $request->input("gestion");
        $convocatoria_nueva = Convocatoria::findOrFail($id);
        $convocatoria_nueva->titulo_convocatoria = $titulo_convocatoria;
        $convocatoria_nueva->fecha_publicacion = $fecha_publicacion;
        $convocatoria_nueva->subunidad_id = $subunidad_id;
        $convocatoria_nueva->porcentaje_conocimiento = $porcentaje_conocimiento;
        $convocatoria_nueva->porcentaje_meritos = $porcentaje_meritos;
        $convocatoria_nueva->comision_conocimiento = $comision_conocimiento;
        $convocatoria_nueva->comision_merito = $comision_merito;
        if($gestion == "2 de la presente gestion y 1 de la siguiente gestion"){
            $convocatoria_nueva->gestion = '2-'.$afecha.' y 1-'.($afecha+1);

        }else{
            $convocatoria_nueva->gestion = $gestion.' - '.$afecha;
        }
        //dd($convocatoria_nueva);
        $convocatoria_nueva->save();

        $postulantes = DB::table('postulants')
        ->join('requerimientos', 'requerimientos.id', 'postulants.requerimiento_id')
        ->join('nota_meritos', 'nota_meritos.postulante_id', 'postulants.id')
        ->select('postulants.id', 'postulants.nota_conocimientos', 'postulants.nota_final', 'nota_meritos.nota')
        ->where('requerimientos.convocatoria_id', $convocatoria_nueva->id)
        ->get();

        foreach($postulantes as $postulante){
            
            $postulante_antiguo = DB::table('postulants')->where('postulants.id', $postulante->id)
            ->join('nota_meritos', 'nota_meritos.postulante_id', 'postulants.id')
            ->select('postulants.id', 'postulants.nota_conocimientos', 'postulants.nota_final', 'nota_meritos.nota')
            ->update([
                "nota_conocimientos" => $postulante->nota_conocimientosfv,
                "nota" => $postulante->nota]);

                //dd($postulante_antiguo, $postulante->nota_conocimientos, $convocatoria_nueva->porcentaje_conocimiento, $convocatoria_nueva->porcentaje_meritos);
        }

        foreach($postulantes as $postulante){

            $nota_nueva = DB::table('postulants')->where('postulants.id', $postulante->id)
            ->join('nota_meritos', 'nota_meritos.postulante_id', 'postulants.id')
            ->select('postulants.id', 'postulants.nota_conocimientos', 'postulants.nota_final', 'nota_meritos.nota')
            ->update([
                "nota_final" => $postulante->nota_conocimientos*$convocatoria_nueva->porcentaje_conocimiento/100+ $postulante->nota*$convocatoria_nueva->porcentaje_meritos/100]);
            
        }
        
        //dd($postulante_antiguo, $postulante->nota_conocimientos, $convocatoria_nueva->porcentaje_conocimiento, $convocatoria_nueva->porcentaje_meritos);

        return redirect('convocatorias')->with('Mensaje','Convocatoria modificada con exito');
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Convocatoria  $convocatoria
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Convocatoria::destroy($id);
        return redirect('convocatorias')->with('Mensaje','Convocatoria eliminada con exito');
    
    }

    public function inhabilitar($id)
    {
        $convocatoria = Convocatoria::find($id);
        if($convocatoria->estado == 'Habilitada'){
            $convocatoria->estado = 'Inhabilitada';
            $convocatoria->update();

        }else{
            $convocatoria->estado = 'Habilitada';
            $convocatoria->update();
        }
        
        //dd($convocatoria);
        return redirect('convocatorias');
    
    }

    public function publicar()
    {
        $convocatorias = DB::table('subunidades')
        ->join('departamentos', 'departamentos.id', 'subunidades.id_departamento')
            ->join('convocatorias', 'subunidades.id', '=', 'convocatorias.subunidad_id')
            ->select('subunidades.name', 'convocatorias.id', 'convocatorias.titulo_convocatoria', 'convocatorias.fecha_publicacion', 'departamentos.name as dep', 'convocatorias.estado')
            ->where('convocatorias.estado', 'Habilitada')
            ->orderBy('fecha_publicacion', 'DESC')
            ->get();
            //dd($convocatorias);
        return view('publicar', compact('convocatorias'));
    }

    public function conocimiento()
    {
        $postulantes = DB::table('requerimientos')
            ->join('convocatorias', 'requerimientos.convocatoria_id', 'convocatorias.id')
            ->join('postulants', 'postulants.requerimiento_id', 'requerimientos.id')
            ->select('requerimientos.destino', 'postulants.id', 'postulants.name', 'postulants.last_name', 'postulants.nota_conocimientos', 'convocatorias.titulo_convocatoria')
            ->where('convocatorias.publicacion_conocimientos', 2)
            ->where('postulants.id_status', '2')
            ->orderBy('convocatorias.titulo_convocatoria', 'ASC')
            ->get();
            //dd($postulantes);
        return view('publicar_conocimiento', compact('postulantes'));
    }   

    public function merito()
    {
        $postulantes = DB::table('requerimientos')
        ->join('convocatorias', 'requerimientos.convocatoria_id', 'convocatorias.id')
            ->join('postulants', 'postulants.requerimiento_id', 'requerimientos.id')
            ->join('nota_meritos', 'postulants.id', 'nota_meritos.postulante_id')
            ->select('requerimientos.destino', 'postulants.id', 'postulants.name', 'postulants.last_name', 'nota_meritos.nota', 'convocatorias.titulo_convocatoria')
            ->where('convocatorias.publicacion_meritos', 2)
            ->where('postulants.id_status', '2')
            ->orderBy('convocatorias.titulo_convocatoria', 'ASC')
            ->get();
            //dd($convocatorias);
        return view('publicar_merito', compact('postulantes'));
    }
}
