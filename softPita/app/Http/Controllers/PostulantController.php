<?php

namespace App\Http\Controllers;

use App\Conocimiento;
use App\Postulant;
use Illuminate\Http\Request;
use App\Convocatoria;
use App\Requerimiento;
use App\Subunidad;
use App\NotaConocimiento;
use App\Departamento;
use App\NotaMerito;
use App\Estudiante;
use App\NotaFinal;
use Illuminate\Support\Facades\DB;

class PostulantController extends Controller
{
    /**
     * Display a listing of the resource.
     * Listar todos los postulantes sin importar su estado, es decir, que estén o no habilitados
     *
     * @return \Illuminate\Http\Response
     */
    public function index($convocatoria_id)
    {

        $postulantes = DB::table('requerimientos')->where('requerimientos.convocatoria_id', $convocatoria_id)
            ->join('postulants', 'postulants.requerimiento_id', 'requerimientos.id')
            ->join('convocatorias', 'requerimientos.convocatoria_id', 'convocatorias.id')
            ->select('postulants.name', 'postulants.last_name', 'postulants.requerimiento_id', 'postulants.id_status', 'postulants.id', 'postulants.cod_sis', 'postulants.nro_documents', 'postulants.nro_certificates', 'requerimientos.destino', 'convocatorias.titulo_convocatoria', 'convocatorias.fecha_publicacion')
            ->orderby('postulants.id', 'ASC')
            ->orderby('convocatorias.fecha_publicacion', 'DESC')
            ->get();

        $requerimientos = DB::table('requerimientos')->where('requerimientos.convocatoria_id', $convocatoria_id)
            ->join('postulants', 'postulants.requerimiento_id', 'requerimientos.id')
            ->select('requerimientos.destino', 'requerimientos.id')
            ->groupby('requerimientos.destino')
            ->get();


        //dd($postulantes);
        $departamentos = Departamento::pluck('name', 'id')->all();
        $pos = new Postulant;
        $cabeceras = $pos->getTableColumns();
        //$postulantes = Postulant::all();

        return view('postulant.lista', ['cabeceras' => $cabeceras, 'postulantes' => $postulantes, 'requerimientos' => $requerimientos, 'departamentos' => $departamentos, 'convocatoria_id' => $convocatoria_id]);
    }


    /**
     * Lista solo a los postulantes que ya han sido habilitados
     */
    public function index_habilitados($convocatoria_id)
    {
        $pos = new Postulant;
        $cabeceras = $pos->getTableColumns();
        $postulantes = DB::table('requerimientos')->where('requerimientos.convocatoria_id', $convocatoria_id)
            ->join('postulants', 'postulants.requerimiento_id', 'requerimientos.id')
            ->join('convocatorias', 'requerimientos.convocatoria_id', 'convocatorias.id')
            ->select('postulants.name', 'postulants.last_name', 'postulants.requerimiento_id', 'postulants.id_status', 'postulants.id', 'postulants.cod_sis', 'postulants.nro_documents', 'postulants.nro_certificates', 'requerimientos.destino', 'convocatorias.titulo_convocatoria', 'convocatorias.fecha_publicacion')
            ->orderby('postulants.id', 'ASC')
            ->orderby('convocatorias.fecha_publicacion', 'DESC')
            ->where('postulants.id_status', 2)
            ->get();
        return view('postulant.habilitados', ['cabeceras' => $cabeceras, 'postulantes' => $postulantes, 'convocatoria_id' => $convocatoria_id]);
    }

    /**
     * Lista solo a los postulantes que han sido inhabilitados
     */
    public function index_inhabilitados($convocatoria_id)
    {
        $pos = new Postulant;
        $cabeceras = $pos->getTableColumns();
        $postulantes = DB::table('requerimientos')->where('requerimientos.convocatoria_id', $convocatoria_id)
            ->join('postulants', 'postulants.requerimiento_id', 'requerimientos.id')
            ->join('convocatorias', 'requerimientos.convocatoria_id', 'convocatorias.id')
            ->select('postulants.name', 'postulants.last_name', 'postulants.requerimiento_id', 'postulants.id_status', 'postulants.id', 'postulants.cod_sis', 'postulants.nro_documents', 'postulants.nro_certificates', 'requerimientos.destino', 'convocatorias.titulo_convocatoria', 'convocatorias.fecha_publicacion')
            ->orderby('postulants.id', 'ASC')
            ->orderby('convocatorias.fecha_publicacion', 'DESC')
            ->where('postulants.id_status', 3)
            ->get();
        return view('postulant.inhabilitados', ['cabeceras' => $cabeceras, 'postulantes' => $postulantes, 'convocatoria_id' => $convocatoria_id]);
    }

    /**
     * Show the form for creating a new resource.
     * Redirecciona a la vista de formulario de registro
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departamentos = Departamento::pluck('name', 'id')->all();
        $convocatoria = Convocatoria::orderBy('fecha_publicacion', 'DESC')->first();
        $requerimientos = Requerimiento::where('convocatoria_id', $convocatoria['id'])->get();
        return view('postulant.create', ['convocatoria' => $convocatoria, 'requerimientos' => $requerimientos, 'departamentos' => $departamentos]);
    }

    public function getSubunidades(Request $request, $id)
    {
        if ($request->ajax()) {
            $subunidades = Subunidad::subunidades($id);
            return response()->json($subunidades);
        }
    }

    public function getConvocatorias(Request $request, $id)
    {
        if ($request->ajax()) {
            $convocatorias = Convocatoria::convocatoriasP($id);
            return response()->json($convocatorias);
        }
    }

    /**
     * Mandamos los datos ingresados a la vista rótulo para su posterior descarga
     */
    public function create_rotulo($cod_sis)
    {
        $postulant = DB::table('postulants')
            ->where('postulants.cod_sis', $cod_sis)
            ->join('requerimientos', 'requerimientos.id', 'postulants.requerimiento_id')
            ->join('convocatorias', 'requerimientos.convocatoria_id', 'convocatorias.id')
            ->join('subunidades', 'subunidades.id', 'convocatorias.subunidad_id')
            ->join('departamentos', 'departamentos.id', 'subunidades.id_departamento')
            ->select('postulants.id','postulants.name', 'postulants.last_name', 'postulants.cod_sis', 'postulants.phone', 'requerimientos.destino', 'departamentos.name as departamentoname', 'subunidades.name as subunidadname', 'postulants.email')
            ->orderBy('postulants.id', 'desc')
            ->first();

        return view('postulant.rotulo', ['postulant' => $postulant]);
    }

    public function create_inhabilitar($sis, $convocatoria_id)
    {

        return view('postulant.inhabilitar', ['sis' => $sis, 'convocatoria_id' => $convocatoria_id]);
    }

    /**
     * Store a newly created resource in storage.
     * Almacena todos los datos del postulante
     * Crea un campo en nota conocimiento y en nota mérito por cada postulante habilitado 
     * Cada nota tiene un valor incial 0
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campos = [
            'cod_sis' => 'required',
            'address' => 'required|string|max:55',
            'ci' => 'required',
            'nro_documents' => 'required',
            'nro_certificates' => 'required',
            'requerimiento' => 'required',
        ];
        $Mensaje = ["required" => 'El :attribute es requerido'];

        $this->validate($request, $campos, $Mensaje);

        $cod_sis = $request->input("cod_sis");
        $address = $request->input("address");
        $phone = $request->input("phone");
        $email = $request->input("email");
        $ci = $request->input("ci");
        $nro_documents = $request->input("nro_documents");
        $nro_certificates = $request->input("nro_certificates");
        $requerimiento_id = $request->input("requerimiento");

        if ($this->verificar_estudiante($request)) {
            $estudiante = $this->verificar_estudiante($request);
            $postulante_nuevo = new Postulant();
            $postulante_nuevo->cod_sis = $cod_sis;
            $postulante_nuevo->name = $estudiante->nombres;
            $postulante_nuevo->last_name = $estudiante->apellidos;
            $postulante_nuevo->address = $address;
            $postulante_nuevo->phone = $phone;
            $postulante_nuevo->email = $email;
            $postulante_nuevo->nro_documents = $nro_documents;
            $postulante_nuevo->nro_certificates = $nro_certificates;
            $postulante_nuevo->requerimiento_id = $requerimiento_id;
            $postulante_nuevo->id_status = 1;
            $postulante_nuevo->observaciones = "Ninguna";
            $path = 'postulant/rotulo/' . $postulante_nuevo->cod_sis;
            $postulante_nuevo->save();

            $new_notaMerito = new NotaMerito();
            $new_notaMerito->postulante_id = $postulante_nuevo->id;
            $new_notaMerito->nota = "0";
            $new_notaMerito->save();

            $notas = DB::table('notas')->where('notas.requerimiento_id', $postulante_nuevo->requerimiento_id)
                ->select('notas.id')
                ->get();

            foreach ($notas as $nota) {
                $new_notaConocimiento = new NotaConocimiento();
                $new_notaConocimiento->postulante_id = $postulante_nuevo->id;
                $new_notaConocimiento->nota_id = $nota->id;
                $new_notaConocimiento->save();
            }

            return redirect($path)->with('cod_sis', $postulante_nuevo['cod_sis']);
        } else {
            return view('miembro', ['name' => null]);
        }
    }

    /**
     * Valida si el postulante es miembro de la Universidad
     * Se valida según CI y CodSis
    */
    public function verificar_estudiante(Request $request)
    {
        $codSis = $request->get('cod_sis');
        $ci = $request->get('ci');
        $estudiante = DB::connection('websis')->table('estudiantes')->where('codsis', $codSis)->where('ci', $ci)
            ->join('informacion_personals', 'estudiantes.id_informacion', 'informacion_personals.id')
            ->first();

        $estudiantes = Estudiante::where('codsis', $codSis)->where('ci', $ci)
            ->first();

        return $estudiante;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Postulant  $postulant
     * @return \Illuminate\Http\Response
     */
    public function show(Postulant $postulant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Postulant  $postulant
     * @return \Illuminate\Http\Response
     */
    public function edit(Postulant $postulant)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Postulant  $postulant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Postulant $postulant)
    {
        //
    }

    public function habilitar(Request $request, $convocatoria_id)
    {
        $id = $request->input("sis");
        $postulante = Postulant::where('id', $id)->first();
        $postulante->id_status = 2;
        $postulante->observaciones = "Ninguna";
        $postulante->save();
        return redirect(route("listaspostulantes.index", ['convocatoria_id' => $convocatoria_id]));
    }

    public function inhabilitar(Request $request, $convocatoria_id)
    {
        $codigo = $request->input("sis");
        $observaciones = $request->input("observaciones");
        $postulante = Postulant::where('id', $codigo)->first();
        $postulante->id_status = 3;
        $postulante->observaciones = $observaciones;
        $postulante->save();
        return redirect(route("listaspostulantes.index", ['convocatoria_id' => $convocatoria_id]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Postulant  $postulant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Postulant $postulant)
    {
        //
    }

    public function filtraje()
    {
        $departamentos = DB::table('departamentos')->get();
        return view('filtrajePostulats', compact('departamentos'));
    }

}
