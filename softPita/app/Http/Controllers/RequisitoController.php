<?php

namespace App\Http\Controllers;

use App\Requisito;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RequisitoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($convocatoria_id)
    {
        $requisitos = DB::table('requisitos')->where('convocatoria_id', $convocatoria_id)->get();

        return View("requisitos/index", ['requisitos' => $requisitos, 'convocatoria_id' => $convocatoria_id]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($convocatoria_id)
    {
        return View("requisitos/create", ['convocatoria_id' => $convocatoria_id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $convocatoria_id)
    {
        $campos = [
            'requisito' => 'required|string|max:255|unique:requisitos,requisito,NULL,id,convocatoria_id,'.$convocatoria_id,
        ];
        $Mensaje = ["required" => 'El :attribute es requerido'];

        $this->validate($request, $campos, $Mensaje);

        $requisito = $request->input("requisito");
        $requisito_nuevo = new Requisito();
        $requisito_nuevo->convocatoria_id = $convocatoria_id;
        $requisito_nuevo->requisito = $requisito;
        $requisito_nuevo->save();
        return redirect(route("detalle.index", ['convocatoria_id' => $convocatoria_id]))->with('Mensaje', 'Requerimiento creado con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Requisito  $requisito
     * @return \Illuminate\Http\Response
     */
    public function show(Requisito $requisito)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Requisito  $requisito
     * @return \Illuminate\Http\Response
     */
    public function edit($convocatoria_id, $requisito_id)
    {
        $requisito = Requisito::findOrFail($requisito_id);

        return View("requisitos/edit", ['requisito' => $requisito, 'convocatoria_id' => $convocatoria_id, 'requisito_id' => $requisito_id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Requisito  $requisito
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $convocatoria_id, $requisito_id)
    {
        $campos = [
            'requisito' => 'required|string|max:255|unique:requisitos,requisito,NULL,id,convocatoria_id,'.$convocatoria_id,
        ];
        $Mensaje = ["required" => 'El :attribute es requerido'];

        $this->validate($request, $campos, $Mensaje);

        $requisito = $request->input("requisito");
        $requisito_nuevo = Requisito::findOrFail($requisito_id);
        $requisito_nuevo->convocatoria_id = $convocatoria_id;
        $requisito_nuevo->requisito = $requisito;
        $requisito_nuevo->save();
        return redirect(route("detalle.index", ['convocatoria_id' => $convocatoria_id]))->with('Mensaje', 'Requerimiento modificado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Requisito  $requisito
     * @return \Illuminate\Http\Response
     */
    public function destroy($convocatoria_id, $requisito_id)
    {
        Requisito::destroy($requisito_id);
        return redirect(route("detalle.index", ['convocatoria_id' => $convocatoria_id]))->with('Mensaje', 'Requerimiento eliminado con exito');
    }
}
