<?php

namespace App\Http\Controllers;

use App\Convocatoria;
use App\NotaMerito;
use App\Departamento;
use App\NotaFinal;
use App\Postulant;
use App\Subunidad;
use App\Requerimiento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NotaMeritoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departamentos = Departamento::get();
        $postulantes = DB::table('postulants')
            ->join('requerimientos', 'requerimientos.id', '=', 'postulants.requerimiento_id')
            ->join('nota_meritos', 'postulants.id', '=', 'nota_meritos.postulante_id')
            ->select('requerimientos.destino', 'postulants.id', 'postulants.name', 'postulants.last_name', 'nota_meritos.nota')
            ->where('id_status', '2')
            ->orderBy('requerimientos.destino', 'ASC')
            ->get();

        return view('notasMerito.index', compact('postulantes', 'departamentos'));
    }

    public function postindex($convocatoria_id)
    {
        $convocatoria = Convocatoria::find($convocatoria_id);

        $postulantes = DB::table('requerimientos')->where('requerimientos.convocatoria_id', $convocatoria_id)
            ->join('postulants', 'postulants.requerimiento_id', 'requerimientos.id')
            ->join('nota_meritos', 'postulants.id', 'nota_meritos.postulante_id')
            ->select('requerimientos.destino', 'postulants.id', 'postulants.name', 'postulants.last_name', 'nota_meritos.nota')
            ->where('id_status', '2')
            ->orderBy('requerimientos.destino', 'ASC')
            ->get();

        return view('notasMerito.postindex', compact('convocatoria', 'postulantes', 'convocatoria_id'));
    }

    public function getSubunidades(Request $request, $id)
    {
        if ($request->ajax()) {
            $subunidades = Subunidad::subunidades($id);
            return response()->json($subunidades);
        }
    }

    public function getConvocatorias(Request $request, $id)
    {
        if ($request->ajax()) {
            $convocatorias = Convocatoria::convocatorias($id);
            return response()->json($convocatorias);
        }
    }

    public function getPostulantes(Request $request, $id)
    {
        if ($request->ajax()) {
            $postulantes = Postulant::postulantes($id);
            return response()->json($postulantes);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($notaMerito)
    {
        $notasMerito = DB::table('nota_meritos')->find($notaMerito);
        $requerimientos = Requerimiento::paginate();
        $meritos = DB::table('meritos')->get();
        $meritos1 = DB::table('meritos1s')->get();
        $mertos = DB::table('mertos')->get();

        return view('notasMerito.create', compact('notasMerito', 'requerimientos', 'meritos', 'meritos1', 'mertos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $notaMerito = NotaMerito::create($request->all());

        return redirect()->route('notasMerito.edit', $notaMerito->id)
            ->with('info', 'Nota guardada con éxito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\NotaMerito  $notaMerito
     * @return \Illuminate\Http\Response
     */
    public function show(NotaMerito $notaMerito)
    {
        return view('notasMerito.show', compact('notaMerito'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\NotaMerito  $notaMerito
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $convocatoria_id, $postulante_id)
    {

        $notasMerito =  DB::table('postulants')->where('postulants.id', $postulante_id)
            ->join('nota_meritos', 'postulants.id', '=', 'nota_meritos.postulante_id')
            ->select('nota_meritos.id', 'nota_meritos.nota', 'postulants.name', 'postulants.last_name')
            ->first();

        $meritos = DB::table('postulants')->where('postulants.id', $postulante_id)
            ->join('nota_meritos', 'postulants.id', '=', 'nota_meritos.postulante_id')
            ->join('requerimientos', 'postulants.requerimiento_id', '=', 'requerimientos.id')
            ->join('meritos', 'requerimientos.convocatoria_id', '=', 'meritos.convocatoria_id')
            ->select('requerimientos.destino', 'postulants.id', 'postulants.name', 'postulants.last_name', 'nota_meritos.nota', 'meritos.nombre', 'meritos.porcentaje', 'meritos.convocatoria_id')
            ->get();

        $mertos = DB::table('postulants')->where('postulants.id', $postulante_id)
            ->join('nota_meritos', 'postulants.id', '=', 'nota_meritos.postulante_id')
            ->join('requerimientos', 'postulants.requerimiento_id', '=', 'requerimientos.id')
            ->join('mertos', 'requerimientos.convocatoria_id', '=', 'mertos.convocatoria_id')
            ->select('requerimientos.destino', 'postulants.id', 'postulants.name', 'postulants.last_name', 'nota_meritos.nota', 'mertos.nombre', 'mertos.porcentaje', 'mertos.convocatoria_id')
            ->get();

        $meritos1 = DB::table('postulants')->where('postulants.id', $postulante_id)
            ->join('nota_meritos', 'postulants.id', '=', 'nota_meritos.postulante_id')
            ->join('requerimientos', 'postulants.requerimiento_id', '=', 'requerimientos.id')
            ->join('meritos1s', 'requerimientos.convocatoria_id', '=', 'meritos1s.convocatoria_id')
            ->select('requerimientos.destino', 'postulants.id', 'postulants.name', 'postulants.last_name', 'nota_meritos.nota', 'meritos1s.nombre', 'meritos1s.porcentaje', 'meritos1s.convocatoria_id')
            ->get();


        return view('notasMerito.edit', compact('notasMerito', 'meritos', 'meritos1', 'mertos', 'convocatoria_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\NotaMerito  $notaMerito
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $convocatoria_id, NotaMerito $notaMerito)
    {

        $nota_merito1 = $request->get('total1');
        $nota_merito2 = $request->get('total2');
        $nota_merito3 = $request->get('total3');

        $postulante_id = $notaMerito->postulante_id;


        $porcentajeMerito1 = $request->get('porcentaje');
        $porcentajeMerito2 = $request->get('porcentaje1');
        $porcentajeMerito3 = $request->get('porcentaje2');


        $newNota = NotaMerito::where('nota_meritos.postulante_id', $postulante_id)->first();
        $newNota->nota = ($nota_merito1 + $nota_merito2 + $nota_merito3);
        $newNota->save();

        $convocatoria = DB::table('convocatorias')->where('convocatorias.id', $convocatoria_id)->first();

        $postulante_final = Postulant::where('postulants.id', $postulante_id)->first();
        $postulante_final->nota_final = ($newNota->nota * $convocatoria->porcentaje_meritos / 100) + ($postulante_final->nota_conocimientos * $convocatoria->porcentaje_conocimiento / 100);

        $postulante_final->update();


        return redirect()->route('notasMerito.postindex', compact('convocatoria_id'))
            ->with('info', 'Nota actualizada con éxito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\NotaMerito  $notaMerito
     * @return \Illuminate\Http\Response
     */
    public function destroy(NotaMerito $notaMerito)
    {
        //
    }

    public function publicar($id)
    {
        $convocatoria = Convocatoria::find($id);
        if ($convocatoria->publicacion_meritos == 1) {
            $convocatoria->publicacion_meritos = 2;
            $convocatoria->update();
        } else {
            $convocatoria->publicacion_meritos = 1;
            $convocatoria->update();
        }

        return redirect()->route('notasMerito.postindex', $convocatoria->id);
    }
}
