<?php

namespace App\Http\Controllers;

use App\Documento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DocumentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($convocatoria_id)
    {
        $documentos = DB::table('documentos')->where('convocatoria_id', $convocatoria_id)->get();

        return View("documentos/index", ['documentos' => $documentos, 'convocatoria_id' => $convocatoria_id]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($convocatoria_id)
    {
        return View("documentos/create", ['convocatoria_id' => $convocatoria_id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $convocatoria_id)
    {
        $campos = [
            'documento' => 'required|string|max:255|unique:documentos,documento,NULL,id,convocatoria_id,'.$convocatoria_id,
        ];
        $Mensaje = ["required" => 'El :attribute es requerido'];

        $this->validate($request, $campos, $Mensaje);

        $documento = $request->input("documento");
        $documento_nuevo = new Documento();
        $documento_nuevo->convocatoria_id = $convocatoria_id;
        $documento_nuevo->documento = $documento;
        $documento_nuevo->save();
        return redirect(route("detalle.index", ['convocatoria_id' => $convocatoria_id]))->with('Mensaje', 'Documento creado con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Documento  $documento
     * @return \Illuminate\Http\Response
     */
    public function show(Documento $documento)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Documento  $documento
     * @return \Illuminate\Http\Response
     */
    public function edit($convocatoria_id, $documento_id)
    {
        $documento = Documento::findOrFail($documento_id);

        return View("documentos/edit", ['documento' => $documento, 'convocatoria_id' => $convocatoria_id, 'documento_id' => $documento_id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Documento  $documento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $convocatoria_id, $documento_id)
    {
        $campos = [
            'documento' => 'required|string|max:255|unique:documentos,documento,NULL,id,convocatoria_id,'.$convocatoria_id,
        ];
        $Mensaje = ["required" => 'El :attribute es requerido'];

        $this->validate($request, $campos, $Mensaje);

        $documento = $request->input("documento");
        $documento_nuevo = Documento::findOrFail($documento_id);
        $documento_nuevo->convocatoria_id = $convocatoria_id;
        $documento_nuevo->documento = $documento;
        $documento_nuevo->save();
        return redirect(route("detalle.index", ['convocatoria_id' => $convocatoria_id]))->with('Mensaje', 'Documento modificado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Documento  $documento
     * @return \Illuminate\Http\Response
     */
    public function destroy($convocatoria_id, $documento_id)
    {
        Documento::destroy($documento_id);
        return redirect(route("detalle.index", ['convocatoria_id' => $convocatoria_id]))->with('Mensaje', 'Documento eliminado con exito');
    }
}
