<?php

namespace App\Http\Controllers;

use App\InformacionPersonal;
use Illuminate\Http\Request;

class InformacionPersonalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InformacionPersonal  $informacionPersonal
     * @return \Illuminate\Http\Response
     */
    public function show(InformacionPersonal $informacionPersonal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InformacionPersonal  $informacionPersonal
     * @return \Illuminate\Http\Response
     */
    public function edit(InformacionPersonal $informacionPersonal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InformacionPersonal  $informacionPersonal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InformacionPersonal $informacionPersonal)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InformacionPersonal  $informacionPersonal
     * @return \Illuminate\Http\Response
     */
    public function destroy(InformacionPersonal $informacionPersonal)
    {
        //
    }
}
