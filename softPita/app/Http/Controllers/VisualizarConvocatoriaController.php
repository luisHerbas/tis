<?php

namespace App\Http\Controllers;

use App\Convocatoria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VisualizarConvocatoriaController extends Controller
{
    //Muestra las convocatorias para el publico
    public function index()
    {
        $convocatorias = DB::table('subunidades')
            ->join('departamentos', 'departamentos.id', 'subunidades.id_departamento')
            ->join('convocatorias', 'subunidades.id', '=', 'convocatorias.subunidad_id')
            ->select('subunidades.name', 'convocatorias.id', 'convocatorias.titulo_convocatoria', 'convocatorias.fecha_publicacion', 'departamentos.name as dep', 'convocatorias.estado')
            ->where('convocatorias.estado', 'Habilitada')
            ->orderBy('fecha_publicacion', 'DESC')
            ->get();
        return view('visualizar.index', compact('convocatorias'));
    }

    //Muestra las convocatorias en la sección de administración
    public function indexAdmin($convocatoria_id)
    {
        $titulos = DB::table('convocatorias')->find($convocatoria_id);
        $requerimientos = DB::table('requerimientos')->where('convocatoria_id', $titulos->id)->get();
        $requisitos = DB::table('requisitos')->where('convocatoria_id', $titulos->id)->get();
        $fechas = DB::table('fecha_pruebas')->where('convocatoria_id', $titulos->id)->get();
        $documentos = DB::table('documentos')->where('convocatoria_id', $titulos->id)->get();
        $meritos = DB::table('meritos')->where('convocatoria_id', $convocatoria_id)->get();
        $meritos1 = DB::table('meritos1s')->where('convocatoria_id', $convocatoria_id)->get();
        $mertos = DB::table('mertos')->where('convocatoria_id', $convocatoria_id)->get();
        $subunidades = DB::table('subunidades')->where('subunidades.id', $titulos->subunidad_id)->first();
        $departamentos = DB::table('departamentos')->where('departamentos.id', $subunidades->id_departamento)->first();
        return View("visualizar.indexAdmin", ['titulos' => $titulos, 'requerimientos' => $requerimientos, 'requisitos' => $requisitos, 'fechas' => $fechas, 'documentos' => $documentos, 'meritos' => $meritos, 'meritos1' => $meritos1, 'mertos' => $mertos, 'departamentos' => $departamentos]);
    }

    //Muestra convocatorias vigentes para el público
    public function indexparaadmin($convocatoria_id)
    {
        $titulos = DB::table('convocatorias')->find($convocatoria_id);
        $requerimientos = DB::table('requerimientos')->where('convocatoria_id', $titulos->id)->get();
        $requisitos = DB::table('requisitos')->where('convocatoria_id', $titulos->id)->get();
        $fechas = DB::table('fecha_pruebas')->where('convocatoria_id', $titulos->id)->get();
        $documentos = DB::table('documentos')->where('convocatoria_id', $titulos->id)->get();
        $meritos = DB::table('meritos')->where('convocatoria_id', $convocatoria_id)->get();
        $meritos1 = DB::table('meritos1s')->where('convocatoria_id', $convocatoria_id)->get();
        $mertos = DB::table('mertos')->where('convocatoria_id', $convocatoria_id)->get();

        $postulantes = DB::table('requerimientos')->where('requerimientos.convocatoria_id', $convocatoria_id)
            ->join('postulants', 'postulants.requerimiento_id', 'requerimientos.id')
            ->join('nota_conocimientos', 'nota_conocimientos.postulante_id', 'postulants.id')
            ->join('nota_meritos', 'nota_meritos.postulante_id', 'postulants.id')
            ->join('notas', 'notas.id', 'nota_conocimientos.nota_id')
            ->select('postulants.name', 'postulants.last_name', 'requerimientos.destino', 'postulants.nota_conocimientos', 'nota_meritos.nota', 'requerimientos.destino', 'notas.nombre_nota', 'nota_conocimientos.nota')
            ->orderby('notas.nombre_nota', 'ASC')
            ->orderby('postulants.last_name', 'ASC')
            ->get();

        $departamento = DB::table('convocatorias')
            ->where('convocatorias.id', $convocatoria_id)
            ->join('subunidades', 'subunidades.id', 'convocatorias.subunidad_id')
            ->join('departamentos', 'departamentos.id', 'subunidades.id_departamento')
            ->select('departamentos.id')
            ->first();

        $departamentos = DB::table('convocatorias')
            ->where('convocatorias.id', $convocatoria_id)
            ->join('subunidades', 'subunidades.id', 'convocatorias.subunidad_id')
            ->join('departamentos', 'departamentos.id', 'subunidades.id_departamento')
            ->select('departamentos.name', 'departamentos.id', 'subunidades.name')
            ->first();

        $notas_meritos = DB::table('requerimientos')->where('requerimientos.convocatoria_id', $convocatoria_id)
            ->join('postulants', 'postulants.requerimiento_id', 'requerimientos.id')
            ->join('nota_meritos', 'nota_meritos.postulante_id', 'postulants.id')
            ->select('postulants.name', 'postulants.last_name', 'requerimientos.destino', 'postulants.nota_conocimientos', 'nota_meritos.nota', 'nota_meritos.nota')
            ->orderby('postulants.last_name', 'ASC', 'requerimientos.destino')
            ->get();

        $finales = DB::table('requerimientos')->where('requerimientos.convocatoria_id', $convocatoria_id)
            ->join('postulants', 'postulants.requerimiento_id', 'requerimientos.id')
            ->join('nota_meritos', 'nota_meritos.postulante_id', 'postulants.id')
            ->select('postulants.name', 'postulants.last_name', 'requerimientos.destino', 'postulants.nota_conocimientos', 'postulants.nota_final', 'nota_meritos.nota', 'nota_meritos.nota')
            ->orderby('postulants.last_name', 'ASC', 'requerimientos.destino')
            ->get();

        return View("historicos.indexVisualizar", [
            'titulos' => $titulos, 'requerimientos' => $requerimientos, 'requisitos' => $requisitos, 'fechas' => $fechas, 'documentos' => $documentos,
            'meritos' => $meritos, 'meritos1' => $meritos1, 'mertos' => $mertos, 'departamentos' => $departamentos, '$departamento_id' => $departamento->id, 'convocatoria_id' => $convocatoria_id, 'postulantes' => $postulantes, 'notas_meritos' => $notas_meritos,
            'finales' => $finales
        ]);
    }
}
