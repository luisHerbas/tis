<?php

namespace App\Http\Controllers;

use App\Subunidad;
use Illuminate\Http\Request;

class SubunidadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id_departamento)
    {
        $sub = new Subunidad;
        $cabeceras = $sub->getTableColumns();
        $unidades = Subunidad::where('id_departamento', $id_departamento)->get();
        return view('subunidades.index', ['unidades' => $unidades, 'cabeceras' => $cabeceras, 'id_departamento' => $id_departamento]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id_departamento)
    {
        return view('subunidades/register', ['id_departamento' => $id_departamento]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id_departamento)
    {
        $campos = [
            'name' => 'required|string|max:255|unique:subunidades,name'

        ];
        $Mensaje = ["required" => 'El :attribute es requerido', "unique" => 'La subunidad ya existe'];

        $this->validate($request, $campos, $Mensaje);

        $name = $request->input("name");

        $new_sub = new Subunidad;
        $new_sub->name = $name;
        $new_sub->id_departamento = $id_departamento;
        //dd($new_sub);
        $new_sub->save();
        return redirect(route("subunidad.index", ['id_departamento' => $id_departamento]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Subunidad  $subunidad
     * @return \Illuminate\Http\Response
     */
    public function show(Subunidad $subunidad)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Subunidad  $subunidad
     * @return \Illuminate\Http\Response
     */
    public function edit($id_dep, $id_sub)
    {
        $sub = Subunidad::where('id', $id_sub)->first();
        return view('subunidades.edit', ['subunidad' => $sub, 'id_sub' => $sub->id, 'id_dep' => $id_dep]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subunidad  $subunidad
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_dep, $id_sub)
    {
        $campos = [
            'name' => 'required|string|max:255|unique:subunidades,name'

        ];
        $Mensaje = ["required" => 'El :attribute es requerido', "unique" => 'La subunidad ya existe'];

        $this->validate($request, $campos, $Mensaje);

        $name = $request->input("name");

        $sub = Subunidad::where('id', $id_sub)->first();
        $sub->name = $name;
        $sub->save();
        return redirect(route("subunidad.index", ['id_dep' => $id_dep]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subunidad  $subunidad
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_dep, $id_sub)
    {
        Subunidad::destroy($id_sub);
        return redirect(route("subunidad.index", ['id_dep' => $id_dep]));
    }
}
