<?php

namespace App\Http\Controllers;

use App\comisionConocimiento;
use Illuminate\Http\Request;

class ComisionConocimientoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\comisionConocimiento  $comisionConocimiento
     * @return \Illuminate\Http\Response
     */
    public function show(comisionConocimiento $comisionConocimiento)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\comisionConocimiento  $comisionConocimiento
     * @return \Illuminate\Http\Response
     */
    public function edit(comisionConocimiento $comisionConocimiento)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\comisionConocimiento  $comisionConocimiento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, comisionConocimiento $comisionConocimiento)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\comisionConocimiento  $comisionConocimiento
     * @return \Illuminate\Http\Response
     */
    public function destroy(comisionConocimiento $comisionConocimiento)
    {
        //
    }
}
