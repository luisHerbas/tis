<?php

namespace App\Http\Controllers;

use App\Requerimiento;
use Illuminate\Http\Request;
use App\Nota;
use Illuminate\Support\Facades\DB;

class RequerimientoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($convocatoria_id)
    {
        $requerimientos = DB::table('requerimientos')->where('convocatoria_id', $convocatoria_id)->get();
        $requisitos = DB::table('requisitos')->where('convocatoria_id', $convocatoria_id)->get();
        $documentos = DB::table('documentos')->where('convocatoria_id', $convocatoria_id)->get();
        $fechas = DB::table('fecha_pruebas')->where('convocatoria_id', $convocatoria_id)->get();

        return View("requerimientos/index", ['requerimientos' => $requerimientos, 'requisitos' => $requisitos, 'fechas' => $fechas, 'documentos' => $documentos, 'convocatoria_id' => $convocatoria_id]);
    }

    /**
     * Show the form for creating a new rcd d   esource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($convocatoria_id)
    {
        $convocatorias = DB::table('convocatorias')->find($convocatoria_id);
        $items = DB::table('items')->where('subunidad_id', $convocatorias->subunidad_id)->get();

        return View("requerimientos/create", ['convocatoria_id' => $convocatoria_id, 'items' => $items]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $convocatoria_id)
    {
        $campos = [
            'cantidad_auxiliares' => 'required|integer',
            'horas_academicas' => 'required|integer',
            'destino' => 'required',

        ];
        $Mensaje = ["required" => 'El :attribute es requerido'];

        $this->validate($request, $campos, $Mensaje);

        $cantidad_auxiliares = $request->input("cantidad_auxiliares");
        $horas_academicas = $request->input("horas_academicas");
        $destino = $request->input("destino");
        $requerimiento_nuevo = new Requerimiento();
        $requerimiento_nuevo->convocatoria_id = $convocatoria_id;
        $requerimiento_nuevo->cantidad_auxiliares = $cantidad_auxiliares;
        $requerimiento_nuevo->horas_academicas = $horas_academicas;
        $requerimiento_nuevo->destino = $destino;
        $requerimiento_nuevo->save();

        $new_nota = new Nota();
        $new_nota->requerimiento_id = $requerimiento_nuevo->id;
        $new_nota->nombre_nota = "Examen de conocimientos de " . $requerimiento_nuevo->destino;

        $new_nota->save();

        return redirect(route("detalle.index", ['convocatoria_id' => $convocatoria_id]))->with('Mensaje', 'Requerimiento creado con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Requerimiento  $requerimiento
     * @return \Illuminate\Http\Response
     */
    public function show(Requerimiento $requerimiento)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Requerimiento  $requerimiento
     * @return \Illuminate\Http\Response
     */
    public function edit($convocatoria_id, $requerimiento_id)
    {
        $requerimiento = Requerimiento::findOrFail($requerimiento_id);
        $convocatorias = DB::table('convocatorias')->find($convocatoria_id);
        $items = DB::table('items')->where('subunidad_id', $convocatorias->subunidad_id)->get();


        return View("requerimientos/edit", ['requerimiento' => $requerimiento, 'convocatoria_id' => $convocatoria_id, 'requerimiento_id' => $requerimiento_id, 'items' => $items]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Requerimiento  $requerimiento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $convocatoria_id, $requerimiento_id)
    {
        $campos = [
            'cantidad_auxiliares' => 'required|integer',
            'horas_academicas' => 'required|integer',
            'destino' => 'required|string|max:255',

        ];
        $Mensaje = ["required" => 'El :attribute es requerido'];

        $this->validate($request, $campos, $Mensaje);

        $cantidad_auxiliares = $request->input("cantidad_auxiliares");
        $horas_academicas = $request->input("horas_academicas");
        $destino = $request->input("destino");
        $requerimiento_nuevo = Requerimiento::findOrFail($requerimiento_id);
        $requerimiento_nuevo->convocatoria_id = $convocatoria_id;
        $requerimiento_nuevo->cantidad_auxiliares = $cantidad_auxiliares;
        $requerimiento_nuevo->horas_academicas = $horas_academicas;
        $requerimiento_nuevo->destino = $destino;
        $requerimiento_nuevo->save();
        return redirect(route("detalle.index", ['convocatoria_id' => $convocatoria_id]))->with('Mensaje', 'Requerimiento modificado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Requerimiento  $requerimiento
     * @return \Illuminate\Http\Response
     */
    public function destroy($convocatoria_id, $requerimiento_id)
    {
        Requerimiento::destroy($requerimiento_id);
        return redirect(route("detalle.index", ['convocatoria_id' => $convocatoria_id]))->with('Mensaje', 'Requerimiento eliminado con exito');
    }
}
