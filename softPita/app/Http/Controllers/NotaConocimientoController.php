<?php

namespace App\Http\Controllers;

use App\Convocatoria;
use App\NotaConocimiento;
use Illuminate\Http\Request;
use App\Subunidad;
use App\Postulant;
use Illuminate\Support\Facades\DB;


class NotaConocimientoController extends Controller
{
    /**
     * Se listan a todos los postulantes habilitados para su calificacion de conocimientos
     */
    public function index($convocatoria_id)
    {
        $convocatoria = DB::table('convocatorias')->where('convocatorias.id', $convocatoria_id)->orderby('fecha_publicacion', 'DESC')->first();

        $notas = DB::table('requerimientos')->where('requerimientos.convocatoria_id', $convocatoria->id)
            ->join('notas', 'requerimientos.id', '=', 'notas.requerimiento_id')
            ->select('notas.id', 'requerimientos.destino', 'notas.nombre_nota', 'notas.requerimiento_id')
            ->get();


        $nombres = DB::table('requerimientos')->where('requerimientos.convocatoria_id', $convocatoria->id)
            ->join('convocatorias', 'requerimientos.convocatoria_id', 'convocatorias.id')
            ->join('notas', 'notas.requerimiento_id', 'requerimientos.id')
            ->select('convocatorias.subunidad_id', 'notas.id', 'notas.requerimiento_id', 'requerimientos.destino', 'notas.nombre_nota', 'convocatorias.publicacion_conocimientos')
            ->groupby('requerimientos.destino')
            ->orderby('requerimientos.destino', 'DESC')
            ->get();

        $asignaturas = DB::table('requerimientos')->where('requerimientos.convocatoria_id', $convocatoria->id)
            ->join('convocatorias', 'requerimientos.convocatoria_id', 'convocatorias.id')
            ->join('notas', 'notas.requerimiento_id', 'requerimientos.id')
            ->select('convocatorias.subunidad_id', 'notas.id', 'notas.requerimiento_id', 'notas.nombre_nota')
            ->orderby('requerimientos.destino', 'DESC')
            ->get();

        $postulantes = DB::table('requerimientos')->where('requerimientos.convocatoria_id', $convocatoria->id)
            ->join('convocatorias', 'requerimientos.convocatoria_id', 'convocatorias.id')
            ->join('postulants', 'postulants.requerimiento_id', 'requerimientos.id')
            ->select('requerimientos.destino', 'postulants.id', 'postulants.name', 'postulants.last_name', 'postulants.nota_conocimientos', 'convocatorias.subunidad_id')
            ->where('postulants.id_status', '2')
            ->orderBy('requerimientos.destino', 'ASC')
            ->get();

        return view('notasConocimiento.index', compact('convocatoria', 'postulantes', 'notas', 'asignaturas', 'nombres', 'convocatoria_id'));
    }

    /**
     * Redirecciona a la pagina que permite hacer el filtraje
     * El filtraje está conformado por: Departamento, subunidad y convocatorias
     */
    public function preindex()
    {
        $departamentos = DB::table('departamentos')->get();

        return view('notasConocimiento.preindex', compact('departamentos'));
    }

    public function getSubunidades(Request $request, $id)
    {
        if ($request->ajax()) {
            $subunidades = Subunidad::subunidades($id);
            return response()->json($subunidades);
        }
    }

    public function getConvocatorias(Request $request, $id)
    {
        if ($request->ajax()) {
            $convocatorias = Convocatoria::convocatorias($id);
            return response()->json($convocatorias);
        }
    }

    public function create()
    {
        return view('notasConocimiento.create');
    }

    public function store(Request $request)
    {
        $notaConocimiento = NotaConocimiento::create($request->all());

        return redirect()->route('notasConocimiento.edit', $notaConocimiento->id)
            ->with('info', 'Nota guardada con éxito');
    }

    public function show(NotaConocimiento $notaConocimiento)
    {
        return view('notasConocimiento.show', compact('notaConocimiento'));
    }

    public function edit(NotaConocimiento $notaConocimiento)
    {
        return view('notasConocimiento.edit', compact('notaConocimiento'));
    }

    /**
     * Ingresa los datos de los requerimientos y postulantes
     * Redirecciona al ingreso de notas conocimiento
     */
    public function ingresar(Request $request, $convocatoria_id, $postulante_id)
    {
        $notas = DB::table('requerimientos')->where('requerimientos.convocatoria_id', $convocatoria_id)
            ->join('notas', 'notas.requerimiento_id', 'requerimientos.id')
            ->join('postulants', 'postulants.requerimiento_id', 'requerimientos.id')
            ->join('nota_conocimientos', 'nota_conocimientos.postulante_id', 'postulants.id')
            ->select('nota_conocimientos.nota', 'notas.nombre_nota', 'notas.porcentaje')
            ->get();

        $notasConocimiento1 = DB::table('postulants')->where('postulants.id', $postulante_id)
            ->first();


        $notasConocimiento = DB::table('postulants')->where('postulants.id', $postulante_id)
            ->join('nota_conocimientos', 'nota_conocimientos.postulante_id', 'postulants.id')
            ->join('notas', 'nota_conocimientos.nota_id', 'notas.id')
            ->select('postulants.name', 'postulants.last_name', 'nota_conocimientos.nota', 'notas.nombre_nota', 'nota_conocimientos.id', 'nota_conocimientos.postulante_id', 'notas.porcentaje')
            ->get();

        return view('notasConocimiento.ingresar', compact('notasConocimiento', 'notas', 'notasConocimiento1', 'convocatoria_id', 'postulante_id'));
    }

    public function calificar(Request $request, $convocatoria_id, $postulante_id, $notaConocimiento_id)
    {
        
        $nota = DB::table('nota_conocimientos')->find($notaConocimiento_id);
        $notas = DB::table('nota_conocimientos')->where('nota_conocimientos.id', $notaConocimiento_id)
        ->join('notas', 'notas.id', 'nota_conocimientos.nota_id')
        ->first();
        return view('notasConocimiento.calificar', compact('nota', 'convocatoria_id', 'postulante_id', 'notas'));
    }


    public function calificarupdate(Request $request, $convocatoria_id, $postulante_id, $notaConocimiento_id)
    {
        $new_nota = $request->get('nota');
        $nota = NotaConocimiento::find($notaConocimiento_id);
        $nota->postulante_id = $postulante_id;
        $nota->nota_id = $nota->nota_id;
        $nota->nota = $new_nota;
        $nota->save();

        return redirect()->route('notasConocimiento.ingresar', compact('convocatoria_id', 'postulante_id'));
    }


    public function update(Request $request, $convocatoria_id,  $postulante_id)
    {
        $campos = [
            'nota_conocimiento' => 'required|max:100'
        ];
        $Mensaje = ["required" => 'El :attribute es requerido'];

        $this->validate($request, $campos, $Mensaje);

        $nota = $request->input("nota_conocimiento");

        $nota_merito = DB::table('nota_meritos')->where('nota_meritos.postulante_id', $postulante_id)
            ->select('nota_meritos.nota')
            ->first();

        $convocatoria = DB::table('convocatorias')->where('convocatorias.id', $convocatoria_id)->first();
        
        $nota_nueva = Postulant::find($postulante_id);
        $nota_nueva->nota_conocimientos = $nota;
        $nota_nueva->nota_final = ($nota_merito->nota * $convocatoria->porcentaje_meritos / 100) + ($nota * $convocatoria->porcentaje_conocimiento / 100);
        $nota_nueva->update();

        return redirect()->route('notasConocimiento.index', $convocatoria->id);
    }

    public function addNewNote(Request $request, NotaConocimiento $notaConocimiento)
    {
        $notaNuevaConocimiento = $request->get('notaPostulante');
        if ($notaConocimiento->Nota1 == 0) {
            $notaConocimiento->Nota1 = $notaNuevaConocimiento;
        } elseif ($notaConocimiento->Nota2 == 0) {
            $notaConocimiento->Nota2 = $notaNuevaConocimiento;
        } elseif ($notaConocimiento->Nota3 == 0) {
            $notaConocimiento->Nota3 = $notaNuevaConocimiento;
        } elseif ($notaConocimiento->Nota4 == 0) {
            $notaConocimiento->Nota4 = $notaNuevaConocimiento;
        } elseif ($notaConocimiento->Nota5 == 0) {
            $notaConocimiento->Nota5 = $notaNuevaConocimiento;
        }
        $notaConocimiento->save();

        return redirect()->route('notasConocimiento.index', $notaConocimiento->id)
            ->with('info', 'Nota guardada con éxito');
    }

    public function publicar($id)
    {
        $convocatoria = Convocatoria::find($id);
        if ($convocatoria->publicacion_conocimientos == 1) {
            $convocatoria->publicacion_conocimientos = 2;
            $convocatoria->update();
        } else {
            $convocatoria->publicacion_conocimientos = 1;
            $convocatoria->update();
        }
        return redirect()->route('notasConocimiento.index', $convocatoria->id);
    }
}
