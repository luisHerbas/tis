<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subunidad extends Model
{
    protected $connection = 'mysql';

    public $timestamps = false;
    protected $fillable = ['name', 'id_departamento'];
    protected $table = "subunidades";

    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    public static function subunidades($id){
        return Subunidad::where('id_departamento', '=', $id)->get();
    }

    public function requisitos(){

        return $this->hasMany(Requisito::class);
    }
}
