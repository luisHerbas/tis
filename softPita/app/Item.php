<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $connection = 'mysql';

    public $timestamps = false;

    protected $fillable = [

        'nombre', 'nivel', 'electiva','subunidad_id'
    ];

}
